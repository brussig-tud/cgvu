
#ifndef __LIBCONFIG_H__
#define __LIBCONFIG_H__


//////
//
// API export management
//

// API link signature
#if defined(WIN32) && defined(BUILDING_CGVU)
	#define CGVU_API __declspec(dllexport)
#elif defined(WIN32) && !defined(CGVU_STATIC_LINK)
	#define CGVU_API __declspec(dllimport)
#else
	#define CGVU_API
#endif



//////
//
// Base classes
//

// Namespace openings
namespace cgvu {


/**
 * @brief Lightweight wrapper for generic handles.
 * @todo: move such things into their own "base"-like module
 */
class CGVU_API Handle
{
private:
	void *handle;

public:
	inline Handle() : handle(nullptr) {}
	inline Handle(const Handle &other) : handle(other.handle) {}
	inline Handle(void *init) : handle(init) {}
	inline Handle(size_t init) : handle(reinterpret_cast<void*>(init)) {}
	template <class T> inline Handle(T init)
		: handle(reinterpret_cast<void*>((size_t)init))
	{}

	inline Handle& operator= (const Handle& other) {handle=other.handle; return *this;}
	inline Handle& operator= (void* newHandle) { handle = newHandle; return *this; }
	inline Handle& operator= (size_t newHandle)
	{
		handle = reinterpret_cast<void*>(newHandle);
		return *this;
	}
	template <class T> inline Handle& operator = (T newHandle)
	{
		handle = reinterpret_cast<void*>((size_t)newHandle);
		return *this;
	}

	inline bool operator== (const Handle& other) { return handle == other.handle; }
	inline bool operator== (void *handleValue) { return handle == handleValue; }
	inline bool operator== (size_t handleValue)
	{
		return handle == reinterpret_cast<void*>(handleValue);
	}
	template <class T> inline bool operator == (T handleValue)
	{
		return handle == reinterpret_cast<void*>((size_t)handleValue);
	}

	inline operator void* () const { return handle; }
	inline operator size_t () const { return reinterpret_cast<size_t>(handle); }
	inline operator bool () const { return handle; }
	template <class T> inline operator T () const { return reinterpret_cast<T>(handle); }
};


// Namespace closings
} // cgvu


#endif // ifndef __LIBCONFIG_H__
