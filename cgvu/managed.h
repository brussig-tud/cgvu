
#ifndef __MANAGED_H__
#define __MANAGED_H__


//////
//
// Includes
//

// Library config
#include "libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Class definitions
//

/**
 * @brief
 *		Base class for all objects that are managed by a top-level CGVu class with
 *		automatic lifecycle. Makes heavy use of CRTP ("<a
 *		href="https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern"
 *		>Curiously Recurring Template Pattern</a>")
 */
template <class Object, class Manager>
class CGVU_API ManagedObject
{

public:

	////
	// Exported types

	/** @brief STL-compatible hasher object that managers can use. */
	struct Hash
	{
		/** @brief The hash function operator as per STL requirements. */
		size_t operator() (const ManagedObject& obj) const { return (size_t)obj.pimpl; }
	};

	/** @brief Proxy class used to pass a reference to a managed object to clients. */
	class Reference
	{
		////
		// Interfacing

		// Friend declarations
		friend class ManagedObject;


	protected:

		////
		// Data members

		/** @brief The instance that this reference points to. */
		Object &obj;


	public:

		////
		// Object construction / destruction

		/** @brief Constructs the reference for the given implementation instance. */
		Reference(Object &obj) : obj(obj) {}
	};


protected:

	////
	// Data members

	/** @brief Object implementation pointer. */
	void *pimpl;

	/**
	 * @brief
	 *		Back-reference to the manager of this object. Needed to deal with limitations
	 *		on virtual function calls during constructor and destructor execution.
	 */
	Manager *mgr;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the object will not be
	 *		usable until a functioning instance is moved in using either @link
	 *		#operator=(cgvu::ManagedObject&&) move assignment @endlink, or @link
	 *		#operator=(cgvu::ManagedObject::Reference&&) reference assignment @endlink.
	 */
	ManagedObject() : pimpl(nullptr), mgr(nullptr) {}

	/** @brief Not copy-constructible. */
	ManagedObject(const ManagedObject&) = delete;

	/** @brief The move constructor. */
	ManagedObject(ManagedObject &&other) : pimpl(other.pimpl), mgr(other.mgr)
	{
		other.pimpl = nullptr;
		other.mgr = nullptr;
	}

	/** @brief The reference constructor. */
	ManagedObject(Reference &&instance)
		: pimpl(instance.obj.pimpl), mgr(&(instance.obj.manager()))
	{}

	/** @brief The destructor. */
	virtual ~ManagedObject()
	{
		if (pimpl)
			mgr->deleteObject(*((Object*)this));
	}


	////
	// Operators

	/** @brief Not copy-assignable. */
	ManagedObject& operator= (const ManagedObject&) = delete;

	/** @brief Move assignment. */
	ManagedObject& operator= (ManagedObject &&other)
	{
		// Steal the other's implementation instance
		std::swap(pimpl, other.pimpl);
		// Steal the other's manager
		std::swap(mgr, other.mgr);
		return *this;
	}

	/** @brief Reference assignment. */
	ManagedObject& operator= (Reference &&instance)
	{
		if (pimpl)
			mgr->deleteObject(*((Object*)this));
		pimpl = instance.obj.pimpl;
		mgr = &(instance.obj.manager());
		return *this;
	}

	/** @brief Equality comparison. */
	bool operator== (const ManagedObject &other) const { return pimpl == other.pimpl; }

	/** @brief Creation check. */
	operator bool (void) const { return pimpl; }


	////
	// Methods

	/** @brief Returns a reference to the manager of this object. */
	virtual Manager& manager (void) = 0;

	/** @brief Returns an ID uniquely identifying this object. */
	virtual size_t objectID (void) const { return (size_t)pimpl; }
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __MANAGED_H__
