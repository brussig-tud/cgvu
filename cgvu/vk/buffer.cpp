
//////
//
// Includes
//

// C++ STL
#include <vector>

// Vulkan
#include <vulkan/vulkan.h>

// CGVU includes
#include "error.h"
#include "util/utils.h"
#include "log/logging.h"

// Module includes
#include "vulkan.h"
#include "vulkan_internals.h"

// Implemented header
#include "buffer.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;



//////
//
// Global state
//

// Anonymous namespace begin
namespace {

	// Empty unintialized uniform buffer for use with the default constructur of
	// cgvu::Uniform
	static UniformBuffer uninitializedUB;

// Anonymous namespace end
}



//////
//
// Class implementations
//

////
// cgvu::BufferObject

struct BufferObject_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	VkBufferUsageFlags usageFlags;
	VkMemoryPropertyFlags memFlags;
	VkDeviceSize size = 0;

	VkBuffer buf = VK_NULL_HANDLE;
	VkDeviceMemory mem = VK_NULL_HANDLE;

	char *mapped = nullptr;
	VkFence workFence = VK_NULL_HANDLE;

	std::vector<BufferObject::EnsureAction> pea;

	BufferObject_impl(
		Context &ctx, VkBufferUsageFlags usageFlags, VkMemoryPropertyFlags memFlags
	)
		: ctx(ctx), usageFlags(usageFlags), memFlags(memFlags)
	{}
};
#define BUFFEROBJ_IMPL ((BufferObject_impl*)pimpl)
#define BUFFEROBJ_PTR(inst) ((BufferObject_impl*)((inst).pimpl))

BufferObject::BufferObject(Context &ctx, size_t size, unsigned usageFlags, unsigned memProps)
{
	// Create implementation instance
	pimpl = new BufferObject_impl(ctx, usageFlags, memProps);
	auto &impl = *BUFFEROBJ_IMPL;

	// Create buffer object
	VkBufferCreateInfo bufCreateInfo = {};
	bufCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufCreateInfo.size = size;
	bufCreateInfo.usage = usageFlags;
	bufCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VkResult result =
		vkCreateBuffer(ctx.handle(), &bufCreateInfo, nullptr, &(impl.buf));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"BufferObject::<ctor>", "Unable to create Vulkan buffer object!"
		));
	impl.initialized = true; // We have something to clean up from here on

	// Allocate device memory for the buffer
	// - query the memory requirements of the buffer
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(ctx.handle(), impl.buf, &memRequirements);
	// - actually allocate memory in the appropriate heap
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	impl.size = allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = ((VulkanDeviceInfo*)ctx.getDeviceInfo())->findMemoryType(
		memRequirements, memProps
	);
	result = vkAllocateMemory(ctx.handle(), &allocInfo, nullptr, &(impl.mem));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"BufferObject::<ctor>", "Unable to allocate device memory for the buffer!"
		));
	result = vkBindBufferMemory(ctx.handle(), impl.buf, impl.mem, 0);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"BufferObject::<ctor>", "Unable to bind allocated device memory to the"
			" buffer!"
		));

	// Done!
	impl.started = true;
}

void BufferObject::destroy (void)
{
	// Shortcut for saving one indirection
	auto &impl = *BUFFEROBJ_IMPL;

	// Handle cleanup
	if (impl.initialized)
	{
		// Buffer object cannot be used from here on anymore
		impl.started = false;

		// Free buffer resources.
		if (impl.mapped)
			unmap();
		if (impl.mem)
		{
			vkFreeMemory(impl.ctx.handle(), impl.mem, nullptr);
			impl.mem = VK_NULL_HANDLE;
		}
		impl.size = 0;
		if (impl.buf)
		{
			vkDestroyBuffer(impl.ctx.handle(), impl.buf, nullptr);
			impl.buf = VK_NULL_HANDLE;
		}

		impl.initialized = false;
	}

	delete BUFFEROBJ_IMPL;
	pimpl = nullptr;
}

Context& BufferObject::manager (void)
{
	return BUFFEROBJ_IMPL->ctx;
}

void BufferObject::addPostEnsureAction(const EnsureAction &action)
{
	BUFFEROBJ_IMPL->pea.emplace_back(action);
}

void* BufferObject::map (void)
{
	// Shortcut for saving indirections
	auto &impl = *BUFFEROBJ_IMPL;

	// Only do something if not yet mapped
	if (!impl.mapped)
	{
		VkResult result = vkMapMemory(
			impl.ctx.handle(), impl.mem, 0, impl.size, 0, (void**)&(impl.mapped)
		);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"BufferObject::map", "vkMapMemory() failed!"
			));
	}

	// Done!
	return impl.mapped;
}

void BufferObject::unmap (void)
{
	// Shortcut for saving indirections
	auto& impl = *BUFFEROBJ_IMPL;

	if (impl.mapped)
	{
		vkUnmapMemory(impl.ctx.handle(), impl.mem);
		impl.mapped = nullptr;
	}
}

size_t BufferObject::size (void) const
{
	return (size_t)(BUFFEROBJ_IMPL->size);
}

void BufferObject::notifyWork (Handle fence)
{
	// Shortcut for saving one indirection
	auto &impl = *BUFFEROBJ_IMPL;

	ensure();
	BUFFEROBJ_IMPL->workFence = fence;
}

void BufferObject::ensure (void) const
{
	// Shortcuts for saving indirections
	auto &impl = *BUFFEROBJ_IMPL;

	if (impl.mapped && !(impl.memFlags&VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
	{
		// Make sure all host-side writes are visible to the device
		VkMappedMemoryRange mmr = {
			VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE, nullptr, impl.mem, 0, VK_WHOLE_SIZE
		};
		VkDevice dev = impl.ctx.handle();
		VkResult result = vkFlushMappedMemoryRanges(dev, 1, &mmr);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"BufferObject::ensure", "Unable to flush host-mapped memory!"
			));
	}
	if (impl.workFence)
	{
		// Wait for the work to complete
		VkResult result =
			vkWaitForFences(impl.ctx.handle(), 1, &(impl.workFence), true, UINT64_MAX);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"BufferObject::ensure", "Waiting on work fence failed!"
			));
		impl.workFence = VK_NULL_HANDLE;
	}

	// Execute registered actions and clear them for the next run
	for (auto& action : impl.pea)
		action();
	impl.pea.clear();
}

Handle BufferObject::handle (void) const
{
	return BUFFEROBJ_IMPL->buf;
}


////
// cgvu::StorageBuffer

struct StorageBuffer_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	VkBufferUsageFlags usageFlags = VK_NULL_HANDLE;

	BufferObject stagingBuf, buf;
	CommandBuffer cmd;

	unsigned idxWidth;

	StorageBuffer_impl(Context &ctx, VkBufferUsageFlags usageFlags)
		: ctx(ctx), usageFlags(usageFlags)
	{}
};
#define STGBUFFER_IMPL ((StorageBuffer_impl*)pimpl)

StorageBuffer::StorageBuffer() : pimpl(nullptr) {}

StorageBuffer::StorageBuffer(StorageBuffer &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

StorageBuffer::StorageBuffer(Context &ctx, StorageBufferType type) : pimpl(nullptr)
{
	// Determine usage flags from buffer type
	unsigned usageFlags;
	switch (type)
	{
		case SBT::VERTEX: usageFlags = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT; break;
		case SBT::INDEX:  usageFlags = VK_BUFFER_USAGE_INDEX_BUFFER_BIT; break;

		default:
			// This should not happen. Throw up...
			throw err::InvalidOpException(CGVU_LOGMSG(
				"StorageBuffer::<ctor>", "Invalid buffer type specified!"
			));
	}

	// Create implementation instance
	pimpl = new StorageBuffer_impl(ctx, usageFlags);
}

StorageBuffer::~StorageBuffer()
{
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *STGBUFFER_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Storage buffer cannot be used from here on anymore
			impl.started = false;

			// Free actual buffer memory.
			// ToDo: consider whether to also explicitely free staging buffer
			impl.ctx.deleteObject(impl.buf);

			impl.initialized = false;
		}
	}

	delete STGBUFFER_IMPL;
	pimpl = nullptr;
}

StorageBuffer& StorageBuffer::operator= (StorageBuffer &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

StorageBuffer::operator const BufferObject& (void) const
{
	return STGBUFFER_IMPL->buf;
}

unsigned StorageBuffer::indexWidth (void) const
{
	return STGBUFFER_IMPL->idxWidth;
}

void StorageBuffer::upload (void *data, size_t count, unsigned stride)
{
	// Shortcut for saving one indirection
	auto &impl = *STGBUFFER_IMPL;

	// Sanity checks
	unsigned size = unsigned(count)*stride;
	if (impl.initialized)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"StorageBuffer::upload", "Cannot upload new data to the buffer after it has"
			" been filled once!" // ToDo: implement it
		));

	// If index buffer, store index width
	if (impl.usageFlags & VK_BUFFER_USAGE_INDEX_BUFFER_BIT)
		if (stride == 4 || stride == 2)
			impl.idxWidth = stride;
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"StorageBuffer::upload", "Invalid integer width for index buffers - only"
				" 16 or 32 bit integers are allowed!"
			));

	// Stage data for uploading
	// - create staging buffer
	impl.stagingBuf = impl.ctx.createBuffer(
		size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
	);
	impl.initialized = true; // We have something to clean up from here on
	// - copy to staging buffer
	std::memcpy(impl.stagingBuf.map(), data, size);
	// - make sure the staging buffer is being unmapped before transfer
	impl.stagingBuf.addPostEnsureAction([&impl]() {
		impl.stagingBuf.unmap();
	});

	// Transfer data to GPU
	// - create GPU-side high performance buffer
	impl.buf = impl.ctx.createBuffer(
		size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | impl.usageFlags,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	);
	// - create transfer command buffer
	impl.cmd = impl.ctx.createCommandBuffer(TC::TRANSFER, true);
	impl.cmd.startRecording();
	impl.cmd.copyBuffer(impl.stagingBuf, impl.buf, {{0, 0, size}});
	impl.cmd.build();
	// - issue the transfer!
	impl.ctx.submitCommands(impl.cmd /* ToDo: try different priority: , 0.5f */);
	impl.buf.addPostEnsureAction([&impl]() {
		impl.ctx.deleteObject(impl.cmd);
		impl.ctx.deleteObject(impl.stagingBuf);
	});

	// We're done!
	impl.started = true;
}

size_t StorageBuffer::size (void) const
{
	return STGBUFFER_IMPL->buf.size();
}

const BufferObject& StorageBuffer::object (void) const
{
	return STGBUFFER_IMPL->buf;
}

Handle StorageBuffer::handle (void) const
{
	return STGBUFFER_IMPL->buf.handle();
}


////
// cgvu::UniformBuffer

struct UniformBuffer_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	BufferObject buf;
	char *mapped = nullptr;

	size_t stride, size=0;

	UniformBuffer_impl(Context &ctx) : ctx(ctx) {}
};
#define UNIFORMBUF_IMPL ((UniformBuffer_impl*)pimpl)

UniformBuffer::UniformBuffer() : pimpl(nullptr) {}

UniformBuffer::UniformBuffer(UniformBuffer &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

UniformBuffer::UniformBuffer(Context &ctx) : pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new UniformBuffer_impl(ctx);

	// Determine minimum possible uniform block alignement
	UNIFORMBUF_IMPL->stride =
		((VulkanDeviceInfo*)ctx.getDeviceInfo())
			->props.limits.minUniformBufferOffsetAlignment;
}

UniformBuffer::~UniformBuffer()
{
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *UNIFORMBUF_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Command buffer cannot be used from here on anymore
			impl.started = false;

			// Free actual buffer memory.
			// ToDo: consider whether to also explicitely free staging buffer
			impl.ctx.deleteObject(impl.buf);

			impl.initialized = false;
		}
	}

	delete UNIFORMBUF_IMPL;
	pimpl = nullptr;
}

UniformBuffer& UniformBuffer::operator= (UniformBuffer &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

UniformBuffer::operator const BufferObject& (void) const
{
	return UNIFORMBUF_IMPL->buf;
}

Uniform UniformBuffer::insert (size_t size)
{
	// Shortcut for saving one indirection
	auto &impl = *UNIFORMBUF_IMPL;

	// Sanity checks
	if (impl.started)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"UniformBuffer::insert", "Cannot insert a uniform block in a buffer that has"
			" already been built!"
		));
	if (size < 1)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"UniformBuffer::insert", "Uniform block size needs to be larger than 0!"
		));

	// Align block according to device limits
	size_t b = std::max(impl.stride, (size_t)32),
	       r = impl.size%b, p = (r>0) ? (b-r) : 0, o = impl.size+p;
	impl.size = o + size;

	// Return Uniform accessor
	return { *this, o, size };
}

void UniformBuffer::build (void)
{
	// Shortcut for saving one indirection
	auto &impl = *UNIFORMBUF_IMPL;

	// Sanity checks
	if (impl.started)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"UniformBuffer::build", "Buffer has already been built!"
		));
	if (impl.size < 1)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"UniformBuffer::build", "No uniform blocks have been defined!"
		));

	// Calculate stride between in-flight regions inside the buffer
	size_t b = std::max(impl.stride, (size_t)32),
	       r = impl.size%b, p = (r>0) ? (b-r) : 0;
	impl.stride = impl.size+p;

	// Create perma-mapped GPU buffer
	impl.buf = impl.ctx.createBuffer(
		impl.stride*impl.ctx.surface().getSwapchainLength(),
		VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
	);
	impl.initialized = true; // We have something to clean up from here on
	impl.mapped = (char*)impl.buf.map();

	// We're done!
	impl.started = true;
}

void* UniformBuffer::access (signed swapImageID)
{
	auto &impl = *UNIFORMBUF_IMPL;
	return
		impl.mapped + (
			swapImageID<0 ? impl.ctx.getSwapImageID() : ((unsigned)swapImageID)
		) * impl.stride;
}

const void* UniformBuffer::access (signed swapImageID) const
{
	auto &impl = *UNIFORMBUF_IMPL;
	return
		impl.mapped + (
			swapImageID<0 ? impl.ctx.getSwapImageID() : ((unsigned)swapImageID)
		) * impl.stride;
}

size_t UniformBuffer::stride (void) const
{
	return UNIFORMBUF_IMPL->stride;
}

Handle UniformBuffer::handle (void) const
{
	return UNIFORMBUF_IMPL->buf.handle();
}

const Context& UniformBuffer::context (void) const
{
	return UNIFORMBUF_IMPL->ctx;
}


////
// cgvu::Uniform

Uniform::Uniform() : container(uninitializedUB), offset(0), size(0) {}

Uniform::Uniform(UniformBuffer& container, size_t offset, size_t size)
	: container(container), offset(offset), size(size)
{}

Uniform::Uniform(const Uniform &other)
	: container(other.container), offset(other.offset), size(other.size)
{}

Uniform& Uniform::operator= (const Uniform &other)
{
	std::memcpy(this, &other, sizeof(Uniform));
	return *this;
}

void* Uniform::access (void)
{
	return ((char*)container.access()) + offset;
}

const void* Uniform::access (void) const
{
	return ((const char*)container.access()) + offset;
}

void Uniform::initialize (const void *data)
{
	// Copy to every per-frame instance
	unsigned scl = container.context().surface().getSwapchainLength();
	for (unsigned i=0; i<scl; i++)
		std::memcpy(((char*)container.access(i)) + offset, data, size);
}



//////
//
// Explicit template instantiations
//

template class cgvu::ManagedObject<BufferObject, Context>;
