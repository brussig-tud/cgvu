
//////
//
// Includes
//

// C++ STL
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <tuple>

// Vulkan
#include <vulkan/vulkan.h>

// PEGlib
#include <peglib.h>

// CGVU includes
#include "error.h"
#include "util/utils.h"
#include "log/logging.h"
#include "os/os.h"

// Module includes
#include "vulkan.h"

// Implemented header
#include "shaderprog.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;



//////
//
// Module-private globals and helper functions
//

// Anonymous namespace begin
namespace {

// Shaderpack definition grammar
static const std::string grammar =
	"Program <- (Stage %eol*)+\n"
	"Stage   <- %w Type %w ':' %w File\n"
	"Type    <- 'vert' / 'geom' / 'tesc' / 'tese' / 'frag' / 'comp'\n"
	"File    <- [0-9a-zA-Z-_'.' ]+\n"
	"~%w     <- [ \t]*"
	"~%eol   <- '\r\n' / '\r' / '\n'";

// Shaderpack file header
struct CGVuShaderPackHeader
{
	uint8_t magicID [4];
	uint32_t filesize;
};

// Shaderpack stage header
struct CGVuShaderPackStage
{
	uint32_t stage;
	uint32_t contentLength;
};

enum ShaderStage
{
	STAGE_VERT = 0,
	STAGE_GEOM = 1,
	STAGE_TESC = 2,
	STAGE_TESE = 3,
	STAGE_FRAG = 4,
	STAGE_COMP = 5,

	STAGE_COUNT
};

const VkShaderStageFlagBits vulkanShaderStageBits [STAGE_COUNT] = {
	VK_SHADER_STAGE_VERTEX_BIT,
	VK_SHADER_STAGE_GEOMETRY_BIT,
	VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT,
	VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
	VK_SHADER_STAGE_FRAGMENT_BIT,
	VK_SHADER_STAGE_COMPUTE_BIT
};

inline std::string shaderStageToString (ShaderStage stage)
{
	switch (stage)
	{
#define STRINGIFY(r) case r: return #r
		STRINGIFY(STAGE_VERT);
		STRINGIFY(STAGE_GEOM);
		STRINGIFY(STAGE_TESC);
		STRINGIFY(STAGE_TESE);
		STRINGIFY(STAGE_FRAG);
		STRINGIFY(STAGE_COMP);
#undef STRINGIFY
	default:
		return "STAGE_UNKNOWN";
	}
}

inline VkShaderStageFlags shaderFlagToVkFlag (ShaderStageFlags shFlag)
{
	switch (shFlag)
	{
		case SH::ALL_GFX: return VK_SHADER_STAGE_ALL_GRAPHICS;
		case SH::ALL: return VK_SHADER_STAGE_ALL;
		default:
		{
			VkShaderStageFlags result = 0;
			if (((unsigned)shFlag) & ((unsigned)SH::VERTEX))
				result = VK_SHADER_STAGE_VERTEX_BIT;
			if (((unsigned)shFlag) & ((unsigned)SH::GEOMETRY))
				result |= VK_SHADER_STAGE_GEOMETRY_BIT;
			if (((unsigned)shFlag) & ((unsigned)SH::TESSCTRL))
				result |= VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
			if (((unsigned)shFlag) & ((unsigned)SH::TESSEVAL))
				result |= VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
			if (((unsigned)shFlag) & ((unsigned)SH::FRAGMENT))
				result |= VK_SHADER_STAGE_FRAGMENT_BIT;
			if (((unsigned)shFlag) & ((unsigned)SH::COMPUTE))
				result |= VK_SHADER_STAGE_FRAGMENT_BIT;
			return result;
		}
	}
}

// Anonymous namespace end
}



//////
//
// Exception implementations
//

////
// cgvu::err::ShaderParseException

err::ShaderParseException::ShaderParseException(
	const std::string &reason, const log::EventSource& source
) noexcept
	: Exception(source), pimpl(new std::string(reason))
{}

err::ShaderParseException::ShaderParseException(
	const std::string &reason, log::EventSource&& source
) noexcept
	: Exception(std::move(source)), pimpl(new std::string(reason))
{}

err::ShaderParseException::~ShaderParseException() noexcept
{
	delete (std::string*)pimpl;
}

std::string err::ShaderParseException::getDescription(void) const
{
	return "ShaderParseException: " + (*(std::string*)pimpl);
}



//////
//
// Class implementations
//

////
// cgvu::ShaderProgram

struct Stage
{
	VkShaderModule handle = VK_NULL_HANDLE;
	std::vector<char> bytecode;
};

struct ShaderProg_impl
{
	ShaderProgSource stype;
	std::string filename;

	peg::parser prsr;

	Stage stages[STAGE_COUNT];
	std::vector<VkPipelineShaderStageCreateInfo> ci;
	VkDescriptorPool pool = VK_NULL_HANDLE;
	VkDescriptorSetLayout dsl = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descSets;

	std::string vertFile, geomFile, tescFile, teseFile, fragFile, compFile;

	std::stringstream err;

	bool loaded = false;

	Context *ctx = nullptr;

	ShaderProg_impl(ShaderProgSource stype, const std::string &filename)
		: stype(stype), filename(filename), onLog(err), onStage(this)
	{}

	static std::string onString (const peg::SemanticValues &sv)
	{
		return sv.token();
	}

	struct OnStage
	{
		ShaderProg_impl &parent;
		OnStage(ShaderProg_impl *parent) : parent(*parent) {}
		void operator() (const peg::SemanticValues &sv)
		{
			const std::string &type = sv[0].get<std::string>(),
			                  &file = sv[1].get<std::string>();

			if (type.compare("vert") == 0)
				parent.vertFile = file;
			else if (type.compare("geom") == 0)
				parent.geomFile = file;
			else if (type.compare("tesc") == 0)
				parent.tescFile = file;
			else if (type.compare("tese") == 0)
				parent.teseFile = file;
			else if (type.compare("frag") == 0)
				parent.fragFile = file;
			else if (type.compare("comp") == 0)
				parent.compFile = file;
		}
	} onStage;

	struct OnLog
	{
		std::stringstream& logstream;
		OnLog(std::stringstream& logstream) : logstream(logstream) {}
		void operator() (size_t line, size_t col, const std::string &msg)
		{
			logstream << "(" <<line<< "," <<col<< "): " << msg << std::endl;
		}
	} onLog;

	static void compileStage (std::vector<char> *bytecode, const std::string &infile)
	{
		bool goOn;
		std::vector<char> buf;

		// Handle reading GLSLC output
		cgvu::os::ChildProcess glslc(CGVU_GLSLC_EXECUTABLE);
		glslc.run("--target-env=vulkan1.1 --target-spv=spv1.3 -O -o - "+infile,
		          os::StandardStream::OUTERR);
		do {
			goOn = glslc.readStdOut(&buf, 3072);
			bytecode->insert(bytecode->end(), buf.begin(), buf.end());
		}
		while (goOn);

		// Handle GLSLC shutdown with retrieval of possible error messages
		if (glslc.wait() != EXIT_SUCCESS)
		{
			std::stringstream msg;
			msg << "GLSLC reports:" << std::endl << "   ";
			do {
				goOn = glslc.readStdErr(&buf, 3072);
				if (buf.size()) msg << buf.data();
			}
			while (goOn);
			throw err::ShaderParseException(std::move(msg.str()),
				CGVU_LOGMSG("ShaderProgram", "Unable to compile '"+infile+"'!")
			);
		}
	}

	void compileStages (void)
	{
		std::string basepath = util::pathWithoutName(filename);

		if (!vertFile.empty())
			compileStage(&(stages[STAGE_VERT].bytecode), basepath+'/'+vertFile);
		if (!geomFile.empty())
			compileStage(&(stages[STAGE_GEOM].bytecode), basepath+'/'+geomFile);
		if (!tescFile.empty())
			compileStage(&(stages[STAGE_TESC].bytecode), basepath+'/'+tescFile);
		if (!teseFile.empty())
			compileStage(&(stages[STAGE_TESE].bytecode), basepath+'/'+teseFile);
		if (!fragFile.empty())
			compileStage(&(stages[STAGE_FRAG].bytecode), basepath+'/'+fragFile);
		if (!compFile.empty())
			compileStage(&(stages[STAGE_COMP].bytecode), basepath+'/'+compFile);
	}
};
#define SHADERPROG_IMPL ((ShaderProg_impl*)pimpl)

ShaderProgram::ShaderProgram(
	ShaderProgSource source, const std::string &programFile, bool loadImmediatly
)
	: pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new ShaderProg_impl(source, programFile);
	ShaderProg_impl &impl = *SHADERPROG_IMPL;

	if (loadImmediatly)
		load();
}

ShaderProgram::~ShaderProgram()
{
	// Shortcut for saving one indirection
	auto &impl = *SHADERPROG_IMPL;

	if (impl.ctx)
	{
		// Destroy descriptor set pool
		VkDevice dev = impl.ctx->handle();
		if (impl.pool)
		{
			vkDestroyDescriptorPool(dev, impl.pool, nullptr);
			impl.pool = VK_NULL_HANDLE;
		}

		// Destroy descriptor set layouts
		if (impl.dsl)
		{
			vkDestroyDescriptorSetLayout(dev, impl.dsl, nullptr);
			impl.dsl = VK_NULL_HANDLE;
		}

		// Release shader modules
		for (Stage &stage : impl.stages) if (stage.handle)
		{
			vkDestroyShaderModule(dev, stage.handle, nullptr);
			stage.handle = VK_NULL_HANDLE;
		}
	}

	// De-allocate implementation instance
	if (pimpl)
		{ delete SHADERPROG_IMPL; pimpl=nullptr; }
}

Handle ShaderProgram::getStageCreationInfo (void) const
{
	return &(SHADERPROG_IMPL->ci);
}

Handle ShaderProgram::getDescSetLayout (void) const
{
	return &(SHADERPROG_IMPL->dsl);
}

Handle ShaderProgram::getDescSet (unsigned swapImageID) const
{
	return &(SHADERPROG_IMPL->descSets[swapImageID]);
}

void ShaderProgram::load (void)
{
	// Shortcut for saving one indirection
	auto &impl = *SHADERPROG_IMPL;

	// If people want to call load() not knowing if it was already loaded or not, they
	// will have to catch this exception...
	if (impl.loaded)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"ShaderProgram::load", "Program already loaded!"
		));

	// Determine the source type of the shader program
	std::ifstream file;
	size_t filesize;
	if (impl.stype == SHADER_SRC_AUTO)
	{
		file.open(impl.filename, std::ios::ate | std::ios::binary);
		if (!file.is_open())
			throw err::FileOpenException(
				err::FileOpenException::READ, impl.filename, CGVU_LOGMSG(
				"ShaderProgram::load", "Unable to determine shader program type!"
			));
		filesize = (size_t)file.tellg();

		if (filesize < sizeof(CGVuShaderPackHeader))
		{
			impl.stype = SHADER_SRC_DEF;
			file.close();
		}
		else
		{
			file.seekg(0);
			CGVuShaderPackHeader header;
			file.read((char*)&header, sizeof(CGVuShaderPackHeader));
			if (header.magicID[0] != 0x0C || header.magicID[1] != 0x47 ||
			    header.magicID[2] != 0x56 || header.magicID[3] != 0x75)
			{
				impl.stype = SHADER_SRC_DEF;
				file.close();
			}
			else
			{
				impl.stype = SHADER_SRC_SPK;
				if (header.filesize != filesize)
					throw err::ShaderParseException(
						"Shader pack file is corrupted - header filesize and actual file"
						" size don't match", CGVU_LOGMSG(
							"ShaderProgram::load", "Unable to load '"+impl.filename+"'!"
						)
					);
			}
		}
	}
	else if (impl.stype == SHADER_SRC_SPK)
	{
		file.open(impl.filename, std::ios::ate | std::ios::binary);
		if (!file.is_open())
			throw err::FileOpenException(
				err::FileOpenException::READ, impl.filename, CGVU_LOGMSG(
					"ShaderProgram::load", "Unable to load shader pack file!"
				));
		filesize = (size_t)file.tellg();

		if (filesize < sizeof(CGVuShaderPackHeader))
			throw err::ShaderParseException(
				"Shader pack file is corrupted - truncated header", CGVU_LOGMSG(
					"ShaderProgram::load", "Unable to load '"+impl.filename+"'!"
			));

		file.seekg(0);
		CGVuShaderPackHeader header;
		file.read((char*)&header, sizeof(CGVuShaderPackHeader));
		if (header.magicID[0] != 0x0C || header.magicID[1] != 0x47 ||
		    header.magicID[2] != 0x56 || header.magicID[3] != 0x75)
			throw err::ShaderParseException(
				"Shader pack file is corrupted - magic bytes mismatch", CGVU_LOGMSG(
					"ShaderProgram::load", "Unable to load '"+impl.filename+"'!"
				));

		if (header.filesize != filesize)
			throw err::ShaderParseException(
				"Shader pack file is corrupted - header filesize and actual file size"
				" don't match!", CGVU_LOGMSG("ShaderProgram::load",
				                             "Unable to read '"+impl.filename+"'")
			);
	}

	switch (impl.stype)
	{
		case SHADER_SRC_DEF:
		{
			file.open(impl.filename);
			if (!file.is_open())
				throw err::FileOpenException(
					err::FileOpenException::READ, impl.filename,
					CGVU_LOGMSG("ShaderProgram::load",
					            "Unable to read shader program definition!")
				);
			std::stringstream contents;
			contents << file.rdbuf();

			// Initialize PEG
			impl.prsr.log = impl.onLog;
			impl.prsr.load_grammar(grammar.c_str());

			// Define actions
			impl.prsr["Stage"] = impl.onStage;
			impl.prsr["Type"] = ShaderProg_impl::onString;
			impl.prsr["File"] = ShaderProg_impl::onString;

			// Parse
			impl.prsr.enable_packrat_parsing();
			if (!impl.prsr.parse((contents.str().c_str())))
			{
				std::stringstream err;
				err << "Program definition contains errors:" << std::endl
				    << "   " << impl.filename << std::move(impl.err.str());
				throw err::ShaderParseException(
					std::move(err.str()), CGVU_LOGMSG(
						"ShaderProgram::load", "Unable to parse '"+impl.filename+"'!"
					));
			}

			// Compile
			impl.compileStages();

			break;
		}
		case SHADER_SRC_SPK:
		{
			std::vector<char> buf;
			size_t bytesRead = sizeof(CGVuShaderPackHeader);
			while (bytesRead < filesize)
			{
				// Read header
				CGVuShaderPackStage stageHeader;
				file.read((char*)&stageHeader, sizeof(CGVuShaderPackStage));
				bytesRead += (size_t)file.gcount();

				// Read stage bytecode
				buf.resize(stageHeader.contentLength);
				file.read(buf.data(), buf.size());
				bytesRead += (size_t)file.gcount();

				// Assign bytecode to stage
				if (stageHeader.stage == STAGE_VERT)
					impl.stages[STAGE_VERT].bytecode = std::move(buf);
				else if (stageHeader.stage == STAGE_GEOM)
					impl.stages[STAGE_GEOM].bytecode = std::move(buf);
				else if (stageHeader.stage == STAGE_TESC)
					impl.stages[STAGE_TESC].bytecode = std::move(buf);
				else if (stageHeader.stage == STAGE_TESE)
					impl.stages[STAGE_TESE].bytecode = std::move(buf);
				else if (stageHeader.stage == STAGE_FRAG)
					impl.stages[STAGE_FRAG].bytecode = std::move(buf);
				else if (stageHeader.stage == STAGE_COMP)
					impl.stages[STAGE_COMP].bytecode = std::move(buf);
				else
					throw err::ShaderParseException(
						"Shaderpack file is corrupted!", CGVU_LOGMSG(
						"ShaderProgram::load", "Unable to load '"+impl.filename+"'!"
					));
			}
			break;
		}

		default:
			// This should not happen...
			throw std::runtime_error(CGVU_TXTMSG(
				"ShaderProgram::load", "Unable to load '"+impl.filename+"' - internal"
				" state corruption!", log::ERROR
			));
	}

	// Done!
	impl.loaded = true;
}

void ShaderProgram::build (
	Context &context, const UniformDescriptors &uniforms, bool autoload
)
{
	// Shortcut for saving one indirection
	auto& impl = *SHADERPROG_IMPL;

	if (!impl.loaded)
	{
		if (autoload)
			load();
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ShaderProgram::build", "Cannot build '" + impl.filename + "' - shader"
				" code has to be loaded first!"
			));
	}

	// Loop through stages and build Vulkan modules
	impl.ctx = &context;
	VkDevice dev = context.handle();
	VkShaderModuleCreateInfo smCreateInfo = {};
	for (ShaderStage s=(ShaderStage)0; s<STAGE_COUNT; s=(ShaderStage)(s+1))
	{
		if (!impl.stages[s].bytecode.empty())
		{
			// Create module object
			smCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
			smCreateInfo.codeSize = impl.stages[s].bytecode.size();
			smCreateInfo.pCode =
				reinterpret_cast<const uint32_t*>(impl.stages[s].bytecode.data());
			VkResult result = vkCreateShaderModule(
				dev, &smCreateInfo, nullptr, &impl.stages[s].handle
			);
			if (result != VK_SUCCESS)
				throw err::VulkanException(result, CGVU_LOGMSG(
					"ShaderProgram::build", "Cannot build '"+impl.filename+"' - failed"
					" to create Vulkan module for stage "+shaderStageToString(s)+"!"
				));

			// Prepare pipeline stage create info
			impl.ci.emplace_back();
			VkPipelineShaderStageCreateInfo &ci = impl.ci.back();
			ci.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			ci.stage = vulkanShaderStageBits[s];
			ci.module = impl.stages[s].handle;
			ci.pName = "main"; // ToDo: make this user-definable
		}
	}

	// Build descriptor set layout
	// - the actual layout
	std::vector<VkDescriptorSetLayoutBinding> lbs;
	lbs.reserve(uniforms.size());
	std::map<VkDescriptorType, unsigned> dtypes;
	for (const auto &uniform : uniforms)
	{
		switch (uniform.type)
		{
			case UFrm::INTEGRAL:
				lbs.emplace_back(VkDescriptorSetLayoutBinding{
					uniform.bindingID, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, uniform.count,
					shaderFlagToVkFlag(uniform.stages), nullptr
				});
				dtypes[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER]++;
				break;
			case UFrm::SAMPLER:
				lbs.emplace_back(VkDescriptorSetLayoutBinding{
					uniform.bindingID, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
					uniform.count, shaderFlagToVkFlag(uniform.stages), nullptr
				});
				dtypes[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER]++;
				break;

			default:
				// Should not happen... throw up
				throw err::InvalidOpException(CGVU_LOGMSG(
					"ShaderProgram::build", "Cannot build '"+impl.filename+"' - Unknown"
					" uniform type provided to descriptor set layout builder!"
				));
		}
	}
	VkDescriptorSetLayoutCreateInfo layoutCI = {};
	layoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutCI.bindingCount = (uint32_t)lbs.size();
	layoutCI.pBindings = lbs.data();
	VkResult result = vkCreateDescriptorSetLayout(dev, &layoutCI, nullptr, &(impl.dsl));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ShaderProgram::build", "Cannot build '"+impl.filename+"' - could not create"
			" descriptor set layout!"
		));
	// - linearly arranged copies to use with the following Vulkan API calls
	uint32_t numSwapchainImages = context.surface().getSwapchainLength();
	std::vector<VkDescriptorSetLayout> layouts(numSwapchainImages, impl.dsl);

	// Create descriptor sets
	// - infer pool sizes
	std::vector<VkDescriptorPoolSize> ps; ps.reserve(2);
	for (const auto &dtype : dtypes)
	{
		auto &newps = ps.emplace_back();
		newps.type = dtype.first;
		newps.descriptorCount = dtype.second*numSwapchainImages;
	}
	// - Common descriptor set pool
	VkDescriptorPoolCreateInfo poolCI = {};
	poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolCI.poolSizeCount = (uint32_t)ps.size();
	poolCI.pPoolSizes = ps.data();
	poolCI.maxSets = numSwapchainImages;
	result = vkCreateDescriptorPool(dev, &poolCI, nullptr, &(impl.pool));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ShaderProgram::build", "Cannot build '" + impl.filename + "' - could not"
			" create descriptor set pool!"
		));
	// - The actual descriptor sets
	VkDescriptorSetAllocateInfo descSetCI = {};
	descSetCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descSetCI.descriptorPool = impl.pool;
	descSetCI.descriptorSetCount = numSwapchainImages;
	descSetCI.pSetLayouts = layouts.data();
	impl.descSets.resize(numSwapchainImages);
	result = vkAllocateDescriptorSets(dev, &descSetCI, impl.descSets.data());
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ShaderProgram::build", "Cannot build '" + impl.filename + "' - could not"
			" create descriptor sets!"
		));
}

void ShaderProgram::save (const std::string& filename) const
{
	// Shortcut for saving one indirection
	auto &impl = *SHADERPROG_IMPL;

	// Prepare header
	CGVuShaderPackHeader header;
	header.magicID[0] = 0x0C; header.magicID[1] = 0x47;
	header.magicID[2] = 0x56; header.magicID[3] = 0x75;
	header.filesize = sizeof(CGVuShaderPackHeader);

	// Pass 1: calculate file size
	std::vector<CGVuShaderPackStage> stages;
	stages.reserve(STAGE_COUNT);
	for (ShaderStage s=(ShaderStage)0; s<STAGE_COUNT; s=(ShaderStage)(s+1))
	{
		if (!(impl.stages[s].bytecode.empty()))
		{
			CGVuShaderPackStage &newStage = stages.emplace_back();
			newStage.stage = s;
			newStage.contentLength = (uint32_t)impl.stages[s].bytecode.size();
			header.filesize += sizeof(CGVuShaderPackStage) + newStage.contentLength;
		}
	}

	// Open file for writing
	std::ofstream file(filename, std::ios::binary);
	if (!file.is_open())
		throw err::FileOpenException(
			err::FileOpenException::WRITE, filename,
			CGVU_LOGMSG("ShaderProgram::save", "Unable to write Shaderpack file!")
		);

	// Pass 2: write file
	file.write((const char*)&header, sizeof(CGVuShaderPackHeader));
	for (const CGVuShaderPackStage &s : stages)
	{
		file.write((char*)&s, sizeof(CGVuShaderPackStage));
		file.write(impl.stages[s.stage].bytecode.data(), s.contentLength);
	}
}

bool ShaderProgram::isBuilt (void) const
{
	return SHADERPROG_IMPL->ctx;
}

void ShaderProgram::updateDescriptorSets (const DescriptorSetUpdates &updates)
{
	// Shortcut for saving one indirection
	auto &impl = *SHADERPROG_IMPL;

	// Compile update information
	unsigned numUpdates = (unsigned)updates.size(),
	         scl = impl.ctx->surface().getSwapchainLength(),
	         numWrites = numUpdates*scl;
	std::vector<VkWriteDescriptorSet> writes(numWrites);
	std::vector<VkDescriptorBufferInfo> bis; bis.reserve(numWrites);
	std::vector<VkDescriptorImageInfo> iis; iis.reserve(numWrites);
	for (unsigned i=0; i<numUpdates; i++)
	{
		unsigned bindingID = updates[i].bindingID;

		switch (updates[i].type)
		{
			case UFrm::INTEGRAL:
			{
				const auto &uniform = *(Uniform*)updates[i].update;

				// Update info of this uniform for all in-flight frames
				size_t stride = uniform.container.stride();
				for (unsigned j=0; j<scl; j++)
				{
					// Shorthands
					VkWriteDescriptorSet &write = writes[i*scl + j];
					VkDescriptorBufferInfo &bi = bis.emplace_back();
					bi.buffer = uniform.container.handle();
					bi.offset = uniform.offset + size_t(j)*stride;
					bi.range = uniform.size;
					write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
					write.dstSet = impl.descSets[j];
					write.dstBinding = bindingID;
					write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
					write.descriptorCount = 1;
					write.pBufferInfo = &bi;
					write.pImageInfo = nullptr;
					write.pTexelBufferView = nullptr;
				}
				break;
			}
			case UFrm::SAMPLER:
			{
				const auto &update = *(SamplerUpdate*)updates[i].update;

				// Update info of this uniform for all in-flight frames
				for (unsigned j=0; j<scl; j++)
				{
					// Shorthands
					VkWriteDescriptorSet &write = writes[i*scl + j];
					VkDescriptorImageInfo &ii = iis.emplace_back();
					ii.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
					ii.imageView = update.image.view();
					ii.sampler = update.sampler;
					write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
					write.dstSet = impl.descSets[j];
					write.dstBinding = bindingID;
					write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					write.descriptorCount = 1;
					write.pBufferInfo = nullptr;
					write.pImageInfo = &ii;
					write.pTexelBufferView = nullptr;
				}
				update.image.ensure();
				break;
			}

			default:
				// Should not happen... throw up
				throw err::InvalidOpException(CGVU_LOGMSG(
					"ShaderProgram::updateDescriptorSets", "Unknown uniform type"
					" provided to descriptor set update builder!"
				));
		}
	}

	// Perform the update
	vkUpdateDescriptorSets(impl.ctx->handle(), numWrites, writes.data(), 0, nullptr);
}

Context& ShaderProgram::context (void) const
{
	// Shortcut for saving one indirection
	auto &impl = *SHADERPROG_IMPL;

	// Return context if available
	if (impl.ctx)
		return *(impl.ctx);
	throw err::InvalidOpException(CGVU_LOGMSG(
		"ShaderProgram::context", "Unable to access context - program not yet built!"
	));
}
