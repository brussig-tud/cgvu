
//////
//
// Includes
//

// C++ STL
#include <unordered_set>

// Vulkan
#include <vulkan/vulkan.h>

// CGVU includes
#include "error.h"
#include "util/utils.h"
#include "log/logging.h"

// Module includes
#include "vulkan.h"
#include "vulkan_internals.h"

// Implemented header
#include "cmdbuffer.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;



//////
//
// Helpers
//

// Anonymous namespace begin
namespace {

// Static int array. ToDo: somewhat unelegant...
static const unsigned allBuffers [] = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30, 31
};

VkPipelineBindPoint pipelineBindPointFromTaskClass (TaskClass taskClass)
{
	switch (taskClass)
	{
		case TC::RENDER: return VK_PIPELINE_BIND_POINT_GRAPHICS;
		case TC::COMPUTE: return VK_PIPELINE_BIND_POINT_COMPUTE;
		case TC::RAYTRACE: return VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR;
		default: return VK_PIPELINE_BIND_POINT_MAX_ENUM;
	}
}

// Anonymous namespace end
}



//////
//
// Class implementations
//


////
// cgvu::CommandBuffer

struct CmdRecord;

struct CommandBuffer_impl
{
	bool initialized=false, started=false, needsFrameSync=false, transient;

	Context &ctx;

	TaskClass taskClass;
	unsigned qfIdx;

	VkCommandPool pool;
	std::vector<VkCommandBuffer> buf;
	std::vector<VkSemaphore> sem;
	std::vector<VkFence> fnc;

	std::vector<CmdRecord*> cmds;
	std::unordered_set<CmdRecord*> postCmds;
	std::unordered_set<BufferObject*> dstBufs;
	std::unordered_set<ImageObject*> dstImgs;

	CommandBuffer_impl(Context &context, TaskClass taskClass, bool transient)
		: ctx(context), taskClass(taskClass), transient(transient)
	{
		VulkanDeviceInfo &dev = *((VulkanDeviceInfo*)context.getDeviceInfo());
		switch (taskClass)
		{
			case TC::RAYTRACE:
			case TC::RENDER:   qfIdx = dev.primaryGraphics; needsFrameSync = true; break;
			case TC::COMPUTE:  qfIdx = dev.primaryCompute; break;
			case TC::TRANSFER: qfIdx = dev.primaryTransfer; break;
			case TC::TRANSFER_GFX: qfIdx = dev.primaryGraphics; break;
			default:
				// This should not happen. Throw up...
				throw err::InvalidOpException(CGVU_LOGMSG(
					"CommandBuffer_impl::<ctor>", "Invalid task class specified!"
				));
			}
		pool = transient ? dev.qf[qfIdx].pool_transient : dev.qf[qfIdx].pool_static;
	}
};
#define CMDBUF_IMPL ((CommandBuffer_impl*)pimpl)

struct CmdRecordState
{
	bool isRendering = false;
	std::unordered_set<BufferObject*> dstBufs;
	std::unordered_set<ImageObject*> dstImgs;
	std::unordered_set<CmdRecord*> postCmds;
};

struct CmdRecord
{
	const CommandBuffer_impl &parent;

	CmdRecord(const CommandBuffer_impl &parent) : parent(parent) {}
	virtual ~CmdRecord() {};

	virtual void issueToBuffer(
		const Context &ctx, VkCommandBuffer buf, unsigned bufID
	) = 0;
	virtual void updateState (CmdRecordState &state) {}

	void issue (const unsigned *bufIDs, unsigned numBufIDs, CmdRecordState &state)
	{
		// Issue to all sub-buffers
		for (unsigned i=0; i<numBufIDs; i++)
			issueToBuffer(parent.ctx, parent.buf[bufIDs[i]], bufIDs[i]);
		// Do necessary state changes
		updateState(state);
	}

	virtual void onSubmit (unsigned bufID, VkFence cmdFence) {}
};

CommandBuffer::CommandBuffer(Context& context, TaskClass taskClass, bool transient)
{
	// Create implementation instance
	pimpl = new CommandBuffer_impl(context, taskClass, transient);
	auto &impl = *CMDBUF_IMPL;

	// Determine how many sub buffers (and additional per-frame semaphores) we need
	unsigned numSubBufs =
		  (   impl.needsFrameSync || taskClass==TC::TRANSFER_GFX
		   || (taskClass==TC::TRANSFER && !transient))
		? impl.ctx.surface().getSwapchainLength() : 1;
	std::vector<VkCommandBuffer> buf(numSubBufs);
	impl.sem.reserve(numSubBufs);
	impl.fnc.reserve(numSubBufs);
	impl.sem.emplace_back(impl.ctx.createSemaphore());
	impl.initialized = true; // We have something to clean up from here on
	impl.fnc.emplace_back(impl.ctx.createFence());
	for (unsigned i=1; i<numSubBufs; i++)
	{
		impl.sem.emplace_back(impl.ctx.createSemaphore());
		impl.fnc.emplace_back(impl.ctx.createFence());
	}

	// Allocate the buffers in the given pool
	VkCommandBufferAllocateInfo cbAlloc = {};
	cbAlloc.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cbAlloc.commandPool = impl.pool;
	cbAlloc.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cbAlloc.commandBufferCount = numSubBufs;
	VkResult result = vkAllocateCommandBuffers(impl.ctx.handle(), &cbAlloc, buf.data());
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"CommandBuffer::<ctor>", "Unable to allocate Vulkan command buffer objects!"
		));

	// Commit
	impl.buf = std::move(buf);
	impl.started = true;
}

void CommandBuffer::destroy (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CMDBUF_IMPL;

	// Handle cleanup
	if (impl.initialized)
	{
		// Command buffer cannot be used from here on anymore
		impl.started = false;

		// Explicitly free the Vulkan buffer objects
		vkFreeCommandBuffers(
			impl.ctx.handle(), impl.pool, (uint32_t)impl.buf.size(), impl.buf.data()
		);

		// Delete our command records on the heap
		for (auto &cmd : impl.cmds)
			delete cmd;

		// Delete the semaphores and fences
		for (VkSemaphore &s : impl.sem)
			impl.ctx.deleteSemaphore(s);
		for (VkFence &f : impl.fnc)
			impl.ctx.deleteFence(f);

		impl.initialized = false;
	}

	delete CMDBUF_IMPL;
	pimpl = nullptr;
}

void CommandBuffer::notifySubmit (unsigned subBufferIndex) const
{
	// Shortcut for saving one indirection
	auto &impl = *CMDBUF_IMPL;

	// Notify every command record that the buffer was submitted
	for (auto cmdRecord : impl.postCmds)
		cmdRecord->onSubmit(subBufferIndex, impl.fnc[subBufferIndex]);

	// Notify every destination buffer that work is being done on them
	for (auto buf : impl.dstBufs)
		buf->notifyWork(impl.fnc[subBufferIndex]);

	// Notify every image that work is being done on them
	for (auto img : impl.dstImgs)
		img->notifyWork(impl.fnc[subBufferIndex]);
}

Context& CommandBuffer::manager (void)
{
	return CMDBUF_IMPL->ctx;
}

void CommandBuffer::startRecording (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CMDBUF_IMPL;

	// Clear the command records and destination buffers
	impl.postCmds.clear();
	for (auto &cmd : impl.cmds)
		delete cmd;
	impl.cmds.clear();
	impl.dstBufs.clear();
	impl.dstImgs.clear();
}

void CommandBuffer::copyBuffer (const BufferObject &src, BufferObject &dst,
                                const std::vector<BufferRegion> &regions)
{
	// Our command record type
	struct CmdCopyBuffer : public CmdRecord
	{
		const BufferObject &src; BufferObject &dst;
		std::vector<VkBufferCopy> regs;
		 
		CmdCopyBuffer(const CommandBuffer_impl *parent, const BufferObject &src,
		              BufferObject &dst, const std::vector<BufferRegion> &regions)
			: CmdRecord(*parent), src(src), dst(dst)
		{
			regs.reserve(regions.size());
			for (const auto &reg : regions)
			{
				auto &newReg = regs.emplace_back();
				newReg.srcOffset = std::get<0>(reg);
				newReg.dstOffset = std::get<1>(reg);
				newReg.size      = std::get<2>(reg);
			}
		}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			vkCmdCopyBuffer(
				buf, src.handle(), dst.handle(), (uint32_t)regs.size(), regs.data()
			);
		}

		virtual void updateState (CmdRecordState &state)
		{
			// Remember to notify this buffer that work is being done on it
			state.dstBufs.insert(&dst);

			// Use this opportunity to wait until any pending buffer writes in the source
			// are visible to the device
			src.ensure();
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdCopyBuffer(CMDBUF_IMPL, src, dst, regions));
}

void CommandBuffer::copyBuffer (
	const BufferObject &src, ImageObject &dst,
	const std::vector<BufferImgRegion> &regions, bool preserveTexels
)
{
	// Our command record type
	struct CmdCopyBufToImg : public CmdRecord
	{
		const BufferObject &src; ImageObject &dst;
		std::vector<VkBufferImageCopy> regs;
		bool preserveTexels;
		 
		CmdCopyBufToImg(const CommandBuffer_impl *parent, const BufferObject &src,
		                ImageObject &dst, const std::vector<BufferImgRegion> &regions,
		                bool preserveTexels)
			: CmdRecord(*parent), src(src), dst(dst), preserveTexels(preserveTexels)
		{
			regs.reserve(regions.size());
			for (const auto &reg : regions)
			{
				auto &newReg = regs.emplace_back();
				newReg.bufferOffset = reg.bufOffset;
				newReg.bufferRowLength = reg.bufRowStride;
				newReg.bufferImageHeight = reg.bufLayerStride;
				newReg.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				newReg.imageSubresource.mipLevel = 0;
				newReg.imageSubresource.baseArrayLayer = 0;
				newReg.imageSubresource.layerCount = 1;
				newReg.imageOffset = {
					(int32_t)reg.imgOffset[0], (int32_t)reg.imgOffset[1],
					(int32_t)reg.imgOffset[2]
				};
				newReg.imageExtent = {
					reg.imgExtent[0], reg.imgExtent[1], reg.imgExtent[2]
				};
			}
		}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// First, transition the target image to transfer-optimal layout
			VkImageMemoryBarrier transitionBarrier{};
			transitionBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			transitionBarrier.oldLayout = preserveTexels ?
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL : VK_IMAGE_LAYOUT_UNDEFINED;
			transitionBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			transitionBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			transitionBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			transitionBarrier.image = dst.handle();
			transitionBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			transitionBarrier.subresourceRange.baseMipLevel = 0;
			transitionBarrier.subresourceRange.levelCount = 1;
			transitionBarrier.subresourceRange.baseArrayLayer = 0;
			transitionBarrier.subresourceRange.layerCount = 1;
			transitionBarrier.srcAccessMask = preserveTexels ?
				VK_ACCESS_MEMORY_WRITE_BIT : 0;
			transitionBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			vkCmdPipelineBarrier(
				buf, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1,
				&transitionBarrier
			);

			// Execute the copy
			vkCmdCopyBufferToImage(
				buf, src.handle(), dst.handle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				(uint32_t)regs.size(), regs.data()
			);

			// Next, transition the target image back to ideal layout for sampling
			transitionBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			transitionBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			transitionBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			transitionBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			vkCmdPipelineBarrier(
				buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
				0, 0, nullptr, 0, nullptr, 1, &transitionBarrier
			);
		}

		virtual void updateState (CmdRecordState &state)
		{
			// Remember to notify this image that work is being done on it
			state.dstImgs.insert(&dst);

			// Use this opportunity to wait until any pending buffer writes in the source
			// are visible to the device
			src.ensure();
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdCopyBufToImg(
		CMDBUF_IMPL, src, dst, regions, preserveTexels
	));
}

void CommandBuffer::copyRenderImage (const ImageObject *srcChain, BufferObject *dstChain,
                                     const std::vector<BufferImgRegion> &regions,
                                     bool preserveSrcLayout)
{
	// Our command record type
	struct CmdCopyImgToBuf : public CmdRecord
	{
		const ImageObject *src; BufferObject *dst;
		std::vector<VkBufferImageCopy> regs;
		bool hasDepth, hasStencil, isColor, preserveSrcLayout;
		 
		CmdCopyImgToBuf(
			const CommandBuffer_impl *parent, const ImageObject *src, BufferObject *dst,
			const std::vector<BufferImgRegion> &regions, bool preserveSrcLayout
		)
			: CmdRecord(*parent), src(src), dst(dst), hasDepth(src->hasDepth()),
			  hasStencil(src->hasStencil()), isColor(!(hasDepth || hasStencil)),
			  preserveSrcLayout(preserveSrcLayout)
		{
			regs.reserve(regions.size());
			for (const auto &reg : regions)
			{
				auto &newReg = regs.emplace_back();
				newReg.bufferOffset = reg.bufOffset;
				newReg.bufferRowLength = reg.bufRowStride;
				newReg.bufferImageHeight = reg.bufLayerStride;
				newReg.imageSubresource.aspectMask =
					  (isColor ? VK_IMAGE_ASPECT_COLOR_BIT : 0)
					| (hasDepth ? VK_IMAGE_ASPECT_DEPTH_BIT : 0)/*
					| (hasStencil ? VK_IMAGE_ASPECT_STENCIL_BIT : 0)*/;
				newReg.imageSubresource.mipLevel = 0;
				newReg.imageSubresource.baseArrayLayer = 0;
				newReg.imageSubresource.layerCount = 1;
				newReg.imageOffset = {
					(int32_t)reg.imgOffset[0], (int32_t)reg.imgOffset[1],
					(int32_t)reg.imgOffset[2]
				};
				newReg.imageExtent = {
					reg.imgExtent[0], reg.imgExtent[1], reg.imgExtent[2]
				};
			}
		}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned bufID)
		{
			// Transition the target image to transfer-optimal layout
			VkImageMemoryBarrier transitionBarrier{};
			transitionBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			if (isColor)
				transitionBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			else if (hasDepth && hasStencil)
				transitionBarrier.oldLayout =
					VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			else if (hasDepth)
				transitionBarrier.oldLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
			else
				transitionBarrier.oldLayout = VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL;
			transitionBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			transitionBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			transitionBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			transitionBarrier.image = src[bufID].handle();
			transitionBarrier.subresourceRange.aspectMask =
				  (isColor ? VK_IMAGE_ASPECT_COLOR_BIT : 0)
				| (hasDepth ? VK_IMAGE_ASPECT_DEPTH_BIT : 0)
				| (hasStencil ? VK_IMAGE_ASPECT_STENCIL_BIT : 0);
			transitionBarrier.subresourceRange.baseMipLevel = 0;
			transitionBarrier.subresourceRange.levelCount = 1;
			transitionBarrier.subresourceRange.baseArrayLayer = 0;
			transitionBarrier.subresourceRange.layerCount = 1;
			transitionBarrier.srcAccessMask =/*
				  (isColor ? */VK_ACCESS_MEMORY_WRITE_BIT/* : 0)
				| (  (hasDepth||hasStencil)
				   ? VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT : 0)*/;
			transitionBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			vkCmdPipelineBarrier(
				buf, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT/*
				isColor ? VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT :
				          VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT*/,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0, 0, nullptr, 0, nullptr, 1, &transitionBarrier
			);

			// Execute the copy
			vkCmdCopyImageToBuffer(
				buf, src[bufID].handle(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				dst[bufID].handle(), (uint32_t)regs.size(), regs.data()
			);

			// If requested, transition the source image back to whatever layout it was
			// before
			if (preserveSrcLayout)
			{
				transitionBarrier.newLayout = transitionBarrier.oldLayout;
				transitionBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				transitionBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				transitionBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
				vkCmdPipelineBarrier(buf, VK_PIPELINE_STAGE_TRANSFER_BIT,
				                     VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0,
				                     nullptr, 1, &transitionBarrier);
			}
		}

		virtual void updateState (CmdRecordState &state)
		{
			// We need to be notified once the command buffer is actually being submitted
			state.postCmds.insert(this);
		}

		virtual void onSubmit (unsigned bufID, VkFence cmdFence)
		{
			// Remember to notify the buffer that work is being done on it
			dst[bufID].notifyWork(cmdFence);
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdCopyImgToBuf(
		CMDBUF_IMPL, srcChain, dstChain, regions, preserveSrcLayout
	));
}

void CommandBuffer::bindPipeline (const Pipeline& pipeline)
{
	// Our command record type
	struct CmdBindPipeline : public CmdRecord
	{
		const Pipeline &pl;
		const ShaderProgram &sh;

		CmdBindPipeline(const CommandBuffer_impl *parent, const Pipeline &pl)
			: CmdRecord(*parent), pl(pl), sh(pl.shaderProg()) {}

		virtual void issueToBuffer (
			const Context &ctx, VkCommandBuffer buf, unsigned bufID
		)
		{
			// Compile command information
			auto &rp = pl.renderPass();
			VkRenderPassBeginInfo rpInfo = {};
			rpInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			rpInfo.renderPass = rp.handle();
			rpInfo.framebuffer = pl.fbi(bufID);
			rpInfo.renderArea.offset = { 0, 0 };
			rpInfo.renderArea.extent = {ctx.getViewportWidth(), ctx.getViewportHeight()};
			VkClearValue clearValues[2];
			clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
			if (rp.hasDepthStencil())
			{
				rpInfo.clearValueCount = 2;
				clearValues[1].depthStencil = {1.0f, 0};
			}
			else
				rpInfo.clearValueCount = 1;
			rpInfo.pClearValues = clearValues;
			VkPipelineBindPoint bindpoint =
				pipelineBindPointFromTaskClass(parent.taskClass);

			// Issue relevant commands
			vkCmdBeginRenderPass(buf, &rpInfo, VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline(buf, bindpoint, pl.handle());

			// Handle dynamic state
			VkViewport *viewport = (VkViewport*)ctx.getViewport();
			VkRect2D scissor = { 0, 0, uint32_t(viewport->width),
			                     uint32_t(viewport->height) };
			vkCmdSetViewport(buf, 0, 1, viewport);
			vkCmdSetScissor(buf, 0, 1, &scissor);

			// Bind descriptor sets
			vkCmdBindDescriptorSets(
				buf, bindpoint, pl.getLayout(), 0, 1, sh.getDescSet(bufID), 0, nullptr
			);
		}

		virtual void updateState (CmdRecordState &state)
		{
			// Indicate we are currently rendering
			state.isRendering = true;
		}
	};

	// Sanity check
	if (!pipeline.isBuilt())
		throw err::InvalidOpException(CGVU_LOGMSG(
			"CommandBuffer::bindPipeline", "Pipeline has not yet been built!"
		));

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdBindPipeline(CMDBUF_IMPL, pipeline));
}

void CommandBuffer::bindBuffer (unsigned bindingID, const BufferObject &buffer,
                                size_t offset)
{
	// Our command record type
	struct CmdBindBuffer : public CmdRecord
	{
		const BufferObject &bufObj;
		unsigned binding;
		size_t offset;

		CmdBindBuffer(
			const CommandBuffer_impl *parent, const BufferObject &buf, unsigned binding,
			size_t offset
		)
			: CmdRecord(*parent),  bufObj(buf), binding(binding), offset(offset)
		{}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// Compile command information
			VkBuffer vertexBuffers[] = { bufObj.handle() };
			VkDeviceSize offsets[] = { offset };

			// Issue the command
			vkCmdBindVertexBuffers(buf, binding, 1, vertexBuffers, offsets);
		}

		virtual void updateState (CmdRecordState&)
		{
			// We don't update any state, but use this opportunity to wait until the
			// buffer contents have been uploaded if they aren't already.
			bufObj.ensure();
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdBindBuffer(CMDBUF_IMPL, buffer, bindingID, offset));
}

void CommandBuffer::bindBuffers (unsigned firstBindingID, const BufferBindCmds& buffers)
{
	// Our command record type
	struct CmdBindBuffers : public CmdRecord
	{
		unsigned binding;
		std::vector<VkBuffer> bufs;
		std::vector<VkDeviceSize> offsets;
		std::unordered_set<const BufferObject*> bufObjects;

		CmdBindBuffers(const CommandBuffer_impl *parent, unsigned firstBindingID,
		               const BufferBindCmds &buffers)
			: CmdRecord(*parent), binding(firstBindingID)
		{
			bufs.reserve(buffers.size());
			offsets.reserve(buffers.size());
			for (auto &buf : buffers)
			{
				bufs.emplace_back(buf.buffer->handle());
				offsets.emplace_back(buf.offset);
				bufObjects.insert(buf.buffer);
			}
		}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// Issue the command
			vkCmdBindVertexBuffers(
				buf, binding, (uint32_t)bufs.size(), bufs.data(), offsets.data()
			);
		}

		virtual void updateState (CmdRecordState&)
		{
			// We don't update any state, but use this opportunity to wait until the
			// contents of all buffers have been uploaded if they aren't already.
			for (auto buf : bufObjects)
				buf->ensure();
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdBindBuffers(CMDBUF_IMPL, firstBindingID,
	                                                  buffers));
}

void CommandBuffer::bindIndexBuffer (const BufferObject &indexBuffer, size_t indexWidth)
{
	// Our command record type
	struct CmdBindIndexBuffer : public CmdRecord
	{
		const BufferObject &bufObj;
		VkIndexType indexType;

		CmdBindIndexBuffer(const CommandBuffer_impl *parent, const BufferObject &buf,
		                   VkIndexType indexType)
			: CmdRecord(*parent), bufObj(buf), indexType(indexType)
		{}

		virtual void issueToBuffer (const Context &ctx, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// Issue the command
			vkCmdBindIndexBuffer(buf, bufObj.handle(), 0, indexType);
		}

		virtual void updateState (CmdRecordState&)
		{
			// We don't update any state, but use this opportunity to wait until the
			// buffer contents have been uploaded if they aren't already.
			bufObj.ensure();
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdBindIndexBuffer(
		CMDBUF_IMPL, indexBuffer,
		(indexWidth == 4) ? VK_INDEX_TYPE_UINT32 : VK_INDEX_TYPE_UINT16
	));
}

void CommandBuffer::draw (unsigned numVertices, unsigned numInstances,
                          unsigned startVertex, unsigned startInstance)
{
	// Our command record type
	struct CmdDraw : public CmdRecord
	{
		unsigned vCound, iCount, vStart, iStart;

		CmdDraw(const CommandBuffer_impl *parent, unsigned vCound, unsigned iCount,
		        unsigned vStart, unsigned iStart)
			: CmdRecord(*parent), vCound(vCound), iCount(iCount), vStart(vStart),
			  iStart(iStart)
		{}

		virtual void issueToBuffer (const Context&, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// Issue the command
			vkCmdDraw(buf, vCound, iCount, vStart, iStart);
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdDraw(
		CMDBUF_IMPL, numVertices, numInstances, startVertex, startInstance
	));
}

void CommandBuffer::drawIndexed (
	unsigned numIndices, unsigned numInstances, unsigned startIndex, unsigned reserved1,
	unsigned reserved2
)
{
	// Our command record type
	struct CmdDrawIndexed : public CmdRecord
	{
		unsigned iCount, insCount, iStart;

		CmdDrawIndexed(const CommandBuffer_impl *parent, unsigned iCount,
		               unsigned insCount, unsigned iStart)
			: CmdRecord(*parent), iCount(iCount), insCount(insCount), iStart(iStart)
		{}

		virtual void issueToBuffer (const Context&, VkCommandBuffer buf,
		                            unsigned /* DontCare */)
		{
			// Issue the command
			vkCmdDrawIndexed(buf, iCount, insCount, iStart, 0, 0);
		}
	};

	// Record it!
	CMDBUF_IMPL->cmds.emplace_back(new CmdDrawIndexed(
		CMDBUF_IMPL, numIndices, numInstances, startIndex
	));
}

void CommandBuffer::build (void)
{
	// Shortcut for saving one indirection
	auto &impl = *CMDBUF_IMPL;

	// Decide whether to switch every sub-buffer to recording mode (for static buffers)
	// or only the current/sole one for transient buffers
	VkCommandBufferBeginInfo bi = {};
	bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	bi.flags = impl.transient ? VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : 0;
	const unsigned
		sbidx = isRenderTask(impl.taskClass) ? impl.ctx.getSwapImageID() : 0,
		numBufs = impl.transient ? 1 : impl.ctx.surface().getSwapchainLength();
	const unsigned *bufIDs = impl.transient ? allBuffers+sbidx : allBuffers;
	for (unsigned i=0; i<numBufs; i++)
	{
		VkResult result = vkBeginCommandBuffer(impl.buf[bufIDs[i]], &bi);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"CommandBuffer::build", "Unable to transition to recording state!"
			));
	}

	// Issue every recorded command
	CmdRecordState state;
	for (auto cmd : impl.cmds)
		cmd->issue(bufIDs, numBufs, state);

	// Implicit stop of last recorded render pass
	if (state.isRendering)
	{
		for (unsigned i=0; i<numBufs; i++)
			vkCmdEndRenderPass(impl.buf[bufIDs[i]]);
		state.isRendering = false;
	}

	// Switch every sub-buffer to ready mode
	for (unsigned i=0; i<numBufs; i++)
	{
		VkResult result = vkEndCommandBuffer(impl.buf[bufIDs[i]]);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"CommandBuffer::build", "Unable to transition to ready state!"
			));
	}

	// Commit list of destination buffers
	impl.postCmds = std::move(state.postCmds);
	impl.dstBufs = std::move(state.dstBufs);
	impl.dstImgs = std::move(state.dstImgs);
}

Handle CommandBuffer::handle (unsigned subBufferIndex) const
{
	return CMDBUF_IMPL->buf[subBufferIndex];
}

Handle CommandBuffer::semaphore (unsigned currentFrame) const
{
	return CMDBUF_IMPL->sem[currentFrame];
}

Handle CommandBuffer::fence (unsigned currentFrame) const
{
	return CMDBUF_IMPL->fnc[currentFrame];
}

unsigned CommandBuffer::queueFamilyIndex(void) const
{
	return CMDBUF_IMPL->qfIdx;
}

bool CommandBuffer::transient (void) const
{
	return CMDBUF_IMPL->transient;
}

TaskClass CommandBuffer::taskClass(void) const
{
	return CMDBUF_IMPL->taskClass;
}

bool CommandBuffer::needsFrameSync (void) const
{
	return CMDBUF_IMPL->needsFrameSync;
}



//////
//
// Explicit template instantiations
//

template class cgvu::ManagedObject<CommandBuffer, Context>;
