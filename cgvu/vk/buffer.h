
#ifndef __BUFFER_H__
#define __BUFFER_H__


//////
//
// Includes
//

// C++ STL
#include <vector>
#include <functional>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/managed.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Vulkan abstraction main classes
class Context;
class ShaderProgram;
class Texture;

// Operational abstractions provided by cgvu::Context
class CommandBuffer;

// Classes in this header
class StorageBuffer;
class Uniform;
template<class Struct> class TypedUniform;



//////
//
// Enums
//

/** @brief The possible types of storage buffers. */
enum class StorageBufferType
{
	/** @brief The buffer will store vertex attributes. */
	VERTEX,

	/** @brief The buffer will store vertex indices. */
	INDEX,

	/** @brief The buffer will be a generic shader storage buffer. */
	STORAGE
};
/** @brief Shorthand for @ref cgvu::StorageBufferType . */
typedef StorageBufferType SBT;



//////
//
// Class definitions
//

/** @brief Encapsulates a buffer object making data available on the GPU. */
class CGVU_API BufferObject : public ManagedObject<BufferObject, Context>
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class CommandBuffer;
	friend class StorageBuffer;
	friend class Texture;

	// Explicit inheritance
	using ManagedObject::ManagedObject;
	using ManagedObject::operator=;
	using ManagedObject::operator==;


	////
	// Exported types

	/** @brief Callback functor type for use with @ref #addEnsureAction . */
	typedef std::function<void(void)> EnsureAction;


protected:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the buffer object in the given context for the given usage.
	 */
	BufferObject (Context &ctx, size_t size, unsigned usageFlags, unsigned memProps);


	////
	// Methods

	/** @brief Handles deregistering and resource deallocation. */
	void destroy (void);

	/**
	 * @brief Adds some action to be executed at the end of the next @ref #ensure call.
	 *
	 * @note This action gets executed after the @a next call to @ref #ensure @a only !
	 */
	void addPostEnsureAction (const EnsureAction &action);


public:

	////
	// Interface: ManagedObject

	/**
	 * @brief
	 *		Returns a reference to the managing context (see @ref
	 *		cgvu::ManagedObject::manager ).
	 */
	virtual Context& manager (void) override;


	////
	// Methods

	/**
	 * @brief
	 *		Maps the buffer into host address space and returns a pointer to the
	 *		beginning of the mapped range (only works for host-visible buffers).
	 */
	void* map (void);

	/** @brief Unmaps the buffer from host address space. */
	void unmap (void);

	/** @brief Reports the usable size of the buffer in bytes. */
	size_t size (void) const;

	/**
	 * @brief
	 *		For use by command buffers - tells the buffer that it was the target of some
	 *		command that might change buffer contents.
	 */
	void notifyWork (Handle fence);

	/**
	 * @brief
	 *		Ensures any pending buffer writes are complete and executes registered
	 *		cleanup actions after any pending write. When binding the buffer via CGVu's
	 *		@ref cgvu::CommandBuffer facilities, there is no need to call this
	 *		explicitely.
	 */
	void ensure (void) const;

	/** @brief Returns internal handle to the underlying buffer object. */
	Handle handle (void) const;
};


/**
 * @brief
 *		Encapsulates a buffer ment for holding large amounts of data to be processed by
 *		the GPU, e.g. mesh vertices.
 */
class CGVU_API StorageBuffer
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;
	friend class CommandBuffer;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Methods

	/**
	 * @brief
	 *		Returns the width of indices if the buffer is an index buffer (undefined if
	 *		not).
	 */
	unsigned indexWidth (void) const;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the storage buffer will not
	 *		be usable until a functioning instance is moved in using @link
	 *		#operator=(StorageBuffer&&) move assignment @endlink .
	 */
	StorageBuffer();

	/** @brief Not copy-constructible. */
	StorageBuffer(const StorageBuffer &) = delete;

	/** @brief The move constructor. */
	StorageBuffer(StorageBuffer &&other);

	/** @brief Constructs a storage buffer of the given type in the given context. */
	StorageBuffer(Context &ctx, StorageBufferType type);

	/** @brief The destructor. */
	virtual ~StorageBuffer();


	////
	// Operators

	/** @brief Not copy-assignable. */
	StorageBuffer& operator= (const StorageBuffer &) = delete;

	/** @brief Move assignment. */
	StorageBuffer& operator= (StorageBuffer &&other);

	/** @brief References the encapsulated buffer object. */
	operator const BufferObject& (void) const;


	////
	// Methods

	/**
	 * @brief
	 *		Returns a pointer to the staging area the host CPU can write to, making it
	 *		possible to avoid host-side data duplication while the upload is pending.
	 */
	//void* stage (size_t count, const BufferInputBindingDesc &bindingDesc);

	/**
	 * @brief
	 *		Returns a typed pointer to the staging area the host CPU can write to, making
	 *		it possible to avoid host-side data duplication while the upload is pending.
	 */
	//template <class Elem>
	//Elem* stage (size_t count)
	//{
	//	return (Elem*)stage(count, Elem::getBindingDesc());
	//}

	/** @brief Initiates upload of staged data. */
	//void upload (void);

	/** @brief Uploads the given data to the buffer. */
	void upload (void *data, size_t count, unsigned stride);

	/** @brief Uploads the given typed data the buffer. */
	template <class Elem>
	void upload (const std::vector<Elem> &elems)
	{
		upload((void*)elems.data(), elems.size(), sizeof(Elem));
	}

	/** @brief Reports the usable size of the buffer in bytes. */
	size_t size (void) const;

	/** @brief Returns the underlying buffer object that holds the data. */
	const BufferObject& object (void) const;

	/**
	 * @brief
	 *		Returns the Vulkan objet handle of the underlying @link cgvu::BufferObject
	 *		buffer object @endlink .
	 */
	Handle handle (void) const;
};


/**
 * @brief
 *		Encapsulates a buffer ment for storing small amounts of data for use as uniform
 *		parameteres for shaders.
 */
class CGVU_API UniformBuffer
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the uniform buffer will not
	 *		be usable until a functioning instance is moved in using @link
	 *		#operator=(UniformBuffer&&) move assignment @endlink .
	 */
	UniformBuffer();

	/** @brief Not copy-constructible. */
	UniformBuffer(const UniformBuffer &) = delete;

	/** @brief The move constructor. */
	UniformBuffer(UniformBuffer&&other);

	/** @brief Constructs the uniform buffer in the given context. */
	UniformBuffer(Context &ctx);

	/** @brief The destructor. */
	virtual ~UniformBuffer();


	////
	// Operators

	/** @brief Not copy-assignable. */
	UniformBuffer& operator= (const UniformBuffer &) = delete;

	/** @brief Move assignment. */
	UniformBuffer& operator= (UniformBuffer &&other);

	/** @brief References the encapsulated buffer object. */
	operator const BufferObject& (void) const;


	////
	// Methods

	/**
	 * @brief
	 *		Inserts the given opaque block into the uniform buffer and returns an
	 *		accessor.
	 */
	Uniform insert (size_t size);

	/**
	 * @brief Inserts the given struct into the uniform buffer and returns an accessor.
	 */
	template <class Struct>
	TypedUniform<Struct> insert ()
	{
		return insert(sizeof(Struct));
	}

	/**
	 * @brief Builds the buffer on the GPU. Call this after all uniforms are inserted.
	 */
	void build (void);

	/**
	 * @brief
	 *		Returns a pointer to the buffer memory for the given swapchain frame, or for
	 *		the current swapchain frame according to the context of the buffer when
	 *		passing a negative frame ID.
	 */
	void* access (signed swapImageID=-1);

	/**
	 * @brief
	 *		Returns a read-only pointer to the buffer memory for the given swapchain
	 *		frame, or for the current swapchain frame according to the context of the
	 *		buffer when passing a negative frame ID.
	 */
	const void* access (signed swapImageID=-1) const;

	/** @brief Reports the stride between in-flight frame regions inside the buffer. */
	size_t stride (void) const;

	/** @brief Retrieves the internal Vulkan handle of the encapsulated buffer object. */
	Handle handle (void) const;

	/**
	 * @brief Retrieves the context the buffer created its internal Vulkan objects in.
	 */
	const Context& context (void) const;
};


/** @brief Wraps access to a single uniform block inside a uniform buffer. */
class CGVU_API Uniform
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class UniformBuffer;
	friend class ShaderProgram;


protected:

	////
	// Data members

	/** @brief The buffer where the uniform is stored. */
	UniformBuffer &container;

	/** @brief The offset inside the buffer where the uniform is stored at. */
	size_t offset;

	/** @brief The size of the uniform block inside the buffer in bytes. */
	size_t size;


	////
	// Object construction / destruction

	/** @brief Constructs the Uniform at the given offset in the given buffer. */
	Uniform(UniformBuffer &container, size_t offset, size_t size);


public:

	////
	// Object construction / destruction

	/** @brief The default constructor. */
	Uniform();

	/** @brief The copy constructor. */
	Uniform(const Uniform &other);


	////
	// Operators

	/** @brief Copy assignment. */
	Uniform& operator= (const Uniform &other);

	/** @brief Accesses the wrapped uniform block as an opaque pointer. */
	operator void* (void) { return access(); }

	/** @brief Accesses the wrapped uniform block as an opaque read-only pointer. */
	operator const void* (void) const { return access(); }


	////
	// Methods

	/** @brief Accesses the wrapped uniform block as an opaque pointer. */
	void* access (void);

	/** @brief Accesses the wrapped uniform block as an opaque read-only pointer. */
	const void* access (void) const;

	/**
	 * @brief
	 *		Writes the given data to all per-frame instances of the uniform. See @ref
	 *		cgvu::UniformBuffer::access for a rough explanation of uniform per-frame
	 *		instances.
	 */
	void initialize (const void *data);
};


/** @brief Wraps access to a single uniform block inside a uniform buffer. */
template <class T>
class TypedUniform : public Uniform
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class UniformBuffer;


protected:

	////
	// Object construction / destruction

	/** @brief Constructs the TypedUniform from the given untyped Uniform. */
	TypedUniform(const Uniform &base) : Uniform(base) {}


public:

	////
	// Object construction / destruction

	/** @brief The default constructor. */
	TypedUniform() {}

	/** @brief The copy constructor. */
	TypedUniform(const TypedUniform& other)
		: Uniform(other.container, other.offset, other.size)
	{}


	////
	// Operators

	/** @brief Copy assignment. */
	TypedUniform& operator= (const TypedUniform &other)
	{
		(*(Uniform*)this) = (*(Uniform*)&other);
		return *this;
	}

	/** @brief Accesses the wrapped uniform. */
	T& set (void) { return *(T*)access(); }

	/** @brief Accesses the wrapped uniform in read-only mode. */
	const T& get (void) const { return *(const T*)access(); }

	/** @brief See @ref cgvu::Uniform::initialize . */
	void initialize (const T &data) { Uniform::initialize(&data); }
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __BUFFER_H__
