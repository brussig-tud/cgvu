
#ifndef __VULKAN_H__
#define __VULKAN_H__


//////
//
// Includes
//

// C++ STL
#include <string>
#include <set>
#include <map>
#include <unordered_map>
#include <functional>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/error.h"
#include "cgvu/managed.h"

// Module includes
#include "vk/buffer.h"
#include "vk/texture.h"
#include "vk/cmdbuffer.h"
#include "vk/pipeline.h"
#include "vk/shaderprog.h"



//////
//
// Exception declarations
//

// Error namespace
namespace cgvu::err {

// Disable warnings related to unexported base classes
#ifdef _MSC_VER
	#pragma warning (push)
	#pragma warning (disable : 4275)
#endif

/** @brief Indicates an error reported by the Vulkan API. */
class CGVU_API VulkanException : public Exception
{

protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the exception for the given Vulkan error code. For further
	 *		semantics, see @ref cgvu::err::Exception::Exception(const log::EventSource&).
	 */
	VulkanException(unsigned errorCode, const log::EventSource &source) noexcept;

	/**
	 * @brief
	 *		Constructs the exception for the given Vulkan error code. For further
	 *		semantics, see @ref cgvu::err::Exception::Exception(log::EventSource &&).
	 */
	VulkanException(unsigned errorCode, log::EventSource &&source) noexcept;
};

// Reset to previous warning settings
#ifdef _MSC_VER
	#pragma warning (pop)
#endif

// Close error namespace
}



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Shader program abstraction
class ShaderProgram;

// Vulkan abstraction main classes
class Vulkan;
class Context;

// Operational abstractions provided by cgvu::Context
class StorageBuffer;
class Pipeline;
class CommandBuffer;
class PresentationSurface;



//////
//
// Enums
//

/**
 * @brief
 *		Physical vulkan device selection guidelines for use in a @ref
 *		cgvu::VulkanDeviceScoreSet .
 */
enum class VulkanScoringCriterion
{
	ID,
	NAME,
	IS_DEDICATED,
	IS_INTEGRATED,
	IS_SOFTWARE,
	SWAPCHAIN_SUITABILITY,
	COMPUTE_CAPABLE,
	DEDICATED_TRANSFER,
	UNIFIED_PRESENTATION,
	PER_EXTENSION,
	PER_QUEUE_FAMILY,
	PER_COMPUTE_QUEUE,
	PER_GRAPHICS_QUEUE
};
/** @brief Shorthand for @ref cgvu::VulkanScoringCriterion . */
typedef VulkanScoringCriterion DevScore;

/** @brief List of default score sets defined by CGVu. */
enum class DefaultScoreSet
{
	/**
	 * @brief
	 *		Default that tries to give the highest score to the most capable (and likely
	 *		fastest) Vulkan-compatible GPU in the system.
	 */
	BEST,

	/**
	 * @brief
	 *		Default that tries to give the highest score to the most energy-efficient
	 *		Vulkan-compatible GPU in the system (currently, this will yield the same
	 *		result as @ref cgvu::DSS::BEST unless there is a non-dedicated, integrated
	 *		APU present in the system).
	 */
	ENERGY_SAVER,

	/**
	 * @brief
	 *		Default that tries to give the highest score to the most capable software
	 *		rendering device for maximum compatibility and debugging capabilities,
	 *		prefering integrated GPUs over dedicated graphics cards when no software
	 *		device is available.
	 */
	SOFTWARE
};
/** @brief Shorthand for @ref cgvu::DefaultScoreSet . */
typedef DefaultScoreSet DSS;

/** @brief Enumeration of possible origins of Vulkan API debug messages. */
enum class VulkanDebugChannel
{
	LOADER_DRIVER = 0,
	VALIDATION    = 1
};
/** @brief Shorthand for @ref cgvu::VulkanDebugChannel . */
typedef VulkanDebugChannel Dbg;

/** @brief Enumeration of possible filtering modes of an image sampler. */
enum class SamplerFiltering
{
	NEAREST = 0,
	LINEAR  = 1
};
/** @brief Shorthand for @ref cgvu::SamplerFiltering . */
typedef SamplerFiltering SFlt;

/** @brief Enumeration of possible repeat modes of an image sampler. */
enum class SamplerRepeat
{
	REPEAT = 0,
	MIRROR = 1,
	CLAMP  = 2,
	CLAMP_MIRRORED = 3,
	BORDER = 4
};
/** @brief Shorthand for @ref cgvu::SamplerRepeat . */
typedef SamplerRepeat SRpt;



//////
//
// Structs and typedefs
//

/**
 * @brief
 *		Map indicating desired message levels for the Vulkan API debug channels for
 *		construction of @ref cgvu::Vulkan instances.
 */
typedef std::map<VulkanDebugChannel, log::Severity> DebugLevels;

/**
 * @brief
 *		Map indicating the criterea to score for device selection; to be used with @ref
 *		cgvu::Vulkan::obtainContext .
 */
typedef std::unordered_map<VulkanScoringCriterion, float> VulkanDeviceScoreSet;



//////
//
// Class definitions
//

/** @brief Encapsulates graphics API state corresponding to one logical Vulkan device. */
class CGVU_API Context
{

	////
	// Interfacing - friend declarations

	friend class Vulkan;


public:

	////
	// Exported types

	/** @brief Interface for event listeners. */
	struct EventListener
	{
		/**
		 * @brief Notified while the main window is undergoing a resize.
		 *
		 * @note This API will be moved to a proper windowing module once that exists.
		 */
		virtual void onWindowIsResizing (
			Context& ctx, unsigned newWidth, unsigned newHeight
		) = 0;

		/**
		 * @brief Notified whenever the OS requests the window redraw its contents.
		 *
		 * @note This API will be moved to a proper windowing module once that exists.
		 */
		virtual void onWindowRefresh (Context& ctx) = 0;

		/** @brief Notified when the surface the context presents to has changed. */
		virtual void onPresentationSurfaceChanged (Context &ctx) = 0;
	};


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Object construction / destruction

	/** @brief Constructs the context according to the passed data (used internally). */
	Context(Vulkan &vulkan, Handle device);


public:

	////
	// Object construction / destruction

	/** @brief Not copy-constructible. */
	Context(const Context &) = delete;

	/** @brief The move constructor. */
	Context(Context &&other);

	/** @brief The destructor. */
	virtual ~Context();


	////
	// Operators

	/** @brief Not copy-assignable. */
	Context& operator= (const Context &) = delete;

	/** @brief Move assignment. */
	Context& operator= (Context &&other);


	////
	// Methods

	/** @brief Creates a buffer object for storing GPU-accessible data. */
	BufferObject::Reference createBuffer (
		size_t size, unsigned usageFlags, unsigned memProps
	);

	/** @brief Deletes the given buffer object. */
	void deleteObject (BufferObject &buffer);

	/** @brief Creates an image object for storing GPU-accessible image data. */
	ImageObject::Reference createImage (
		ImageDimension dims, const unsigned *sizes, TexelFormat texelFmt,
		bool supportReadBack=false
	);

	/** @brief Deletes the given image object. */
	void deleteObject (ImageObject &buffer);

	/** @brief Creates an object encapsulating graphics pipeline state for rendering. */
	Pipeline::Reference createPipeline (const ShaderProgram &shaderProg);

	/** @brief Deletes the given pipeline object. */
	void deleteObject (Pipeline &pipeline);

	/**
	 * @brief
	 *		Creates a command buffer for a given class of tasks in either the static or
	 *		transient pools.
	 */
	CommandBuffer::Reference createCommandBuffer (TaskClass taskClass,
	                                              bool transient=true);

	/** @brief Deletes the given command buffer. */
	void deleteObject (CommandBuffer &buffer);

	/** @brief Creates a Vulkan semaphore object. */
	Handle createSemaphore (void);

	/** @brief Delete a Vulkan semaphore object. */
	void deleteSemaphore (Handle semaphore);

	/** @brief Creates a Vulkan fence object. */
	Handle createFence (bool signaled=false);

	/** @brief Delete a Vulkan fence object. */
	void deleteFence (Handle fence);

	/**
	 * @brief
	 *		Retrieve (and creates, if it doesn't yet exist) the image sampler with the
	 *		given parameters.
	 */
	Handle obtainSampler (
		SamplerFiltering minFilter, SamplerFiltering magFilter, SamplerRepeat repeatU,
		SamplerRepeat repeatV, SamplerRepeat repeatW, unsigned anisotropy=0
	);

	/**
	 * @brief Retrieves the image sampler with the given parameters.
	 *
	 * @note
	 *		Since this overload is read-only, the method will fail if no sampler with
	 *		the specified parameter set exists yet.
	 */
	Handle obtainSampler (
		SamplerFiltering minFilter, SamplerFiltering magFilter, SamplerRepeat repeatU,
		SamplerRepeat repeatV, SamplerRepeat repeatW, unsigned anisotropy=0
	) const;

	/**
	 * @brief
	 *		Prepares the presentation surface to receive a new frame. The behavior when
	 *		invoked on a non-presenting context is undefined.
	 */
	void prepareFrame (void);

	/** @brief Submits a command buffer. */
	void submitCommands (const CommandBuffer &cmdBuffer, float priority=1.0f);

	/**
	 * @brief
	 *		Ends the current frame and presents the next image in the swap chain to the
	 *		surface. The behavior when invoked on a non-presenting context is undefined.
	 */
	void presentFrame (void);

	/**
	 * @brief
	 *		Blocks the calling thread until the logical device encapsulated by the
	 *		context is idle.
	 */
	void waitIdle (void) const;

	/** @brief Retrieves the actual logical device handle encapsulated by the context. */
	Handle handle (void) const;

	/**
	 * @brief
	 *		Retrieves the actual physical device handle the context has been created
	 *		from.
	 */
	Handle handlePhy (void) const;

	/** @brief Retrieves the window handle the context has been created for. */
	Handle window (void) const;

	/**
	 * @brief
	 *		Retrieves the @ref cgvu::PresentationSurface the context presents to . If the
	 *		context does not present to a surface, the behavior is undefined.
	 */
	PresentationSurface& surface (void);

	/**
	 * @brief
	 *		Retrieves the @ref cgvu::PresentationSurface the context presents to in
	 *		read-only mode. If the context does not present to a surface, the behavior is
	 *		undefined.
	 */
	const PresentationSurface& surface (void) const;

	/** @brief Retrieves the Vulkan object representing the main viewport. */
	Handle getViewport (void) const;

	/** @brief Retrieves the main viewport width (0 for non-presenting contexts). */
	unsigned getViewportWidth (void) const;

	/** @brief Retrieves the main viewport height (0 for non-presenting contexts). */
	unsigned getViewportHeight (void) const;

	/**
	 * @brief
	 *		Reports the length of the swapchain (forwards @ref
	 *		cgvu::PresentationSurface::getSwapchainLength ).
	 */
	unsigned getSwapchainLength (void) const;

	/** @brief Reports the ID of the current swapchain image. */
	unsigned getSwapImageID (void) const;

	/** @brief Reports the ID of the last used swapchain image. */
	unsigned getLastSwapImageID (void) const;

	Handle getSwapImage (unsigned id) const;

	Handle getCurrentSwapImage (void) const;

	Handle getLastSwapImage (void) const;

	/** @brief Reports the ID of the current in-flight frame. */
	unsigned getFrameID (void) const;

	/** @brief Reports the ID of the last in-flight frame. */
	unsigned getLastFrameID (void) const;

	/**
	 * @brief Retrieves an internal descriptor of the underlying device properties.
	 *
	 * @note
	 *		This is a temporary method that will be removed once proper mechanisms are in
	 *		place!
	 */
	Handle getDeviceInfo (void) const;

	/** @brief Reports the "best" supported format for depth/stencil images. */
	TexelFormat bestDepthStencilFormat (bool stencilSupport=true) const;

	/** @brief Subscribes an event listener. */
	void subscribeEventListener (EventListener *subscriber);

	/** @brief Unsubscribes an event listener. */
	void unsubscribeEventListener (EventListener *subscriber);
};


/**
 * @brief
 *		Encapsulates a Vulkan surface that a context can present to. Will likely be
 *		superceded by some texture-surface abstraction later on.
 */
class CGVU_API PresentationSurface
{

	////
	// Interfacing - friend declarations

	friend class Vulkan;
	friend class Context;
	friend class Pipeline;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Object construction / destruction

	/** @brief Constructs the presentation surface in the given Vulkan instance. */
	PresentationSurface(Vulkan &vulkan);


	////
	// Methods

	/** @brief Creates the internal Vulkan surface for the given window. */
	void init (Handle window);

	/** @brief Creates a swapchain with the given capabilities for the given context. */
	void createSwapchain (
		Context &ctx, Handle capabilities, Handle format, Handle mode,
		const std::vector<unsigned> &queueSharing
	);

	/** @brief Destroys the swapchain and frees all associated resources. */
	void destroySwapchain (void);

	/**
	 * @brief
	 *		Selects an image from the swapchain and returns its index. The call initiates
	 *		acquisition of the image but returns immediatly - the provided semaphore will
	 *		be signalled once acquisition is complete. Commands buffers rendering to the
	 *		surface should be set up to wait on this semaphore.
	 */
	unsigned acquire (Handle readySemaphore);

	/** @brief Returns a handle to the Vulkan color format used by the surface. */
	Handle getColorFormat (void) const;

	/** @brief Returns a handle to the presentation mode the swapchain operates in. */
	Handle getPresentationMode (void) const;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		The default constructor. If constructed this way, the presentation surface
	 *		object will not be usable until a functioning instance is moved in using
	 *		@link #operator=(PresentationSurface&&) move assignment @endlink .
	 */
	PresentationSurface();

	/** @brief Not copy-constructible. */
	PresentationSurface(const PresentationSurface&) = delete;

	/** @brief The move constructor. */
	PresentationSurface(PresentationSurface &&other);

	/** @brief The destructor. */
	virtual ~PresentationSurface();


	////
	// Operators

	/** @brief Not copy-assignable. */
	PresentationSurface& operator= (const PresentationSurface&) = delete;

	/** @brief Move assignment. */
	PresentationSurface& operator= (PresentationSurface &&other);


	////
	// Methods

	/** @brief Tests the given Vulkan physical device againt the surface. */
	bool queryCapabilities (Handle capsOut, Handle physicalDevice) const;

	/**
	 * @brief
	 *		Retrieves (and creates, if none yet exists) a framebuffer for the underlying
	 *		swapchain that can be used with the given render pass.
	 */
	Handle obtainFramebuffer (RenderPass &renderPass);

	/**
	 * @brief
	 *		Retrieves a framebuffer for the underlying swapchain that can be used with
	 *		the given render pass.
	 *
	 * @note
	 *		Since this overload is read-only, the method will fail if no suitable
	 *		framebuffer exists yet.
	 */
	Handle obtainFramebuffer (const RenderPass &renderPass) const;

	/** @brief Retrieves the internal Vulkan handle encapsulated by this class. */
	Handle handle (void);

	/**
	 * @brief
	 *		Retrieves the internal Vulkan handle to the swapchain associated with the
	 *		presentation surface.
	 */
	Handle swapchain (void);

	/** @brief Retrieves the surface width. */
	unsigned getWidth (void) const;

	/** @brief Retrieves the surface height. */
	unsigned getHeight (void) const;

	/** @brief Returns the number of images in the surface swapchain. */
	unsigned getSwapchainLength (void) const;

	/** @brief Returns a handle to the given swapchain image. */
	Handle getSwapchainImage(unsigned id) const;

	/** @brief Returns a Vulkan struct describing the capabilities of the surface. */
	Handle getCapabilities (void) const;

	/** @brief Returns the pixel format the surface framebuffers where created with. */
	Handle getFormat (void) const;

	/** @brief Returns the presentation mode used by the surface swapchain. */
	Handle getPresentMode (void) const;

	/**
	 * @brief
	 *		Returns true whenever some surface properties changed (e.g. due to resizing).
	 *		This indicates that a swapchain recreation is necessary.
	 */
	bool dirty (void) const;
};


/** @brief Encapsulates the Vulkan API. */
class CGVU_API Vulkan
{

public:

	////
	// Exported types

	/** @brief Callback functions for Vulkan debug logging. */
	typedef std::function<void(const std::string&,log::Severity severity)> DebugCallback;

	/** @brief Container struct for storing info about device extensions. */
	struct DeviceExtensionInfo
	{
		/** @brief Version number of the extension. @c 0 matches any version. */
		unsigned version;

		/** @brief Pointer to the corresponding Vulkan extension features struct. */
		void *features;
	};


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/** @brief The default constructor. */
	Vulkan();

	/** @brief Not copy-constructible. */
	Vulkan(const Vulkan &) = delete;

	/** @brief The move constructor. */
	Vulkan(Vulkan &&other);

	/** @brief The destructor. */
	virtual ~Vulkan();


	////
	// Operators

	/** @brief Not copy-assignable. */
	Vulkan& operator= (const Vulkan &) = delete;

	/** @brief Move assignment. */
	Vulkan& operator= (Vulkan &&other);


	////
	// Methods

	/** @brief Sets up Vulkan debug reporting. Should be used before @ref #init . */
	void setDebugLevel (VulkanDebugChannel channel, log::Severity verbosity);

	/** @brief Sets the Vulkan debug message callback. */
	void setDebugCallback (const Vulkan::DebugCallback &callback);

	/** @brief Initializes the Vulkan API instance. */
	void init (const std::string &appName, const std::set<std::string> &reqInstanceExts,
	           const std::set<std::string> &reqInstanceLayers);

	/** @brief Returns a handle to the underlying Vulkan API instance. */
	Handle handle (void);

	/**
	 * @brief
	 *		Retrieves (and initializes if not yet used) a Vulkan device context with
	 *		highest possible score according to the given scoring criterea. Throws @ref
	 *		cgvu::err::NotSupportedException in case no suitable context could be
	 *		obtained. If a context without an associated presentation surface is desired,
	 *		@a window should be set to @c NULL .
	 *
	 * @note
	 *		Contexts obtained from this will become invalid when the Vulkan object that
	 *		created them is destroyed!
	 */
	Context& obtainContext (
		Handle window, const VulkanDeviceScoreSet &scoring,
		const std::map<std::string, DeviceExtensionInfo> &reqDeviceExts={}
	);

	/** @brief Retrieves a default score set for use with @ref #obtainContext . */
	static const VulkanDeviceScoreSet& getDefaultScoreSet (DefaultScoreSet set);
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __VULKAN_H__
