
#ifndef __SHADERPROG_H__
#define __SHADERPROG_H__


//////
//
// Includes
//

// C++ STL
#include <string>
#include <vector>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/error.h"
#include "cgvu/log/logging.h"



//////
//
// Exception declarations
//

// Error namespace
namespace cgvu::err {

/** @brief Indicates an error when loading a shader program. */
class ShaderParseException : public Exception
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/** @brief See @ref cgvu::err::Exception::Exception(const log::EventSource &) . */
	ShaderParseException(const std::string &reason, const log::EventSource &source) noexcept;

	/** @brief See @ref cgvu::err::Exception::Exception(log::EventSource &&) . */
	ShaderParseException(const std::string &reason, log::EventSource &&source) noexcept;

	/** @brief The destructor. */
	~ShaderParseException() noexcept;
};

// Close error namespace
}



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Vulkan abstraction main classes
class Context;

// Operational abstractions provided by cgvu::Context
class ImageObject;

// Vulkan convenience classes
class Uniform;



//////
//
// Enums
//

/** @brief Indicates the source file type of a shader program. */
enum ShaderProgSource
{
	/** @brief The program is in binary shaderpack format. */
	SHADER_SRC_SPK  = 0,

	/** @brief The program is a text file shaderpack definition. */
	SHADER_SRC_DEF  = 1,

	/** @brief The type will be determined automatically at runtime. */
	SHADER_SRC_AUTO = 0xff
};

/** @brief Flags used to refer to the shader stages. */
enum class ShaderStageFlags
{
	NONE     = 0,

	VERTEX   = 0x01,
	GEOMETRY = 0x02,
	TESSCTRL = 0x04,
	TESSEVAL = 0x08,
	FRAGMENT = 0x10,
	COMPUTE  = 0x20,

	VERTEX_FRAG = VERTEX | FRAGMENT,
	VERTEX_GEOM = VERTEX | GEOMETRY,
	VERTEX_GEOM_FRAG = VERTEX | GEOMETRY | FRAGMENT,

	ALL_GFX = VERTEX | GEOMETRY | TESSCTRL | TESSEVAL | FRAGMENT,
	ALL     = VERTEX | GEOMETRY | TESSCTRL | TESSEVAL | FRAGMENT | COMPUTE
};
/** @brief Shorthand for @ref cgvu::StorageBufferType . */
typedef ShaderStageFlags SH;

/** @brief Indicates the type of a uniform for use with @ref cgvu::UniformDescriptor . */
enum class UnfiformType
{
	/**
	 * @brief
	 *		Inficates that the uniform contains ordinary data, i.e. integral GLSL types
	 *		like floats, ints, or vectors or matrices thereof.
	 */
	INTEGRAL,

	/** @brief Indicates that this uniform is a sampler. */
	SAMPLER,

	/** @brief Indicatesa a shader storage buffer rather than a classical uniform. */
	STORAGE
};
/** @brief Shorthand for @ref cgvu::UnfiformType . */
typedef UnfiformType UFrm;



//////
//
// Structs and typedefs
//

/**
 * @brief
 *		Struct describing a set of uniforms that the shader accesses at the same binding
 *		point.
 */
struct UniformDescriptor
{
	/** @brief The ID of the binding point. */
	unsigned bindingID;

	/** @brief The type of this uniform. */
	UnfiformType type;

	/**
	 * @brief
	 *		The number of instances of this uniform set at the binding point. Values
	 *		greater than one indicate an array.
	 */
	unsigned count;

	/** @brief The shader stages this uniform set is used in. */
	ShaderStageFlags stages;
};
/** @brief Convenience shorthand for an array of @ref cgvu::UniformDescriptor . */
typedef std::vector<UniformDescriptor> UniformDescriptors;

/** @brief Struct describing a single descriptor set update. */
struct DescriptorSetUpdate
{
	/** @brief The ID of the binding point. */
	unsigned bindingID;

	/** @brief The type of the uniform to update from. */
	UnfiformType type;

	/** @brief Opaque update definition. */
	const void *update;
};
/**
 * @brief
 *		Array of descriptor set bindingID -> uniform mappings for use with @ref
 *		cgvu::ShaderProgram::updateDescriptorSets .
 */
typedef std::vector<DescriptorSetUpdate> DescriptorSetUpdates;

/** @brief Specialization of @ref cgvu::DescriptorSetUpdate for integral uniforms. */
struct UniformUpdate : public DescriptorSetUpdate
{
	// Explicit inheritance
	using DescriptorSetUpdate::DescriptorSetUpdate;

	/** @brief Constructs an update from the given uniform. */
	UniformUpdate(unsigned bindingID, const Uniform &uniform)
		: DescriptorSetUpdate({bindingID, UFrm::INTEGRAL, &uniform})
	{}
};

/** @brief Specialization of @ref cgvu::DescriptorSetUpdate for samplers. */
struct SamplerUpdate : public DescriptorSetUpdate
{
	// Explicit inheritance
	using DescriptorSetUpdate::DescriptorSetUpdate;

	/** @brief The image to bind to the descriptor set. */
	const ImageObject &image;

	/** @brief The sampler that shall be used to access the image. */
	Handle sampler;

	/** @brief Constructs an update from the given sampler. */
	SamplerUpdate(unsigned bindingID, const ImageObject &image, Handle sampler)
		: DescriptorSetUpdate({bindingID, UFrm::SAMPLER, this}), image(image),
		  sampler(sampler)
	{}
};



//////
//
// Class definitions
//

/**
 * @brief
 *		Encapsulates a shaderprogram made up of a collection of individual stages which
 *		can be linked together in a @ref cgvu::Pipeline .
 */
class CGVU_API ShaderProgram
{
	////
	// Interfacing - friend declarations

	friend class Pipeline;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


protected:

	////
	// Methods

	/**
	 * @brief
	 *		Returns shader stage creation information needed for linking the program into
	 *		a @ref cgvu::Pipeline .
	 */
	Handle getStageCreationInfo (void) const;

	/**
	 * @brief
	 *		Returns the descriptor set layout needed for linking the program into a @ref
	 *		cgvu::Pipeline .
	 */
	Handle getDescSetLayout (void) const;


public:

	////
	// Object construction / destruction

	/** @brief Constructs the object for the given shader program. */
	ShaderProgram(ShaderProgSource source, const std::string &programFile,
	              bool loadImmediatly=false);

	/** @brief The destructor. */
	~ShaderProgram();


	////
	// Methods

	/**
	 * @brief
	 *		Load shader program such that it resides in system memory as a collection of
	 *		SPIR-V bytecode modules.
	 */
	void load (void);

	/**
	 * @brief
	 *		Builds stages of the shader program into Vulkan modules that can be linked
	 *		into a pipeline, given the description of uniforms used in the actual shader
	 *		code. If @c autoload is set to true, the shader program will first be loaded
	 *		from disk (see @ref #load ) if necessary.
	 */
	void build (
		Context &context, const UniformDescriptors &uniforms, bool autoload=true
	);

	 /** @brief Writes the loaded program to disk as a ShaderPack file. */
	void save (const std::string &filename) const;

	/** @brief Returns whether the program has been built and linked already. */
	bool isBuilt (void) const;

	/** @brief Updates the given descriptor sets with the given uniforms. */
	void updateDescriptorSets (const DescriptorSetUpdates &updates);

	/** @brief Retrieves the Vulkan context used to build the program. */
	Context& context (void) const;

	/**
	 * @brief
	 *		Returns the descriptor set for the given swapchain image (for use with a @ref
	 *		cgvu::Pipeline ).
	 */
	Handle getDescSet (unsigned swapImageID) const;
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __SHADERPROG_H__
