
//////
//
// Includes
//

/* None. */
#include <iostream>


//////
//
// Internals definitions
//

////
// cgvu::Context

struct VulkanQueue
{
	VulkanQueue(const VkQueueFamilyProperties &props) : props(props) {}

	VkQueueFamilyProperties props;
	bool canPresent = false;

	std::vector<VkQueue> subQueues;

	VkCommandPool pool_static=VK_NULL_HANDLE, pool_transient=VK_NULL_HANDLE;

	inline VkQueue subQueue (float priority) {
		return subQueues[unsigned((1.0f-priority)*float(subQueues.size()))];
	}
};

struct VulkanDeviceInfo
{
	VulkanDeviceInfo(VkPhysicalDevice physicalDevice)
		: handle(physicalDevice), props({}), primaryGraphics(-1), primaryCompute(-1),
		  primaryTransfer(-1), primaryPresent(-1)
	{}

	VkPhysicalDevice handle;
	VkPhysicalDeviceProperties props;
	VkPhysicalDeviceFeatures features;
	VkPhysicalDeviceMemoryProperties memProps;

	std::unordered_map<VkFormat, VkFormatProperties> formatSupport;
	std::map<std::string, unsigned> extensions;

	std::vector<VulkanQueue> qf;
	signed primaryGraphics, primaryCompute, primaryTransfer, primaryPresent;

	inline VulkanQueue& primaryGraphicsQF (void) { return qf[primaryGraphics]; }
	inline VulkanQueue& primaryComputeQF (void) { return qf[primaryCompute]; }
	inline VulkanQueue& primaryTransferQF (void) { return qf[primaryTransfer]; }
	inline VulkanQueue& primaryPresentationQF (void) { return qf[primaryPresent]; }

	inline VkQueue getGraphicsQueue (float priority=1.0f) {
		return qf[primaryGraphics].subQueue(priority);
	}
	inline VkQueue getComputeQueue(float priority=1.0f) {
		return qf[primaryCompute].subQueue(priority);
	}
	inline VkQueue getTransferQueue(float priority=1.0f) {
		return qf[primaryTransfer].subQueue(priority);
	}
	inline VkQueue getPresentationQueue (float priority=1.0f) {
		return qf[primaryPresent].subQueue(priority);
	}

	uint32_t findMemoryType (
		const VkMemoryRequirements &requirements, VkMemoryPropertyFlags properties
	) const
	{
		for (unsigned heap=0; heap<memProps.memoryTypeCount; heap++)
		if (   (requirements.memoryTypeBits & (1<<heap))
		    && (memProps.memoryTypes[heap].propertyFlags & properties) == properties)
			return heap;
		throw cgvu::err::NotSupportedException(CGVU_LOGMSG(
			"VulkanDeviceInfo::findMemoryType", "No type of memory that the device"
			" provides fits the specified requirements!"
		));
	}
};

struct RenderPass_impl
{
	VkRenderPass rp = VK_NULL_HANDLE;
	cgvu::DepthStencilBuffer *dsBuf = nullptr;
};
#define RP_IMPL ((RenderPass_impl*)pimpl)
#define RP_PTR(inst) ((RenderPass_impl*)((inst).pimpl))
