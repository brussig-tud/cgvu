
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <sstream>
#include <cstring>

// Vulkan
#include <vulkan/vulkan.h>

// STB library
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

// CGVU includes
#include "error.h"
#include "util/utils.h"
#include "log/logging.h"

// Module includes
#include "buffer.h"
#include "vulkan.h"
#include "vulkan_internals.h"

// Implemented header
#include "texture.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::err;
using namespace cgvu::util;



//////
//
// Helper functions
//

// _stricmp macro
#ifndef _MSC_VER
	#define _stricmp strcasecmp
#endif

// Anonymous namespace begin
namespace {

// Returns the texel size in bytes for a given texel format
inline unsigned calcTexelSize (TexelFormat texelFmt)
{
	switch (texelFmt)
	{
		case TFmt::RGB_INT8:
			return 3;
		case TFmt::RGB_FLT16:
			return 6;
		case TFmt::RGB_FLT32:
			return 12;
		case TFmt::RGBA_INT8:
			return 4;
		case TFmt::RGBA_FLT16:
			return 8;
		case TFmt::RGBA_FLT32:
			return 16;
		case TFmt::D_INT16:
			return 2;
		case TFmt::D16S8_INT:
			return 3;
		case TFmt::D_INT24:
			return 4;
		case TFmt::D24S8_INT:
			return 4;
		case TFmt::D_FLT32:
			return 4;
		case TFmt::S_INT8:
			return 1;

		default:
			// This should not happen... silently fail
			return 0;
	}
}

// Returns the texel size in bytes for a given texel format
inline VkImageViewType getViewType (ImageDimension dims)
{
	switch (dims)
	{
		case IDim::I1D:
			return VK_IMAGE_VIEW_TYPE_1D;
		case IDim::I2D:
			return VK_IMAGE_VIEW_TYPE_2D;
		case IDim::I3D:
			return VK_IMAGE_VIEW_TYPE_3D;
		case IDim::CUBE:
			return VK_IMAGE_VIEW_TYPE_CUBE;

		default:
			// This should not happen... silently fail
			return VK_IMAGE_VIEW_TYPE_MAX_ENUM;
	}
}

// Anonymous namespace end
}



//////
//
// Class implementations
//

////
// cgvu::ImageObject

struct ImageObject_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	VkFormat format = VK_FORMAT_UNDEFINED;
	bool hasDepth=false, hasStencil=false;

	VkImage img = VK_NULL_HANDLE;
	VkDeviceMemory mem = VK_NULL_HANDLE;
	VkImageView view = VK_NULL_HANDLE;
	VkDeviceSize sizeDev = 0;
	VkImageUsageFlags usage;
	size_t size = 0;

	char *mapped = nullptr;
	VkFence workFence = VK_NULL_HANDLE;

	std::vector<BufferObject::EnsureAction> pea;

	ImageObject_impl(Context &ctx): ctx(ctx) {}
};
#define IMAGEOBJ_IMPL ((ImageObject_impl*)pimpl)

ImageObject::ImageObject(
	Context &ctx, ImageDimension dims, const unsigned *sizes, TexelFormat texelFmt,
	bool supportReadBack
)
{
	// Create implementation instance
	pimpl = new ImageObject_impl(ctx);
	auto &impl = *IMAGEOBJ_IMPL;

	// Determine image object parameters
	// - fill setup info
	VkImageCreateInfo imageCI{};
	imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	switch (dims)
	{
		case IDim::I1D:
			imageCI.imageType = VK_IMAGE_TYPE_1D;
			imageCI.extent.width = (uint32_t)sizes[0];
			imageCI.extent.depth = imageCI.extent.height = 1;
			break;

		case IDim::I2D:
			imageCI.imageType = VK_IMAGE_TYPE_2D;
			imageCI.extent.width = (uint32_t)sizes[0];
			imageCI.extent.height = (uint32_t)sizes[1];
			imageCI.extent.depth = 1;
			break;

		case IDim::I3D:
		case IDim::CUBE:
			imageCI.imageType = VK_IMAGE_TYPE_3D;
			imageCI.extent.width = (uint32_t)sizes[0];
			imageCI.extent.height = (uint32_t)sizes[1];
			imageCI.extent.depth = (uint32_t)sizes[2];
			break;

		default:
			// This should not happen. Throw up...
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ImageObject::<ctor>", "Invalid image dimensionality specified!"
			));
	}
	imageCI.mipLevels = 1;
	imageCI.arrayLayers = 1;
	switch (texelFmt)
	{
		case TFmt::RGB_INT8:
			impl.format = imageCI.format = VK_FORMAT_R8G8B8_SRGB;
			break;
		case TFmt::RGB_FLT16:
			impl.format = imageCI.format = VK_FORMAT_R16G16B16_SFLOAT;
			break;
		case TFmt::RGB_FLT32:
			impl.format = imageCI.format = VK_FORMAT_R32G32B32_SFLOAT;
			break;
		case TFmt::RGBA_INT8:
			impl.format = imageCI.format = VK_FORMAT_R8G8B8A8_SRGB;
			break;
		case TFmt::RGBA_FLT16:
			impl.format = imageCI.format = VK_FORMAT_R16G16B16A16_SFLOAT;
			break;
		case TFmt::RGBA_FLT32:
			impl.format = imageCI.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			break;
		case TFmt::D_INT16:
			impl.format = imageCI.format = VK_FORMAT_D16_UNORM;
			impl.hasDepth = true;
			break;
		case TFmt::D16S8_INT:
			impl.format = imageCI.format = VK_FORMAT_D16_UNORM_S8_UINT;
			impl.hasStencil = impl.hasDepth = true;
			break;
		case TFmt::D_INT24:
			impl.format = imageCI.format = VK_FORMAT_X8_D24_UNORM_PACK32;
			impl.hasDepth = true;
			break;
		case TFmt::D24S8_INT:
			impl.format = imageCI.format = VK_FORMAT_D24_UNORM_S8_UINT;
			impl.hasStencil = impl.hasDepth = true;
			break;
		case TFmt::D_FLT32:
			impl.format = imageCI.format = VK_FORMAT_D32_SFLOAT;
			impl.hasDepth = true;
			break;
		case TFmt::S_INT8:
			impl.format = imageCI.format = VK_FORMAT_S8_UINT;
			impl.hasStencil = true;
			break;

		default:
			// This should not happen. Throw up...
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ImageObject::<ctor>", "Invalid texel format specified!"
			));
	}
	bool isColor = !(impl.hasDepth || impl.hasStencil);
	// - determine queue sharing indices (ToDo: do this only once in the context)
	auto &devInfo = *((VulkanDeviceInfo*)ctx.getDeviceInfo());
	std::vector<uint32_t>sharingQueues; sharingQueues.reserve(3);
	if (isColor)
	{
		for (unsigned q=0; q<devInfo.qf.size(); q++)
			if (   devInfo.qf[q].props.queueFlags & VK_QUEUE_GRAPHICS_BIT
			    || devInfo.qf[q].props.queueFlags & VK_QUEUE_COMPUTE_BIT
			    || devInfo.qf[q].props.queueFlags & VK_QUEUE_TRANSFER_BIT
			    || devInfo.qf[q].props.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
				sharingQueues.emplace_back(q);
	}
	else
		for (unsigned q=0; q<devInfo.qf.size(); q++)
			if (   devInfo.qf[q].props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				sharingQueues.emplace_back(q);
	imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageCI.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	if (isColor)
		imageCI.usage =
			  (supportReadBack ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0)
			| VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	else if (impl.hasDepth || impl.hasStencil)
		imageCI.usage =
			  (supportReadBack ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0)
			| VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	impl.usage = imageCI.usage;
	if (sharingQueues.size() > 1)
	{
		imageCI.sharingMode = VK_SHARING_MODE_CONCURRENT;
		imageCI.queueFamilyIndexCount = (uint32_t)sharingQueues.size();
		imageCI.pQueueFamilyIndices = sharingQueues.data();
	}
	else
		imageCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
	imageCI.flags = 0; // Optional

	// Create the Vulkan image object
	VkDevice dev = ctx.handle();
	VkResult result = vkCreateImage(dev, &imageCI, nullptr, &(impl.img));
	if (result  != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ImageObject::<ctor>", "Unable to create Vulkan image object!"
		));
	impl.initialized = true; // We have something to clean up from here on
	impl.size =
		  imageCI.extent.width*imageCI.extent.height*imageCI.extent.depth
		* calcTexelSize(texelFmt);

	// Allocate device memory for the buffer
	// - query the memory requirements of the image
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(dev, impl.img, &memRequirements);
	// - actually allocate memory in the appropriate heap
	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	impl.sizeDev = allocInfo.allocationSize = memRequirements.size;
	if (impl.size != impl.sizeDev)
		std::cout.flush();
	allocInfo.memoryTypeIndex = devInfo.findMemoryType(
		memRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	);
	result = vkAllocateMemory(dev, &allocInfo, nullptr, &(impl.mem));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ImageObject::<ctor>", "Unable to allocate device memory for the image!"
		));
	result = vkBindImageMemory(dev, impl.img, impl.mem, 0);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ImageObject::<ctor>", "Unable to bind allocated device memory to the image!"
		));

	// Create image view
	VkImageViewCreateInfo viewCI{};
	viewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewCI.image = impl.img;
	viewCI.viewType = getViewType(dims);
	viewCI.format = impl.format;
	viewCI.subresourceRange.aspectMask =
		  (isColor ? VK_IMAGE_ASPECT_COLOR_BIT : 0)
		| (impl.hasDepth ? VK_IMAGE_ASPECT_DEPTH_BIT : 0)
		| (impl.hasStencil ? VK_IMAGE_ASPECT_STENCIL_BIT : 0);
	viewCI.subresourceRange.baseMipLevel = 0;
	viewCI.subresourceRange.levelCount = 1;
	viewCI.subresourceRange.baseArrayLayer = 0;
	viewCI.subresourceRange.layerCount = 1;
	result = vkCreateImageView(dev, &viewCI, nullptr, &(impl.view));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ImageObject::<ctor>", "Unable to create Vulkan image view!"
		));

	// Done!
	impl.started = true;
}

void ImageObject::destroy (void)
{
	// Shortcut for saving one indirection
	auto &impl = *IMAGEOBJ_IMPL;

	// Handle cleanup
	if (impl.initialized)
	{
		// image object cannot be used from here on anymore
		impl.started = false;

		// Free buffer resources.
		VkDevice dev = impl.ctx.handle();
		if (impl.view)
		{
			vkDestroyImageView(dev, impl.view, nullptr);
			impl.view = VK_NULL_HANDLE;
		}
		if (impl.mem)
		{
			vkFreeMemory(dev, impl.mem, nullptr);
			impl.mem = VK_NULL_HANDLE;
		}
		impl.sizeDev = impl.size = 0;
		if (impl.img)
		{
			vkDestroyImage(dev, impl.img, nullptr);
			impl.img = VK_NULL_HANDLE;
		}

		impl.initialized = false;
	}

	delete IMAGEOBJ_IMPL;
	pimpl = nullptr;
}

Context& ImageObject::manager (void)
{
	return IMAGEOBJ_IMPL->ctx;
}

void ImageObject::addPostEnsureAction (const EnsureAction &action)
{
	IMAGEOBJ_IMPL->pea.emplace_back(action);
}

void ImageObject::notifyWork (Handle fence)
{
	// Shortcut for saving one indirection
	auto &impl = *IMAGEOBJ_IMPL;

	ensure();
	IMAGEOBJ_IMPL->workFence = fence;
}

void ImageObject::ensure (void) const
{
	// Shortcuts for saving indirections
	auto &impl = *IMAGEOBJ_IMPL;

	// See if work is currently being done on the image
	if (impl.workFence)
	{
		// Wait for the work to complete
		VkResult result =
			vkWaitForFences(impl.ctx.handle(), 1, &(impl.workFence), true, UINT64_MAX);
		if (result != VK_SUCCESS)
			throw err::VulkanException(result, CGVU_LOGMSG(
				"ImageObject::ensure", "Waiting on work fence failed!"
			));
		impl.workFence = VK_NULL_HANDLE;
	}

	// Execute registered actions and clear them for the next run
	for (auto& action : impl.pea)
		action();
	impl.pea.clear();
}

Handle ImageObject::handle (void) const
{
	return IMAGEOBJ_IMPL->img;
}

Handle ImageObject::view (void) const
{
	return IMAGEOBJ_IMPL->view;
}

size_t ImageObject::size (void) const
{
	return IMAGEOBJ_IMPL->size;
}

size_t ImageObject::sizeOnDevice (void) const
{
	return IMAGEOBJ_IMPL->sizeDev;
}

Handle ImageObject::getInternalFormat(void) const
{
	return IMAGEOBJ_IMPL->format;
}

bool ImageObject::hasDepth (void) const
{
	return IMAGEOBJ_IMPL->hasDepth;
}

bool ImageObject::hasStencil (void) const
{
	return IMAGEOBJ_IMPL->hasStencil;
}

bool ImageObject::supportsReadback (void) const
{
	return IMAGEOBJ_IMPL->usage | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
}



////
// cgvu::Texture

struct Texture_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	TexelFormat texelFmt;

	BufferObject stagingBuf;
	ImageObject image;
	CommandBuffer cmd;

	bool keepStagingBuf;

	unsigned sizes[3];

	void performBasicInit (ImageDimension dims, const ImageExtend &initSizes)
	{
		// Create GPU-side image
		sizes[0] = initSizes.x; sizes[2] = sizes[1] = 1;
		if (dims == IDim::I2D)
			sizes[1] = initSizes.y;
		else if (dims == IDim::I3D)
			sizes[2] = initSizes.z;
		else if (dims == IDim::CUBE)
			sizes[2] = 6;
		image = ctx.createImage(dims, sizes, texelFmt);

		// We have something to clean up from here on
		initialized = true;
	}

	Texture_impl(Context &ctx, TexelFormat texelFmt, bool keepStagingBuf)
		: ctx(ctx), texelFmt(texelFmt), keepStagingBuf(keepStagingBuf)
	{}
};
#define TEXTURE_IMPL ((Texture_impl*)pimpl)

Texture::Texture() : pimpl(nullptr) {}

Texture::Texture(Texture &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

Texture::Texture(
	Context &ctx, ImageDimension dims, const ImageExtend &sizes, TexelFormat texelFmt,
	bool keepStagingBuffer
)
	: pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new Texture_impl(ctx, texelFmt, keepStagingBuffer);
	auto &impl = *TEXTURE_IMPL;

	// Common init
	impl.performBasicInit(dims, sizes);
}

Texture::Texture(
	Context &ctx, const std::string &filename, bool keepStagingBuffer
)
	: pimpl(nullptr)
{
	// Load image data
	int width, height, channels;
	RAIIbuffer<stbi_uc, stbi_image_free> pixels = stbi_load(
		filename.c_str(), &width, &height, &channels, STBI_rgb_alpha
	);
	if (!pixels)
	{
		std::stringstream msg;
		msg << "Unable to read image from file - " << stbi_failure_reason();
		throw FileOpenException(
			FileOpenException::READ, filename, CGVU_LOGMSG("Texture::<ctor>", msg.str())
		);
	}

	// Create implementation instance
	pimpl = new Texture_impl(ctx, TFmt::RGBA_INT8, keepStagingBuffer);
	auto &impl = *TEXTURE_IMPL;

	// Common init
	impl.performBasicInit(IDim::I2D, {(unsigned)width, (unsigned)height});

	// Stage the loaded image data, and make sure the staging buffer is being unmapped
	// before the transfer
	void *buf = stage();
	std::memcpy(buf, pixels, impl.stagingBuf.size());
	upload();
}

Texture::~Texture()
{
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *TEXTURE_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Texture cannot be used from here on anymore
			impl.started = false;

			// Free actual image memory.
			// ToDo: consider whether to also explicitely free staging buffer
			impl.ctx.deleteObject(impl.image);

			impl.initialized = false;
		}

		delete TEXTURE_IMPL;
		pimpl = nullptr;
	}
}

Texture& Texture::operator= (Texture &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

Texture::operator const ImageObject& (void) const
{
	return TEXTURE_IMPL->image;
}

void* Texture::stage (void)
{
	// Shortcut for saving one indirection
	auto &impl = *TEXTURE_IMPL;

	// Prepare staging buffer
	size_t size = impl.sizes[0]*impl.sizes[1]*impl.sizes[2]*calcTexelSize(impl.texelFmt);
	if (!(impl.stagingBuf))
		impl.stagingBuf = impl.ctx.createBuffer(
			size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
		);

	// Done!
	return impl.stagingBuf.map();
}

void Texture::upload (void)
{
	// Shortcut for saving one indirection
	auto &impl = *TEXTURE_IMPL;

	// Make sure the staging buffer is being unmapped before transfer
	impl.stagingBuf.addPostEnsureAction([&impl]() {
		impl.stagingBuf.unmap();
	});

	// Transfer data to GPU
	// - create transfer command buffer
	impl.cmd = impl.ctx.createCommandBuffer(TC::TRANSFER, true);
	impl.cmd.startRecording();
	impl.cmd.copyBuffer(
		impl.stagingBuf, impl.image, 
		{ {0, 0, 0, {0,0,0}, {impl.sizes[0], impl.sizes[1], impl.sizes[2]}} }
	);
	impl.cmd.build();
	// - issue the transfer!
	impl.ctx.submitCommands(impl.cmd /* ToDo: try different priority: , 0.5f */);
	impl.image.addPostEnsureAction([&impl]() {
		impl.ctx.deleteObject(impl.cmd);
		if (!(impl.keepStagingBuf))
			impl.ctx.deleteObject(impl.stagingBuf);
	});

	// We're done!
	impl.started = true;
}

unsigned Texture::getWidth (void) const
{
	return TEXTURE_IMPL->sizes[0];
}

unsigned Texture::getHeight (void) const
{
	return TEXTURE_IMPL->sizes[1];
}

unsigned Texture::getDepth (void) const
{
	return TEXTURE_IMPL->sizes[2];
}

unsigned Texture::isCubeMap (void) const
{
	return false; // ToDo: implement cube maps!
}

TexelFormat Texture::getTexelFormat (void) const
{
	return TEXTURE_IMPL->texelFmt;
}


////
// cgvu::DepthStencilBuffer

struct DepthStencilBuffer_impl
{
	bool initialized=false, started=false;

	Context &ctx;

	TexelFormat texelFmt;

	std::vector<ImageObject> images;
	std::vector<BufferObject> rbufs;
	std::vector<void*> rbufMaps;
	CommandBuffer cmdRb;

	unsigned sizes[2];

	DepthStencilBuffer_impl(Context &ctx, unsigned w, unsigned h) : ctx(ctx)
	{
		sizes[0] = w;
		sizes[1] = h;
	}
};
#define DSBUF_IMPL ((DepthStencilBuffer_impl*)pimpl)

DepthStencilBuffer::DepthStencilBuffer() : pimpl(nullptr) {}

DepthStencilBuffer::DepthStencilBuffer(DepthStencilBuffer &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

DepthStencilBuffer::DepthStencilBuffer(Context &ctx, unsigned width, unsigned height,
                                       DepthStencilType type, bool supportReadBack)
	: pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new DepthStencilBuffer_impl(ctx, width, height);
	auto &impl = *DSBUF_IMPL;

	// Find best format for the buffer
	switch (type)
	{
		case DST::DEPTH_STENCIL:
			impl.texelFmt = ctx.bestDepthStencilFormat(true);
			break;

		case DST::DEPTH_ONLY:
			impl.texelFmt = ctx.bestDepthStencilFormat(false);
			break;

		case DST::STENCIL_ONLY:
			impl.texelFmt = TFmt::S_INT8;
			break;

		default:
			throw InvalidOpException(CGVU_LOGMSG(
				"DepthStencilBuffer::<ctor>", "Invalid buffer type specified!"
			));
	}

	// Create one buffer per swapchain image
	unsigned scl = ctx.surface().getSwapchainLength();
	impl.images.reserve(scl);
	impl.images.emplace_back(
		ctx.createImage(IDim::I2D, impl.sizes, impl.texelFmt, supportReadBack)
	);
	impl.initialized = true; // We have something to clean up from here on
	for (unsigned i=1; i<scl; i++)
		impl.images.emplace_back(
			ctx.createImage(IDim::I2D, impl.sizes, impl.texelFmt, supportReadBack)
		);

	// In case of readback support, create one buffer per swapchain image
	if (supportReadBack)
	{
		// Host-side buffers
		size_t imgSize = impl.images[0].size();
		impl.rbufs.reserve(scl);
		impl.rbufMaps.reserve(scl);
		for (unsigned i=0; i<scl; i++)
		{
			impl.rbufMaps.emplace_back(
				impl.rbufs.emplace_back(
					ctx.createBuffer(
						imgSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT,
						  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
						| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
					)
				).map()
			);
		}
		// Readback command buffer
		impl.cmdRb = ctx.createCommandBuffer(TC::TRANSFER_GFX, false);
		impl.cmdRb.startRecording();
		impl.cmdRb.copyRenderImage(
			impl.images.data(), impl.rbufs.data(),
			{ {0, 0, 0, {0,0,0}, {impl.sizes[0], impl.sizes[1], 1}} }
		);
		impl.cmdRb.build();
	}

	// Done!
	impl.started = true;
}

DepthStencilBuffer::~DepthStencilBuffer()
{
	if (pimpl)
	{
		// Shortcut for saving one indirection
		auto &impl = *DSBUF_IMPL;

		// Handle cleanup
		if (impl.initialized)
		{
			// Texture cannot be used from here on anymore
			impl.started = false;

			// Free readback buffers
			for (auto &rbuf : impl.rbufs)
				impl.ctx.deleteObject(rbuf);

			// Free actual image memory.
			for (auto &img : impl.images)
				impl.ctx.deleteObject(img);

			impl.initialized = false;
		}

		delete DSBUF_IMPL;
		pimpl = nullptr;
	}
}

DepthStencilBuffer& DepthStencilBuffer::operator= (DepthStencilBuffer &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

bool DepthStencilBuffer::resize (unsigned newWidth, unsigned newHeight)
{
	// Shortcut for saving one indirection
	auto &impl = *DSBUF_IMPL;

	// Check if we need to do something
	if (newWidth == impl.sizes[0] && newHeight == impl.sizes[1])
		return false;

	// Create new images and discard the old one
	bool supportReadBack = !(impl.rbufs.empty());
	impl.sizes[0] = newWidth; impl.sizes[1] = newHeight;
	for (auto &img : impl.images)
	{
		impl.ctx.deleteObject(img);
		img = impl.ctx.createImage(IDim::I2D, impl.sizes, impl.texelFmt, supportReadBack);
	}
	if (supportReadBack)
	{
		// Resize readback buffers
		size_t imgSize = impl.images[0].size();
		for (unsigned i=0; i<impl.rbufs.size(); i++)
		{
			auto &rbuf = impl.rbufs[i];
			impl.ctx.deleteObject(rbuf);
			rbuf = impl.ctx.createBuffer(
				imgSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT
				| VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
			impl.rbufMaps[i] = rbuf.map();
		}
		// Re-record readback command buffer
		impl.cmdRb.startRecording();
		impl.cmdRb.copyRenderImage(
			impl.images.data(), impl.rbufs.data(),
			{ {0, 0, 0, {0,0,0}, {impl.sizes[0], impl.sizes[1], 1}} }
		);
		impl.cmdRb.build();
	}
	return true;
}

const ImageObject& DepthStencilBuffer::image (signed swapImageID) const
{
	// Shortcut for saving one indirection
	auto &impl = *DSBUF_IMPL;

	unsigned id = swapImageID<0 ? impl.ctx.getSwapImageID() : ((unsigned)swapImageID);
	return impl.images[id];
}

size_t DepthStencilBuffer::size (void) const
{
	return DSBUF_IMPL->images[0].size();
}

unsigned DepthStencilBuffer::getWidth (void) const
{
	return DSBUF_IMPL->sizes[0];
}

unsigned DepthStencilBuffer::getHeight (void) const
{
	return DSBUF_IMPL->sizes[1];
}

TexelFormat DepthStencilBuffer::getTexelFormat (void) const
{
	return DSBUF_IMPL->texelFmt;
}

unsigned DepthStencilBuffer::getTexelSize (void) const
{
	return calcTexelSize(DSBUF_IMPL->texelFmt);
}

bool DepthStencilBuffer::hasDepth (void) const
{
	return DSBUF_IMPL->images[0].hasDepth();
}

bool DepthStencilBuffer::hasStencil (void) const
{
	return DSBUF_IMPL->images[0].hasStencil();
}

bool DepthStencilBuffer::supportsReadback (void) const
{
	return DSBUF_IMPL->images[0].supportsReadback();
}

const unsigned char* DepthStencilBuffer::access (void) const
{
	// Shortcut for saving one indirection
	auto &impl = *DSBUF_IMPL;

	// Issue transfer
	impl.ctx.submitCommands(impl.cmdRb);

	// Actually access contents
	unsigned curImg = impl.ctx.getSwapImageID();
	impl.rbufs[curImg].ensure();

	// Done!
	return (const unsigned char*)(impl.rbufMaps[curImg]);
}

const unsigned char* DepthStencilBuffer::access (const ImageOffset& offset,
                                                 const ImageExtend& extend) const
{
	// ToDo: implement it!
	return nullptr;
}

void DepthStencilBuffer::dumpToFile (const std::string &filename) const
{
	// Shortcut for saving one indirection
	auto &impl = *DSBUF_IMPL;

	// Prepare color data
	size_t dataSize = impl.rbufs[0].size(),
	       texelSize = calcTexelSize(impl.texelFmt),
	       numTexels = dataSize/texelSize;
	auto data = access();
	std::vector<float> depths; depths.reserve(numTexels);
	std::vector<char> pxl; pxl.reserve(numTexels*3);
	float max=-1, min=std::numeric_limits<float>::infinity();
	for (unsigned i=0; i<dataSize; i+=(unsigned)texelSize)
	{
		float z = depths.emplace_back(readDepth(data+i, impl.texelFmt));
		if (z<min) min = z;
		if (z<1.f && z>max) max = z;
	}
	float span = max-min;
	for (unsigned i=0; i<numTexels; i++)
	{
		char c = (char)glm::clamp((((depths[i]-min)/span)*255.f), 0.f, 255.f);
		pxl.emplace_back(c);
		pxl.emplace_back(c);
		pxl.emplace_back(c);
	}

	// Check file type
	std::string ext = getFileExtension(filename);
	if (_stricmp(ext.c_str(), "bmp") == 0)
		stbi_write_bmp(filename.c_str(), impl.sizes[0], impl.sizes[1], 3, pxl.data());
	else if (_stricmp(ext.c_str(), "png") == 0)
		stbi_write_png(
			filename.c_str(), impl.sizes[0], impl.sizes[1], 3, pxl.data(),
			impl.sizes[0]*3
		);
	else
		// ToDo: Implement more formats
		throw err::NotSupportedException(CGVU_LOGMSG(
			"DepthStencilBuffer::dumpToFile", "Unsupported format: " + ext + "!"
		));
}



//////
//
// Function implementations
//

CGVU_API const void* cgvu::addressAtPos (
	const void* mem, TexelFormat fmt, const glm::uvec2& sizes, const glm::uvec2& pos
)
{
	return ((const char*)mem) + calcTexelSize(fmt)*((sizes.x*pos.y) + pos.x);
}

CGVU_API float cgvu::readDepth (const void *mem, TexelFormat fmt)
{
	switch (fmt)
	{
		case TFmt::D_INT16:
		case TFmt::D16S8_INT:
			return float(
					*((uint8_t*)mem) | unsigned(((uint8_t*)mem)[1])<<8
				) / float(65535);

		case TFmt::D_INT24:
		case TFmt::D24S8_INT:
			return float(
					  *((uint8_t*)mem) | unsigned(((uint8_t*)mem)[1])<<8
					| unsigned(((uint8_t*)mem)[2])<<16
				) / float(16777215);

		case TFmt::D_FLT32:
		default:
			return *((float*)mem);
	}
}

CGVU_API float cgvu::readDepthAt (
	const void *mem, TexelFormat fmt, const glm::uvec2 &sizes, const glm::uvec2 &pos
)
{
	return readDepth(addressAtPos(mem, fmt, sizes, pos), fmt);
}



//////
//
// Explicit template instantiations
//

template class cgvu::ManagedObject<ImageObject, Context>;
