
#ifndef __CMD_BUFFER_H__
#define __CMD_BUFFER_H__


//////
//
// Includes
//

// C++ STL
#include <vector>
#include <tuple>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/managed.h"

// Module includes
#include "cgvu/vk/buffer.h"
#include "cgvu/vk/pipeline.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Forward declarations
//

// Vulkan abstraction main classes
class Context;

// Operational abstractions provided by cgvu::Context
class StorageBuffer;
class Pipeline;



//////
//
// Enums
//

/**
 * @brief
 *		Classes of tasks that a @link cgvu::CommandBuffer command buffer @endlink can
 *		process.
 */
enum class TaskClass
{
	/** @brief Indicates a rasterization-based render operation. */
	RENDER,

	/** @brief Indicates a compute operation. */
	COMPUTE,

	/** @brief Indicates a raytracing-based render operation. */
	RAYTRACE,

	/** @brief Indicates a generic resource transfer operation. */
	TRANSFER,

	/**
	 * @brief Indicates a resource transfer operation that depends on render operations.
	 */
	TRANSFER_GFX
};
/** @brief Shorthand for @ref cgvu::TaskClass . */
typedef TaskClass TC;



//////
//
// Structs and typedefs
//

/**
 * @brief
 *		Struct describing an individual buffer binding action for use with @ref
 *		cgvu::CommandBuffer::bindBuffers .
 */
struct BufferBindCmd
{
	/** @brief The buffer to bind. */
	const BufferObject *buffer;

	/**
	 * @brief
	 *		The offset from the start of the buffer indicating where the actual data
	 *		bound to this ID begins.
	 */
	size_t offset;

	/** @brief Constructor for use with unified initializer lists. */
	BufferBindCmd(const BufferObject &buffer, size_t offset)
		: buffer(&buffer), offset(offset)
	{}
};

/** @brief Convenience shorthand for an array of @ref cgvu::BufferBindCmd . */
typedef std::vector<BufferBindCmd> BufferBindCmds;



//////
//
// Function definitions
//

/**
 * @brief
 *		Determines whether the given task class does rendering or not. This is useful for
 *		e.g. determining if sub-buffers will be needed for every swapchain image etc.
 */
inline bool isRenderTask (TaskClass task)
{
	return task == TC::RENDER || task == TC::RAYTRACE;
}



//////
//
// Class definitions
//

/**
 * @brief
 *		Encapsulates a Vulkan command buffer. When requesting one from a @link
 *		cgvu::Context context @endlink , it will be implicitely associated with a certain
 *		queue family, limiting the types of commands it is allowed to contain.
 */
class CGVU_API CommandBuffer : public ManagedObject<CommandBuffer, Context>
{

public:

	////
	// Interfacing

	// Friend declarations
	friend class Context;

	// Explicit inheritance
	using ManagedObject::ManagedObject;
	using ManagedObject::operator=;
	using ManagedObject::operator==;


	////
	// Exported types

	/**
	 * @brief
	 *		Tuple type used for specifiying regions for buffy copy commands. 1st element
	 *		is source offset, 2nd element is destination offset, and 3rd element
	 *		specifies region size in bytes.
	 */
	typedef std::tuple<unsigned, unsigned, unsigned> BufferRegion;

	/**
	 * @brief
	 *		Struct describing a buffer to image region for use with the @ref #copyBuffer
	 *		command.
	 */
	struct BufferImgRegion
	{
		/** @brief Offset from the beginning of the buffer to start copying from/to. */
		size_t bufOffset;

		/**
		 * @brief
		 *		Stride to get from one row (y-dimension) of the image data in the source
		 *		buffer to the next. Setting this to 0 indicates tight row packing.
		 */
		unsigned bufRowStride;

		/**
		 * @brief
		 *		Stride to get from one layer (z-dimension) of the image data in the
		 *		source buffer to the next. Setting this to 0 indicates tight layer
		 *		packing.
		 */
		unsigned bufLayerStride;

		/**
		 * @brief
		 *		The x, y and z coordinates specifying the origin of the target region in
		 *		the image.
		 */
		unsigned imgOffset[3];

		/**
		 * @brief
		 *		The x, y and z extents specifying size of the target region in the image.
		 */
		unsigned imgExtent[3];
	};


protected:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the command buffer in the appropriate command pool given a
	 *		task class and transience.
	 */
	CommandBuffer(Context &context, TaskClass taskClass, bool transient);


	////
	// Methods

	/** @brief Performs cleanup operations. */
	void destroy (void);

	/**
	 * @brief
	 *		Notify the command buffer that one of its per-frame-in-flight sub buffers has
	 *		been dispatched as a result of the command buffer being submitted (used
	 *		internally by @ref cgvu::Context ).
	 */
	void notifySubmit (unsigned subBufferIndex) const;


public:

	////
	// Interface: ManagedObject

	/**
	 * @brief
	 *		Returns a reference to the managing context (see @ref
	 *		cgvu::ManagedObject::manager ).
	 */
	virtual Context& manager (void) override;


	////
	// Methods

	/**
	 * @brief
	 *		Starts the recording process for the command buffer. This will erase all
	 *		previously recorded commands, if any.
	 */
	void startRecording (void);

	/** @brief Records a "copy buffer" command, with another buffer as destination. */
	void copyBuffer (const BufferObject &src, BufferObject &dst,
	                 const std::vector<BufferRegion> &regions);

	/**
	 * @brief
	 *		Records a "copy buffer" command, with an image as destination. If desired,
	 *		texels already in the image are preserved in regions untouched by the copy.
	 */
	void copyBuffer (const BufferObject& src, ImageObject &dst,
	                 const std::vector<BufferImgRegion> &regions,
	                 bool preserveOldTexels=false);

	/**
	 * @brief
	 *		Records a "copy render image" command, with a buffer set as destination. If
	 *		desired, the original attachment-optimal layout of the source render image
	 *		will be restored after the transfer.
	 */
	void copyRenderImage (const ImageObject *srcChain, BufferObject *dstChain,
	                      const std::vector<BufferImgRegion> &regions,
	                      bool preserveSrcLayout=false);

	/** @brief Records a "bind pipeline" command. */
	void bindPipeline (const Pipeline &pipeline);

	/**
	 * @brief
	 *		Records a "bind buffer" command. Note that the pipeline bound via @ref
	 *		#bindPipeline dictates what binding IDs need a buffer assigned for draw
	 *		commands to be successful.
	 */
	void bindBuffer (unsigned bindingID, const BufferObject &buffer, size_t offset=0);

	/**
	 * @brief
	 *		Records a "bind buffers" command, which binds one or more buffers at
	 *		arbitrary offsets to their designated binding IDs at once. Note that the
	 *		pipeline bound via @ref #bindPipeline dictates what binding IDs need a buffer
	 *		assigned for draw commands to be successful.
	 *
	 * @note
	 *		For maximum convenience, use unified initialization to pass the buffers, e.g.
	 *		@code cmd.bindBuffers({{buf1, offset1}, {buf2, offset2}, ...}); @endcode
	 */
	void bindBuffers (unsigned firstBindingID, const BufferBindCmds &buffers);

	/**
	 * @brief
	 *		Records a "bind index buffer" command, enabling subsequent indexed draw
	 *		calls. This does not do any checks to guard against binding buffers which
	 *		were are not created for indexing.
	 */
	void bindIndexBuffer (const BufferObject &indexBuffer, size_t indexWidth);

	/**
	 * @brief
	 *		Convenience method for using @link cgvu::StorageBuffer Storage Buffers
	 *		@endlink as index buffers.
	 */
	void bindIndexBuffer (const StorageBuffer &indexBuffer)
	{
		bindIndexBuffer(indexBuffer, indexBuffer.indexWidth());
	}

	/** @brief Records a "draw" command. */
	void draw (unsigned numVertices, unsigned numInstances, unsigned startVertex,
	           unsigned startInstance);

	/**
	 * @brief
	 *		Convenience method for recording a "draw" command with arbitrary integer
	 *		types for the various counts and indices the command needs.
	 */
	template <class T1, class T2, class T3, class T4>
	void draw (T1 numVertices, T2 numInstances, T3 startVertex, T4 startInstance)
	{
		draw((unsigned)numVertices, (unsigned)numInstances, (unsigned)startVertex,
		     (unsigned)startInstance);
	}

	/** @brief Records a "draw indexed" command. */
	void drawIndexed (
		unsigned numIndices, unsigned numInstances, unsigned startIndex,
		unsigned reserved1=0, unsigned reserved2=0
	);

	/**
	 * @brief
	 *		Convenience method for recording a "draw indexed" command with arbitrary
	 *		integer types for the various counts and indices the command needs.
	 */
	template <class T1, class T2, class T3>
	void drawIndexed (T1 numIndices, T2 numInstances, T3 startIndex)
	{
		drawIndexed((unsigned)numIndices, (unsigned)numInstances, (unsigned)startIndex);
	}

	/** @brief Compiles the command buffer, making it ready for issuing to a queue. */
	void build (void);

	/** @brief Returns an implemention-specific handle to the given sub buffer. */
	Handle handle (unsigned subBufferIndex) const;

	/**
	 * @brief
	 *		Returns the semaphore that is signaled whenever the buffer completes
	 *		execution on the given in-flight frame (0 for non-rendering buffers)
	 */
	Handle semaphore (unsigned currentFrame) const;

	/**
	 * @brief
	 *		Returns the fence that is signaled whenever the buffer completes
	 *		execution on the given in-flight frame (0 for non-rendering buffers)
	 */
	Handle fence (unsigned currentFrame) const;

	/**
	 * @brief
	 *		Returns index of the queue family that the command pool the buffer was
	 *		allocated from belongs to.
	 */
	unsigned queueFamilyIndex (void) const;

	/**
	 * @brief
	 *		Indicates that this command buffer needs to be re-recorded after submission.
	 */
	bool transient (void) const;

	/** @brief Returns the task class of this command buffer. */
	TaskClass taskClass (void) const;

	/** @brief TEMPORARY - WILL BE REMOVED ONCE PROPER MECHANISM IS IN PLACE */
	bool needsFrameSync (void) const;
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}


#endif // ifndef __CMD_BUFFER_H__
