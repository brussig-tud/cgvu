
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <memory>

// Vulkan
#include <vulkan/vulkan.h>

// CGVU includes
#include "error.h"
#include "managed.h"
#include "util/utils.h"
#include "log/logging.h"

// Module includes
#include "vulkan.h"
#include "vulkan_internals.h"

// Implemented header
#include "pipeline.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;



//////
//
// Helper functions
//

// Anonymous namespace begin
namespace {

inline VkFormat toVulkanFmt (AttribFormatDescriptor attribDesc)
{
	switch (attribDesc)
	{
		case AFmt::FLOAT32:    return VK_FORMAT_R32_SFLOAT;
		case AFmt::VEC2_FLT32: return VK_FORMAT_R32G32_SFLOAT;
		case AFmt::VEC3_FLT32: return VK_FORMAT_R32G32B32_SFLOAT;
		case AFmt::VEC4_FLT32: return VK_FORMAT_R32G32B32A32_SFLOAT;
		case AFmt::FLOAT64:    return VK_FORMAT_R64_SFLOAT;
		case AFmt::VEC2_FLT64: return VK_FORMAT_R64G64_SFLOAT;
		case AFmt::VEC3_FLT64: return VK_FORMAT_R64G64B64_SFLOAT;
		case AFmt::VEC4_FLT64: return VK_FORMAT_R64G64B64A64_SFLOAT;
		default:
			throw err::InvalidOpException(CGVU_LOGMSG(
				"::toVulkanFmt", "Unknown format descriptor!"
			));
	}
}

inline VkPrimitiveTopology toVulkanPT (PrimitiveType pt)
{
	switch (pt)
	{
		case PT::POINT_LIST:
			return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		case PT::LINE_LIST:
			return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
		case PT::LINE_STRIP:
			return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
		case PT::TRIANGLE_LIST:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		case PT::TRIANGLE_STRIP:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
		case PT::TRIANGLE_FAN:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
		case PT::LINE_LIST_ADJACENCY:
			return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
		case PT::LINE_STRIP_ADJACENCY:
			return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
		case PT::TRIANGLE_LIST_ADJACENCY:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
		case PT::TRIANGLE_STRIP_ADJACENCY:
			return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
		case PT::PATCH_LIST:
			return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
		default:
			throw err::InvalidOpException(CGVU_LOGMSG(
				"::toVulkanPT", "Unknown primitive type!"
			));
	}
}

// Anonymous namespace end
}



//////
//
// Class implementations
//

////
// cgvu::RenderPass

RenderPass::RenderPass() : pimpl(nullptr)
{
	pimpl = new RenderPass_impl();
}

RenderPass::RenderPass(RenderPass &&other) : pimpl(other.pimpl)
{
	other.pimpl = nullptr;
}

RenderPass::~RenderPass()
{
	if (pimpl)
	{
		delete RP_IMPL;
		pimpl = nullptr;
	}
}

RenderPass& RenderPass::operator= (RenderPass &&other)
{
	// Steal the other's implementation instance
	std::swap(pimpl, other.pimpl);
	return *this;
}

Handle RenderPass::handle (void) const
{
	return RP_IMPL->rp;
}

bool RenderPass::hasDepthStencil (void) const
{
	return RP_IMPL->dsBuf;
}

DepthStencilBuffer& RenderPass::depthStencilBuffer (void)
{
	return *(RP_IMPL->dsBuf);
}

const DepthStencilBuffer& RenderPass::depthStencilBuffer(void) const
{
	return *(RP_IMPL->dsBuf);
}


////
// cgvu::Pipeline

struct PipelineCreateInfo
{
	std::vector<VkVertexInputAttributeDescription> attribDescs;
	std::vector<VkVertexInputBindingDescription> bindingDescs;

	struct Attachment {
		VkAttachmentDescription desc = {};
		VkAttachmentReference ref = {};
	} at_color;
	std::unique_ptr<Attachment> at_ds;

	VkSubpassDescription subpass = {};
	VkSubpassDependency subpassDep = {};
	VkRenderPassCreateInfo rpCI = {};

	VkPipelineVertexInputStateCreateInfo vi = {};
	VkPipelineInputAssemblyStateCreateInfo ia = {};
	VkPipelineRasterizationStateCreateInfo ra = {};
	VkPipelineMultisampleStateCreateInfo ms = {};
	VkPipelineDepthStencilStateCreateInfo ds = {};
	VkPipelineColorBlendAttachmentState ba = {};
	VkPipelineColorBlendStateCreateInfo bl = {};
	VkGraphicsPipelineCreateInfo plCi = {};
};

struct Pipeline_impl
{
	bool initialized = false, started = false;

	Context &ctx;
	const ShaderProgram &shaders;

	PipelineCreateInfo *ci = nullptr;

	VkPipeline handle = VK_NULL_HANDLE;
	VkPipelineLayout layout = VK_NULL_HANDLE;
	RenderPass rp;

	std::vector<VkFramebuffer> *swapchainFBs;

	Pipeline_impl(const ShaderProgram &shaders)
		: ctx(shaders.context()), shaders(shaders)
	{
		// Create the intermediate storage for the creation phase
		ci = new PipelineCreateInfo;
	}
	~Pipeline_impl()
	{
		freeCI();
	}

	void freeCI (void)
	{
		if (ci)
		{
			delete ci;
			ci = nullptr;
		}
	}

	inline VkVertexInputAttributeDescription* fillAttribDescs (const AttribDescs &ad)
	{
		ci->attribDescs.reserve(ad.size());
		for (auto &d : ad)
			ci->attribDescs.emplace_back(VkVertexInputAttributeDescription{
				d.location, d.binding, toVulkanFmt(d.format), d.offset
			});
		return ci->attribDescs.data();
	}

	inline VkVertexInputBindingDescription* fillBindingDescs (
		const BufferBindingDescs &bd
	)
	{
		ci->bindingDescs.reserve(bd.size());
		for (auto &d : bd)
			ci->bindingDescs.emplace_back(VkVertexInputBindingDescription{
				d.binding, d.stride,
				d.isInstance ?
					VK_VERTEX_INPUT_RATE_INSTANCE : VK_VERTEX_INPUT_RATE_VERTEX,
			});
		return ci->bindingDescs.data();
	}
};
#define PIPELINE_IMPL ((Pipeline_impl*)pimpl)

Pipeline::Pipeline(const ShaderProgram &shaders)
{
	// Create implementation instance
	pimpl = new Pipeline_impl(shaders);
	auto &impl = *PIPELINE_IMPL;

	// Input
	impl.ci->vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	impl.ci->ia.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;

	// Rasterization (ToDo: Generalize)
	impl.ci->ra.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	impl.ci->ra.depthClampEnable = false;
	impl.ci->ra.rasterizerDiscardEnable = false;
	impl.ci->ra.polygonMode = VK_POLYGON_MODE_FILL;
	impl.ci->ra.lineWidth = 1.0f;
	impl.ci->ra.cullMode = VK_CULL_MODE_BACK_BIT;
	impl.ci->ra.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	impl.ci->ra.depthBiasEnable = false;
	impl.ci->ra.depthBiasConstantFactor = 0.0f; // Optional
	impl.ci->ra.depthBiasClamp = 0.0f; // Optional
	impl.ci->ra.depthBiasSlopeFactor = 0.0f; // Optional

	// Multisampling (ToDo: Generalize)
	impl.ci->ms.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	impl.ci->ms.sampleShadingEnable = false;
	impl.ci->ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	impl.ci->ms.minSampleShading = 1.0f; // Optional
	impl.ci->ms.pSampleMask = nullptr; // Optional
	impl.ci->ms.alphaToCoverageEnable = false; // Optional
	impl.ci->ms.alphaToOneEnable = false; // Optional

	// ToDo: Generalize
	impl.ci->ds.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	impl.ci->ds.depthTestEnable = true;
	impl.ci->ds.depthWriteEnable = true;
	impl.ci->ds.depthCompareOp = VK_COMPARE_OP_LESS;
	impl.ci->ds.depthBoundsTestEnable = false;
	impl.ci->ds.minDepthBounds = 0; // Optional
	impl.ci->ds.maxDepthBounds = 0; // Optional
	impl.ci->ds.stencilTestEnable = false;
	impl.ci->ds.front = VkStencilOpState(); // Optional
	impl.ci->ds.back = VkStencilOpState(); // Optional

	// Blending (ToDo: Generalize)
	impl.ci->ba.colorWriteMask =
		  VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT
		| VK_COLOR_COMPONENT_A_BIT;
	impl.ci->ba.blendEnable = false;
	impl.ci->ba.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	impl.ci->ba.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	impl.ci->ba.colorBlendOp = VK_BLEND_OP_ADD; // Optional
	impl.ci->ba.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	impl.ci->ba.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	impl.ci->ba.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
	impl.ci->bl.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	impl.ci->bl.logicOpEnable = false;
	impl.ci->bl.logicOp = VK_LOGIC_OP_COPY; // Optional
	impl.ci->bl.attachmentCount = 1;
	impl.ci->bl.pAttachments = &(impl.ci->ba);
	impl.ci->bl.blendConstants[0] = 0.0f; // Optional
	impl.ci->bl.blendConstants[1] = 0.0f; // Optional
	impl.ci->bl.blendConstants[2] = 0.0f; // Optional
	impl.ci->bl.blendConstants[3] = 0.0f; // Optional

	// Pipeline layout
	VkPipelineLayoutCreateInfo plCreateInfo = {};
	plCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	plCreateInfo.setLayoutCount = 1;
	plCreateInfo.pSetLayouts = shaders.getDescSetLayout();
	plCreateInfo.pushConstantRangeCount = 0; // Much too restricted to be useful
	plCreateInfo.pPushConstantRanges = nullptr; // Much too restricted to be useful
	VkResult result = vkCreatePipelineLayout(
		impl.ctx.handle(), &plCreateInfo, nullptr, &impl.layout
	);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Pipeline::<ctor>", "failed to create pipeline layout!"
		));
	impl.initialized = true; // We have something to clean up from here on

	// Render pass (ToDo: Generalize - possibly move into its own class again)
	// - color target description
	auto &colorAttachment = impl.ci->at_color.desc;
	colorAttachment.format =
		((VkSurfaceFormatKHR*)impl.ctx.surface().getColorFormat())->format;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	auto &colorAttachmentRef = impl.ci->at_color.ref;
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	// - subpass description
	auto &subpass = impl.ci->subpass;
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	auto &subpassDep = impl.ci->subpassDep;
	subpassDep.srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDep.dstSubpass = 0;
	subpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDep.srcAccessMask = 0;
	subpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDep.dstAccessMask =
		VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	// - fill out creation form
	auto &rpCreateInfo = impl.ci->rpCI;
	rpCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	rpCreateInfo.attachmentCount = 1;
	rpCreateInfo.pAttachments = &colorAttachment;
	rpCreateInfo.subpassCount = 1;
	rpCreateInfo.pSubpasses = &subpass;
	rpCreateInfo.dependencyCount = 1;
	rpCreateInfo.pDependencies = &subpassDep;

	// Finalize
	// - retrieve shader stage info
	auto &stages = *((const std::vector<VkPipelineShaderStageCreateInfo>*)
	                 shaders.getStageCreationInfo());
	// - fill out top-level create struct
	impl.ci->plCi.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	// ToDo: impl.plCi.flags = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
	impl.ci->plCi.stageCount = (uint32_t)stages.size();
	impl.ci->plCi.pStages = stages.data();
	impl.ci->plCi.pVertexInputState = &(impl.ci->vi);
	impl.ci->plCi.pInputAssemblyState = &(impl.ci->ia);
	impl.ci->plCi.pRasterizationState = &(impl.ci->ra);
	impl.ci->plCi.pMultisampleState = &(impl.ci->ms);
	impl.ci->plCi.pDepthStencilState = &(impl.ci->ds);
	impl.ci->plCi.pColorBlendState = &(impl.ci->bl);
	impl.ci->plCi.layout = impl.layout;
	impl.ci->plCi.subpass = 0;
	impl.ci->plCi.basePipelineHandle = VK_NULL_HANDLE;
	impl.ci->plCi.basePipelineIndex = -1;
}

void Pipeline::destroy (void)
{
	// Shortcut for saving one indirection
	auto &impl = *PIPELINE_IMPL;

	// Handle cleanup
	impl.freeCI();
	if (impl.initialized)
	{
		// Pipeline can not be used from here on anymore
		impl.started = false;

		VkDevice dev = impl.ctx.handle();
		if (impl.handle)
		{
			vkDestroyPipeline(dev, impl.handle, nullptr);
			impl.handle = VK_NULL_HANDLE;
		}
		auto &rpimpl = *RP_PTR(impl.rp);
		if (rpimpl.rp)
		{
			vkDestroyRenderPass(dev, rpimpl.rp, nullptr);
			rpimpl.rp = VK_NULL_HANDLE;
		}
		if (impl.layout)
		{
			vkDestroyPipelineLayout(dev, impl.layout, nullptr);
			impl.layout = VK_NULL_HANDLE;
		}

		impl.initialized = false;
	}

	delete PIPELINE_IMPL;
	pimpl = nullptr;
}

void Pipeline::onRenewedFramebuffer (Handle newFB)
{
	PIPELINE_IMPL->swapchainFBs = newFB;
}

Context& Pipeline::manager (void)
{
	return PIPELINE_IMPL->ctx;
}

void Pipeline::setInputBindings (
	PrimitiveType primitiveType, const AttribDescs &attribs,
	const BufferBindingDescs &bindings
)
{
	// Shortcut for saving one indirection
	auto &impl = *PIPELINE_IMPL;

	// Sanity check
	if (impl.started)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Pipeline::setInputBindings", "Cannot change pipeline state after it has"
			" been built!"
		));

	// Vertex input setup
	impl.ci->vi.vertexAttributeDescriptionCount = (uint32_t)attribs.size();
	impl.ci->vi.pVertexAttributeDescriptions = impl.fillAttribDescs(attribs);
	impl.ci->vi.vertexBindingDescriptionCount = (uint32_t)bindings.size();
	impl.ci->vi.pVertexBindingDescriptions = impl.fillBindingDescs(bindings);

	// Primitve assembly setup
	impl.ci->ia.topology = toVulkanPT(primitiveType);
}

void Pipeline::enablePrimitiveRestart (bool enabled)
{
	// Shortcut for saving one indirection
	auto &impl = *PIPELINE_IMPL;

	// Sanity check
	if (impl.started)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Pipeline::enablePrimitiveRestart", "Cannot change pipeline state after it"
			" has been built!"
		));

	// Set it
	impl.ci->ia.primitiveRestartEnable = enabled;
}

void Pipeline::attachDepthStencilBuffer (
	const DepthStencilBuffer &buffer, DepthStencilStoreOps store, DepthStencilClearOps clear
)
{
	// Shortcut for saving one indirection
	auto &impl = *PIPELINE_IMPL;

	// Sanity check
	if (impl.started)
		throw err::InvalidOpException(CGVU_LOGMSG(
			"Pipeline::attachDepthStencilBuffer", "Cannot change pipeline state after it"
			" has been built!"
		));

	// Create entry for the depth/stencil attachment if necessary
	if (!(impl.ci->at_ds))
		impl.ci->at_ds.reset(new PipelineCreateInfo::Attachment);

	// Add info about the attachment and reference it properly
	// - preliminaries
	auto &templateImg = buffer.image(0);
	const bool
		clearDepth = templateImg.hasDepth() && util::bitTest(clear, DSclear::DEPTH),
		storeDepth = templateImg.hasDepth() && util::bitTest(store, DSstore::DEPTH),
		clearStncl = templateImg.hasStencil() && util::bitTest(clear, DSclear::STENCIL),
		storeStncl = templateImg.hasStencil() && util::bitTest(store, DSstore::STENCIL);
	// - descriptor
	//   ToDo: revisit load & store OPs once render passes are properly implemented
	auto &dsAtt = impl.ci->at_ds->desc;
	dsAtt.format = (VkFormat)(size_t)templateImg.getInternalFormat();
	dsAtt.samples = VK_SAMPLE_COUNT_1_BIT;
	dsAtt.loadOp = clearDepth ?
		VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	dsAtt.storeOp = storeDepth ?
		VK_ATTACHMENT_STORE_OP_STORE : VK_ATTACHMENT_STORE_OP_DONT_CARE;
	dsAtt.stencilLoadOp = clearStncl ?
		VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	dsAtt.stencilStoreOp = storeStncl ?
		VK_ATTACHMENT_STORE_OP_STORE : VK_ATTACHMENT_STORE_OP_DONT_CARE;
	dsAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	dsAtt.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	// - reference
	auto &dsRef = impl.ci->at_ds->ref;
	dsRef.attachment = 1;
	dsRef.layout = dsAtt.finalLayout;
	// - subpass entry
	impl.ci->subpass.pDepthStencilAttachment = &dsRef;
	// - reference the depth buffer
	RP_PTR(impl.rp)->dsBuf = const_cast<DepthStencilBuffer*>(&buffer);
}

void Pipeline::build (void)
{
	// Shortcut for saving one indirection
	auto &impl = *PIPELINE_IMPL;
	auto &rpimpl = *RP_PTR(impl.rp);

	// Create render pass
	// - compile attachments
	VkAttachmentDescription ats[2];
	if (impl.ci->at_ds)
	{
		ats[0] = impl.ci->at_color.desc; ats[1] = impl.ci->at_ds->desc;
		impl.ci->rpCI.attachmentCount = 2;
		impl.ci->rpCI.pAttachments = ats;
	}
	// - actually create the render pass
	VkResult result = vkCreateRenderPass(
		impl.ctx.handle(), &(impl.ci->rpCI), nullptr, &(rpimpl.rp)
	);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Pipeline::<ctor>", "failed to create render pass descriptor!"
		));
	impl.ci->plCi.renderPass = rpimpl.rp;

	// ToDo: make viewport user-definable
	VkPipelineViewportStateCreateInfo vp = {};
	VkViewport viewport = *(VkViewport*)(impl.ctx.getViewport());
	VkRect2D scissor = {};
	scissor.extent = { uint32_t(viewport.width), uint32_t(viewport.height) };
	vp.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	vp.viewportCount = 1;
	vp.pViewports = &viewport;
	vp.scissorCount = 1;
	vp.pScissors = &scissor;
	impl.ci->plCi.pViewportState = &vp;

	// Dynamic state (ToDo: cannot be user-defined for now - should it even be?)
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR
	};
	VkPipelineDynamicStateCreateInfo dys = {};
	dys.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dys.dynamicStateCount = 2;
	dys.pDynamicStates = dynamicStates;
	impl.ci->plCi.pDynamicState = &dys;

	// Create actual pipeline object
	result = vkCreateGraphicsPipelines(
		impl.ctx.handle(), VK_NULL_HANDLE, 1, &(impl.ci->plCi), nullptr, &(impl.handle)
	);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"Pipeline::build", "Failed to create Vulkan pipeline object!"
		));

	// Obtain suitable framebuffers from swapchain
	// ToDo: generalize to offscreen surfaces
	impl.swapchainFBs =
		(std::vector<VkFramebuffer>*)impl.ctx.surface().obtainFramebuffer(impl.rp);

	// Clean up intermediates
	impl.freeCI();

	// Done!
	impl.started = true;
}

bool Pipeline::isBuilt(void) const
{
	return PIPELINE_IMPL->started;
}

Handle Pipeline::handle (void) const
{
	return PIPELINE_IMPL->handle;
}

Handle Pipeline::getLayout(void) const
{
	return PIPELINE_IMPL->layout;
}

ShaderProgram& Pipeline::shaderProg (void)
{
	return const_cast<ShaderProgram&>(PIPELINE_IMPL->shaders);
}

const ShaderProgram& Pipeline::shaderProg (void) const
{
	return PIPELINE_IMPL->shaders;
}

const RenderPass& Pipeline::renderPass (void) const
{
	return PIPELINE_IMPL->rp;
}

Handle Pipeline::fbi (unsigned swapchainImageID) const
{
	return PIPELINE_IMPL->swapchainFBs->at(swapchainImageID);
}



//////
//
// Explicit template instantiations
//

template class cgvu::ManagedObject<Pipeline, Context>;
