
#ifndef __SETTINGS_H__
#define __SETTINGS_H__


//////
//
// Includes
//

// C++ STL
#include <string>
#include <any>

// Library config
#include "libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {



//////
//
// Class definitions
//

/** @brief Encapsulates settings in a key-value like fashion. */
class CGVU_API Settings
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


public:

	////
	// Object construction / destruction

	/** @brief The default constructor. */
	Settings();

	/** @brief The destructor. */
	virtual ~Settings();


	////
	// Methods

	/** @brief Assign a value to a setting. */
	void set (const std::string &name, std::any value);

	/** @brief Retrieve a setting. */
	const std::any& get (const std::string& name) const;

	/** @brief Check if a setting has been set to something. */
	bool isSet (const std::string& name) const;
};



//////
//
// Namespace closings
//

// Root namespace: cgvu
}



#endif // ifndef __SETTINGS_H__
