
//////
//
// Includes
//

// C++ STL
#include <map>

// CGVu includes
#include "util/utils.h"
#include "log/logging.h"

// Implemented header
#include "settings.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;



//////
//
// Class implementations
//

////
// cgvu::Settings

struct Settings_impl
{
	std::map<std::string, std::any> db;
	const std::any uninitialized;
};
#define SETTINGS_IMPL ((Settings_impl*)pimpl)

Settings::Settings() : pimpl(nullptr)
{
	
	// Create implementation instance
	pimpl = new Settings_impl();
}

Settings::~Settings()
{
	// De-allocate implementation instance
	if (pimpl)
		delete SETTINGS_IMPL;
}

void Settings::set (const std::string& name, std::any value)
{
	SETTINGS_IMPL->db[name] = std::move(value);
}

const std::any& Settings::get (const std::string& name) const
{
	// Shortcut for saving one indirection
	auto &impl = *SETTINGS_IMPL;

	// Forward to map implementation
	auto it = impl.db.find(name);
	if (it != impl.db.end())
		return it->second;
	else
		return impl.uninitialized;
}

bool Settings::isSet (const std::string& name) const
{
	auto &impl = *SETTINGS_IMPL; // Shortcut for saving one indirection
	return impl.db.find(name) != impl.db.end();
}
