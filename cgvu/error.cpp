
//////
//
// Includes
//

// C++ STL
#include <string>
#include <sstream>
#include <utility>

// CGVU includes
#include "log/logging.h"

// Implemented header
#include "error.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::err;



//////
//
// Interface pre-implementations
//

////
// cgvu::err::Exception

struct Exception_impl
{
	std::string what;
	log::EventSource src;

	Exception_impl(const log::EventSource &src) noexcept : src(src) {}
	Exception_impl(log::EventSource &&src) noexcept : src(std::move(src)) {}
};
#define EXCEPTION_IMPL ((Exception_impl*)pimpl)

err::Exception::Exception(const log::EventSource &source) noexcept
	: pimpl(nullptr)
{
	pimpl = new Exception_impl(source);
}

err::Exception::Exception(log::EventSource &&source) noexcept
	: pimpl(nullptr)
{
	pimpl = new Exception_impl(std::move(source));
}

err::Exception::~Exception() noexcept
{
	if (pimpl)
		delete EXCEPTION_IMPL;
}

const char* err::Exception::what (void) const noexcept
{
	// Shortcut for saving one indirection
	auto &impl = *EXCEPTION_IMPL;

	// Lazy description building
	if (impl.what.empty())
	{
		std::stringstream sdesc;
		sdesc << std::move(impl.src.msg) << std::endl << getDescription();
		impl.src.msg = sdesc.str();
		impl.what = log::formatEvent(impl.src, log::ERROR);
	}

	// Return message
	return impl.what.c_str();
}

const log::EventSource& err::Exception::eventSource (void) const
{
	return EXCEPTION_IMPL->src;
}



//////
//
// Exception implementations
//

////
// cgvu::err::InvalidOpException

err::InvalidOpException::InvalidOpException(const log::EventSource &source) noexcept
	: Exception(source)
{}

err::InvalidOpException::InvalidOpException(log::EventSource &&source) noexcept
	: Exception(std::move(source))
{}

std::string err::InvalidOpException::getDescription(void) const
{
	return "Invalid operation!";
}


////
// cgvu::err::NotSupportedException

err::NotSupportedException::NotSupportedException(const log::EventSource &source)noexcept
	: Exception(source)
{}

err::NotSupportedException::NotSupportedException(log::EventSource &&source) noexcept
	: Exception(std::move(source))
{}

std::string err::NotSupportedException::getDescription(void) const
{
	return "Not supported!";
}


////
// cgvu::err::FileOpenException

struct FileOpenException_impl
{
	err::FileOpenException::AccessType type;
	std::string fn;

	FileOpenException_impl(
		err::FileOpenException::AccessType type, const std::string& fn
	) noexcept
		: type(type), fn(fn)
	{}

	const char* typeStr (void)
	{
		switch (type)
		{
			case err::FileOpenException::READ:      return "'READ'";
			case err::FileOpenException::WRITE:     return "'WRITE'";
			case err::FileOpenException::READWRITE: return "'READ/WRITE'";
		}
		return "<UNKNOWN_MODE>";
	}
};
#define FOEXCEPTION_IMPL ((FileOpenException_impl*)pimpl)

err::FileOpenException::FileOpenException(
	AccessType type, const std::string& filename, const log::EventSource& source
) noexcept
	: Exception(source), pimpl(nullptr)
{
	pimpl = new FileOpenException_impl(type, filename);
}

err::FileOpenException::FileOpenException(
	AccessType type, const std::string& filename, log::EventSource&& source
) noexcept
	: Exception(std::move(source)), pimpl(nullptr)
{
	pimpl = new FileOpenException_impl(type, filename);
}

err::FileOpenException::~FileOpenException() noexcept
{
	if (pimpl)
		delete FOEXCEPTION_IMPL;
}

std::string err::FileOpenException::getDescription (void) const
{
	// Shortcut for saving one indirection
	auto& impl = *FOEXCEPTION_IMPL;

	// Build description
	std::stringstream desc;
	desc << "Error opening file: \"" << impl.fn << "\"" << std::endl
	     << "  Attempt to open in mode " << impl.typeStr() << " failed!";

	// Done!
	return std::move(desc.str());
}
