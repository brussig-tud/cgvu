
#ifndef __LOGGING_H__
#define __LOGGING_H__


//////
//
// Includes
//

// C++ STL
#include <string>
#include <functional>
#include <algorithm>

// Library config
#include "cgvu/libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {

// Module namespace
namespace log {



//////
//
// Macros and constants
//

/** @brief Calls @ref log::createEvent with current source file and line number. */
#define CGVU_LOGMSG(sender, msg) \
	cgvu::log::createEvent((sender), (msg), __FILE__, __LINE__)

/**
 * @brief
 *		Calls @ref log::formatEvent on the result of @ref log::createEvent called with
 *		current source file and line number.
 */
#define CGVU_TXTMSG(sender, msg, sev) \
	cgvu::log::formatEvent(cgvu::log::createEvent((sender), (msg), __FILE__, __LINE__), (sev))



//////
//
// Enums
//

/** @brief Event severity. */
enum Severity
{
    ERROR   = 0,
    WARNING = 1,
    INFO    = 2,
    VERBOSE = 3
};



//////
//
// Structs
//

/** @brief Encapsulates the source of a log event. */
struct EventSource
{
	std::string sender, msg;
	const char* file;
	unsigned lineNumber;
	unsigned code;
};



//////
//
// Function definitions
//

/** @brief Creates a log event source, copying all std::strings. */
CGVU_API EventSource createEvent (const std::string &sender, const std::string &msg,
                                  const char *file=nullptr, unsigned lineNumber=-1);

/**
 * @brief Creates a log event source, moving the sender string and copying the message.
 */
CGVU_API EventSource createEvent(std::string &&sender, const std::string &msg,
                                 const char* file=nullptr, unsigned lineNumber=-1);

/**
 * @brief Creates a log event source, copying the sender string and moving the message.
 */
CGVU_API EventSource createEvent(const std::string &sender, std::string &&msg,
                                 const char* file=nullptr, unsigned lineNumber=-1);

/** @brief Creates a log event source, moving all std::strings. */
CGVU_API EventSource createEvent(std::string &&sender, std::string &&msg,
                                 const char* file=nullptr, unsigned lineNumber=-1);

/** @brief Formats a log event as plain text. */
CGVU_API std::string formatEvent (const cgvu::log::EventSource &source, cgvu::log::Severity severity);



//////
//
// Namespace closings
//

// Module namespace: log
}

// Root namespace: cgvu
}


#endif // ifndef __LOGGING_H__
