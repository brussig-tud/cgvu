
//////
//
// Includes
//

// C++ STL
#include <string>
#include <sstream>
#include <utility>

// Implemented header
#include "logging.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::log;



//////
//
// Function implementations
//

EventSource cgvu::log::createEvent (const std::string &sender, const std::string &msg,
                                    const char* file, unsigned lineNumber)
{
	EventSource ret;
	ret.sender = sender;
	ret.msg = msg;
	ret.file = file;
	ret.lineNumber = lineNumber;
	return ret;
}

EventSource cgvu::log::createEvent (std::string &&sender, const std::string &msg,
                                    const char* file, unsigned lineNumber)
{
	EventSource ret;
	ret.sender = std::move(sender);
	ret.msg = msg;
	ret.file = file;
	ret.lineNumber = lineNumber;
	return ret;
}

EventSource cgvu::log::createEvent (const std::string &sender, std::string &&msg,
                                    const char* file, unsigned lineNumber)
{
	EventSource ret;
	ret.sender = sender;
	ret.msg = std::move(msg);
	ret.file = file;
	ret.lineNumber = lineNumber;
	return ret;
}

EventSource cgvu::log::createEvent (std::string &&sender, std::string &&msg,
                                    const char* file, unsigned lineNumber)
{
	EventSource ret;
	ret.sender = std::move(sender);
	ret.msg = std::move(msg);
	ret.file = file;
	ret.lineNumber = lineNumber;
	return ret;
}

std::string cgvu::log::formatEvent (const EventSource &source, Severity severity)
{
	std::stringstream str;
	str << "[" << source.sender << "] ";
	if ((source.file && source.file[0]) && int(source.lineNumber) < 0)
		str << source.file << ":" << std::endl;
	else if (source.lineNumber)
		str << source.file << ", line " << source.lineNumber << ":" << std::endl;
	switch (severity)
	{
		case VERBOSE: str << "(verbose) "; break;
		case INFO: break;
		case WARNING: str << "WARNING: "; break;
		case ERROR: str << "!!!ERROR!!! "; break;
		default: str << "(UNKNOWN_SEVERITY) ";
	}
	str << source.msg;
	return std::move(str.str());
}
