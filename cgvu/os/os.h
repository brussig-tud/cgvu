
#ifndef __OS_H__
#define __OS_H__


//////
//
// Includes
//

// C++ STL
#include <string>

// Library config
#include "cgvu/libconfig.h"

// CGVU includes
#include "cgvu/error.h"
#include "cgvu/log/logging.h"



//////
//
// Exception declarations
//

// Error namespace
namespace cgvu::err {

/** @brief Indicates an error reported by the operating system. */
class OSException : public Exception
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


protected:

	////
	// Interface: cgvu::err::Exception

	/** @brief See @ref cgvu::err::Exception::getDescription . */
	virtual std::string getDescription (void) const;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the OS exception, automatically querying the error cause from the
	 *		operating system. For further semantics, see @ref
	 *		cgvu::err::Exception::Exception(const log::EventSource &) .
	 */
	OSException(const log::EventSource &source) noexcept;

	/**
	 * @brief
	 *		Constructs the OS exception, automatically querying the error cause from the
	 *		operating system. For further semantics, see @ref
	 *		cgvu::err::Exception::Exception(log::EventSource &&) for further semantics.
	 */
	OSException(log::EventSource &&source) noexcept;

	/**
	 * @brief
	 *		Constructs the OS exception for the given OS error code. For further,
	 *		semantics, see @ref cgvu::err::Exception::Exception(const log::EventSource &)
	 */
	OSException(size_t errorCode, const log::EventSource& source) noexcept;

	/**
	 * @brief
	 *		Constructs the OS exception for the given OS error code. For further,
	 *		semantics, see @ref cgvu::err::Exception::Exception(log::EventSource &&)
	 */
	OSException(size_t errorCode, log::EventSource&& source) noexcept;
};

// Close error namespace
}



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {

// Module namespace
namespace os {



//////
//
// Enums
//

/** @brief Enum used to indicate */
enum StandardStream
{
	NONE = 0,
	OUT = 1,
	ERR = 2,
	OUTERR = OUT | ERR,
	IN = 4,
	INOUT = OUT | IN,
	INERR = ERR | IN,
	ALL   = OUT | ERR | IN
};



//////
//
// Functions
//

/**
 * @brief Returns a system-defined description string for the given error code.
 *
 * @param errorCode
 *		The error code to try and find a description for. Will only work with error codes
 *		specific to the underlying platform.
 */
CGVU_API std::string getSystemErrorMsg (unsigned errorCode);



//////
//
// Class definitions
//

/** @brief Handles dispatching and controlling a child process. */
class CGVU_API ChildProcess
{

private:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


public:

	////
	// Object construction / destruction

	/** @brief Constructs an instance for the given executable file. */
	ChildProcess(const std::string &executable);

	/** @brief The destructor. */
	virtual ~ChildProcess();


	////
	// Methods

	/**
	 * @brief
	 *		Dispatches the process, optionally with command line arguments. No standard
	 *		streams will be redirected.
	 */
	void run (const std::string &args="");

	/** @brief Dispatches the process, redirecting the indicated standard streams. */
	void run (StandardStream redirect);

	/**
	 * @brief
	 *		Dispatches the process with the given command line arguments, redirecting the
	 *		indicated standard streams.
	 */
	void run (const std::string& args, StandardStream redirect);

	/**
	 * @brief
	 *		Writes to the standard input of the child process. Returns true as long as
	 *		the child process uses is initial stdin, and false as soon as it closes its
	 *		end of the stream (e.g. because the process is shutting down).
	 */
	bool writeStdIn (const void *data, size_t numBytes);

	/**
	 * @brief
	 *		Reads from the standard output of the child process. Returns true as long as
	 *		the child process uses is initial stdout, and false as soon as it closes its
	 *		end of the stream (e.g. because the process is shutting down).
	 */
	bool readStdOut (std::vector<char> *buf, size_t maxBytes);

	/**
	 * @brief
	 *		Reads from the error output of the child process. Returns true as long as the
	 *		child process uses is initial stderr, and false as soon as it closes its end
	 *		of the stream (e.g. because the process is shutting down).
	 */
	bool readStdErr (std::vector<char>* buf, size_t maxBytes);

	/** @brief Waits for the process to exit and returns its exit code. */
	int wait (void) const;

	/**
	 * @brief
	 *		Makes the process terminate. Equivalent to pressing Ctrl-C in a terminal.
	 *		Note that the child process can handle and ignore this.
	 */
	void requestTermination (void) const;
};



//////
//
// Namespace closings
//

// Module namespace: os
}

// Root namespace: cgvu
}


#endif // ifndef __OS_H__
