
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <string>
#include <sstream>
#include <utility>
#include <thread>

// OS APIs
#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <Windows.h>
	#undef ERROR
	#undef IN
	#undef OUT
	#define OS_STR "Win32"
	#define CGVU_GET_LAST_ERROR (GetLastError())
	#define UNPACK_ERROR_CODE (util::fromVoidP<DWORD>(pimpl))
#else
	#include <sys/wait.h>
	#include <unistd.h>
	#include <cstdio>
	#include <cerrno>
	#include <clocale>
	#define OS_STR "POSIX"
	#define CGVU_GET_LAST_ERROR (errno)
	#define UNPACK_ERROR_CODE (util::fromVoidP<int>(pimpl))
#endif

// CGVU includes
#include "error.h"
#include "log/logging.h"
#include "util/utils.h"

// Implemented header
#include "os.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::os;



//////
//
// Exception implementations
//

////
// cgvu::err::OSException

err::OSException::OSException(const log::EventSource &source) noexcept
	: Exception(source), pimpl(util::toVoidP(CGVU_GET_LAST_ERROR))
{}

err::OSException::OSException(log::EventSource &&source) noexcept
	: Exception(std::move(source)), pimpl(util::toVoidP(CGVU_GET_LAST_ERROR))
{}

err::OSException::OSException(
	size_t errorCode, const log::EventSource &source
) noexcept
	: Exception(source), pimpl((void*)errorCode)
{}

err::OSException::OSException(
	size_t errorCode, log::EventSource &&source
) noexcept
	: Exception(std::move(source)), pimpl((void*)errorCode)
{}

std::string err::OSException::getDescription (void) const
{
	// Build description
	std::stringstream buf;
	buf << OS_STR" error: E_" << UNPACK_ERROR_CODE << std::endl
	    << "  " << os::getSystemErrorMsg(UNPACK_ERROR_CODE);
	return std::move(buf.str());
}



//////
//
// Function implementations
//

std::string os::getSystemErrorMsg (unsigned errorCode)
{
#ifdef WIN32
	char buf [2048];
	if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nullptr, errorCode, 0, buf,
		2048, nullptr))
		return buf;
	else
		return "<UNABLE TO OBTAIN ERROR DESCRIPTION>";
#else
	return std::strerror(errorCode);
#endif
}



//////
//
// Class implementations
//

////
// cgvu::util::ChildProcess

struct ChildProcess_impl
{
	std::string filename;

#ifdef WIN32
	PROCESS_INFORMATION pi;
	HANDLE stdin_wr, stdout_rd, stderr_rd;

	inline ChildProcess_impl()
		: pi{}, stdin_wr(nullptr), stdout_rd(nullptr), stderr_rd(nullptr)
	{
	}
	inline ~ChildProcess_impl()
	{
		if (pi.hThread) { CloseHandle(pi.hThread); pi.hThread=nullptr; }
		if (pi.hProcess) { CloseHandle(pi.hProcess); pi.hProcess=nullptr; }
		if (stdin_wr) { CloseHandle(stdin_wr); stdin_wr=nullptr; }
		if (stdout_rd) { CloseHandle(stdout_rd); stdout_rd=nullptr; }
		if (stderr_rd) { CloseHandle(stderr_rd); stderr_rd=nullptr; }
	}

	inline void run (const std::string &args, StandardStream redirect)
	{
		// Configure child process startup
		STARTUPINFO si = {};
		si.cb = sizeof(STARTUPINFO);
		si.dwFlags = redirect ? STARTF_USESTDHANDLES : NULL;

		// Setup standard stream redirection
		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.bInheritHandle = true;
		sa.lpSecurityDescriptor = nullptr;
		if (redirect & StandardStream::IN)
		{
			if (!CreatePipe(&(si.hStdInput), &stdin_wr, &sa, 0))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for STDIN"
					            " redirection - CreatePipe() failed!")
				);
			if (!SetHandleInformation(stdin_wr, HANDLE_FLAG_INHERIT, false))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for STDIN"
					            " redirection - SetHandleInformation() failed!")
				);
		}
		if (redirect & StandardStream::OUT)
		{
			if (!CreatePipe(&stdout_rd, &(si.hStdOutput), &sa, 0))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for"
					            " STDOUT redirection - CreatePipe() failed!")
				);
			if (!SetHandleInformation(stdout_rd, HANDLE_FLAG_INHERIT, false))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for"
					            " STDOUT redirection - SetHandleInformation() failed!")
				);
		}
		if (redirect & StandardStream::ERR)
		{
			if (!CreatePipe(&stderr_rd, &(si.hStdError), &sa, 0))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for"
					            " STDERR redirection - CreatePipe() failed!")
				);
			if (!SetHandleInformation(stderr_rd, HANDLE_FLAG_INHERIT, false))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to create unnamed pipe for"
					            " STDERR redirection - SetHandleInformation() failed!")
				);
		}

		// Create child process
		if (!CreateProcess(
			nullptr, const_cast<LPSTR>((filename+" "+args).c_str()), nullptr, nullptr,
			true, NORMAL_PRIORITY_CLASS, nullptr, nullptr, &si, &pi))
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Unable to dispatch new process!")
			);

		// Clean up in case of standard stream redirection
		if (si.hStdInput)
			if (!CloseHandle(si.hStdInput))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to set up unnamed pipe for STDIN"
					            " redirection - CloseHandle() failed!")
				);
		if (si.hStdOutput)
			if (!CloseHandle(si.hStdOutput))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to set up unnamed pipe for"
					            " STDOUT redirection - CloseHandle() failed!")
				);
		if (si.hStdError)
			if (!CloseHandle(si.hStdError))
				throw err::OSException(
					CGVU_LOGMSG("ChildProcess", "Unable to set up unnamed pipe for"
					            " STDERR redirection - CloseHandle() failed!")
				);
	}
	inline bool writeStdIn (const void *data, size_t numBytes)
	{
		bool pipeIntact = true;
		if (stdin_wr)
		{
			DWORD bytesWritten=0;
			if (!WriteFile(stdin_wr, data, (DWORD)numBytes, &bytesWritten, nullptr))
			{
				DWORD errorCode = GetLastError();
				if (errorCode == ERROR_BROKEN_PIPE)
					pipeIntact = false;
				else
					throw err::OSException(errorCode, CGVU_LOGMSG(
						"ChildProcess::writeStdIn", "Unable to write to standard input"
						" of child process!"
					));
			}
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::writeStdIn", "Child process not started with standard"
				" input redirection!"
			));

		return pipeIntact;
	}
	inline bool readStdOut (std::vector<char> *buf, size_t maxBytes)
	{
		bool pipeIntact = true;
		if (stdout_rd)
		{
			DWORD bytesRead=0;
			buf->resize(maxBytes);
			if (!ReadFile(stdout_rd, buf->data(), (DWORD)maxBytes, &bytesRead, nullptr))
			{
				DWORD errorCode = GetLastError();
				if (errorCode == ERROR_BROKEN_PIPE)
					pipeIntact = false;
				else
					throw err::OSException(errorCode, CGVU_LOGMSG(
						"ChildProcess::readStdOut", "Unable to read from standard output"
						" of child process!"
					));
			}
			buf->resize(bytesRead);
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::readStdOut", "Child process not started with standard"
				" output redirection!"
			));

		return pipeIntact;
	}
	inline bool readStdErr (std::vector<char> *buf, size_t maxBytes)
	{
		bool pipeIntact = true;
		if (stderr_rd)
		{
			DWORD bytesRead=0;
			buf->resize(maxBytes);
			if (!ReadFile(stderr_rd, buf->data(), (DWORD)maxBytes, &bytesRead, nullptr))
			{
				DWORD errorCode = GetLastError();
				if (errorCode == ERROR_BROKEN_PIPE)
					pipeIntact = false;
				else
					throw err::OSException(errorCode, CGVU_LOGMSG(
						"ChildProcess::readStdErr", "Unable to read from error output of"
						" child process!"
					));
			}
			buf->resize(bytesRead);
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::readStdErr", "Child process not started with error output"
				" redirection!"
			));

		return pipeIntact;
	}
	inline int wait (void) const
	{
		// Block until completion
		if (WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED)
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Waiting for child process failed!")
			);

		// Get the exit code
		DWORD exitCode;
		if (!GetExitCodeProcess(pi.hProcess, &exitCode))
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Unable to query child process exit code!")
			);

		// Report exit code
		return (int)exitCode;
	}
	inline void sigint (void) const
	{
		if (!AttachConsole(pi.dwProcessId))
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Unable to signal termination to child"
				            " process - AttachConsole() failed!")
			);
		util::Finally attachConsolefinalizer([]() {
			// Undo attach console
			FreeConsole();
		});

		// Temporarily disable ctrl-c handling for our program
		if (!SetConsoleCtrlHandler(nullptr, true))
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Unable to signal termination to child"
				            " process - SetConsoleCtrlHandler() failed!")
			);
		util::Finally ctrlCfinalizer( []() {
			// Restore normal ctrl-c handling
			// ToDo: verify that this indeed restores previously active custom handlers,
			//       as the documentation seems to imply
			SetConsoleCtrlHandler(nullptr, false);
		});

		// Sent Ctrl-C to the attached console
		if (!GenerateConsoleCtrlEvent(CTRL_C_EVENT, 0))
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess", "Unable to signal termination to child"
				            " process - GenerateConsoleCtrlEvent() failed!")
			);
	}
#else
	pid_t pid;
	int stdIn[2]={0}, stdOut[2]={0}, stdErr[2]={0};
	#define PIPE_READ_END 0
	#define PIPE_WRITE_END 1

	inline static bool isWhitespace (const char c)
	{
		return c == ' ' || c == '\t' || c == '\n' || c == '\r';
	}

	struct ArgV
	{
		char *buf;
		std::vector<char*> v;

		ArgV(const std::string &filename, const std::string &args) : buf(nullptr)
		{
			buf = new char [args.length()+1];
			v.push_back(const_cast<char*>(filename.c_str()));
			char *pbuf = buf;
			bool inArg = false;
			for (size_t i=0; i<args.length(); i++)
			{
				if (inArg)
				{
					if (isWhitespace(args[i]))
					{
						*pbuf = 0;
						inArg = false;
					}
					else
						*pbuf = args[i];
					pbuf++;
				}
				else if (!isWhitespace(args[i]))
				{
					v.push_back(pbuf);
					inArg = true;
					*(pbuf++) = args[i];
				}
			}
			*pbuf = 0;
			v.push_back(NULL);
		}
		~ArgV() { if (buf) delete[] buf; }
	};

	inline ChildProcess_impl() : pid(0)
	{
	}
	inline ~ChildProcess_impl()
	{
		if (stdIn[PIPE_READ_END]) close(stdIn[PIPE_READ_END]);
		if (stdIn[PIPE_WRITE_END]) close(stdIn[PIPE_WRITE_END]);
		if (stdOut[PIPE_READ_END]) close(stdOut[PIPE_READ_END]);
		if (stdOut[PIPE_WRITE_END]) close(stdOut[PIPE_WRITE_END]);
		if (stdErr[PIPE_READ_END]) close(stdErr[PIPE_READ_END]);
		if (stdErr[PIPE_WRITE_END]) close(stdErr[PIPE_WRITE_END]);
	}

	inline void run (const std::string &args, StandardStream redirect)
	{
		// Create pipes for requested redirection
		if (redirect & StandardStream::IN)
			if (pipe(stdIn) != 0)
				throw err::OSException(CGVU_LOGMSG(
					"ChildProcess::run", "Unable to create pipe for STDIN redirection"
					" - pipe() failed!"
				));
		if (redirect & StandardStream::OUT)
			if (pipe(stdOut) != 0)
				throw err::OSException(CGVU_LOGMSG(
					"ChildProcess::run", "Unable to create pipe for STDOUT redirection"
					" - pipe() failed!"
				));
		if (redirect & StandardStream::ERR)
			if (pipe(stdErr) != 0)
				throw err::OSException(CGVU_LOGMSG(
					"ChildProcess::run", "Unable to create pipe for STDERR redirection"
					" - pipe() failed!"
				));

		// Perform the fork
		pid = fork();
		if (pid < 0)
			throw err::OSException(
				CGVU_LOGMSG("ChildProcess::run", "Unable to fork process!")
			);

		// Apply redirection
		if (redirect & StandardStream::IN)
		{
			if (pid > 0)
			{
				// Parent
				if (close(stdIn[PIPE_READ_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDIN failed while closing"
						" the unused pipe end of the parent!"
					));
				stdIn[PIPE_READ_END] = 0;
			}
			else
			{
				// Child - dupe the read end of the pipe to stdin
				if (dup2(stdIn[PIPE_READ_END], STDIN_FILENO) < 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDIN failed while"
						" duplicating the file descriptor of the pipe end!"
					));

				// Let go of the pipe ends (the duping will remain intact)
				if (close(stdIn[PIPE_READ_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDIN failed while closing"
						" the read end of the pipe on the child side!"
					));
				if (close(stdIn[PIPE_WRITE_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDIN failed while closing"
						" the write end of the pipe on the child side!"
					));
			}
		}
		if (redirect & StandardStream::OUT)
		{
			if (pid > 0)
			{
				// Parent
				if (close(stdOut[PIPE_WRITE_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDOUT failed while closing"
						" the unused pipe end of the parent!"
					));
				stdOut[PIPE_WRITE_END] = 0;
			}
			else
			{
				// Child - dupe the write end of the pipe to stdout
				if (dup2(stdOut[PIPE_WRITE_END], STDOUT_FILENO) < 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDOUT failed while"
						" duplicating the file descriptor of the pipe end!"
					));

				// Let go of the pipe ends (the duping will remain intact)
				if (close(stdOut[PIPE_READ_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDOUT failed while closing"
						" the read end of the pipe on the child side!"
					));
				if (close(stdOut[PIPE_WRITE_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDOUT failed while closing"
						" the write end of the pipe on the child side!"
					));
			}
		}
		if (redirect & StandardStream::ERR)
		{
			if (pid > 0)
			{
				// Parent
				if (close(stdErr[PIPE_WRITE_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDERR failed while closing"
						" the unused pipe end of the parent!"
					));
				stdErr[PIPE_WRITE_END] = 0;
			}
			else
			{
				// Child - dupe the write end of the pipe to stdout
				if (dup2(stdErr[PIPE_WRITE_END], STDERR_FILENO) < 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDERR failed while"
						" duplicating the file descriptor of the pipe end!"
					));

				// Let go of the pipe ends (the duping will remain intact)
				if (close(stdErr[PIPE_READ_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDERR failed while closing"
						" the read end of the pipe on the child side!"
					));
				if (close(stdErr[PIPE_WRITE_END]) != 0)
					throw err::OSException(CGVU_LOGMSG(
						"ChildProcess::run", "Redirection of STDERR failed while closing"
						" the write end of the pipe on the child side!"
					));
			}
		}

		// Finalize forking logic
		if (pid == 0)
		{
			// Build command line argument list
			ArgV arg(filename, args);

			// Replace process image of child with target
			int retCode = execv(filename.c_str(), arg.v.data());
			exit(retCode);
		}
	}
	inline bool writeStdIn(const void *data, size_t numBytes)
	{
		if (stdIn[PIPE_WRITE_END])
		{
			int result = write(stdIn[PIPE_WRITE_END], data, numBytes);
			if (result < 0 )
			{
				int errorCode = errno;
				if (errorCode == EPIPE)
					// Indicate broken pipe
					return false;
				throw err::OSException(errorCode, CGVU_LOGMSG(
					"ChildProcess::writeStdIn", "Unable to write to standard input of"
					" child process!"
				));
			}
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::writeStdIn", "Child process not started with standard"
				" input redirection!"
			));

		// Indicate an intact pipe
		return true;
	}
	inline bool readStdOut (std::vector<char> *buf, size_t maxBytes)
	{
		if (stdOut[PIPE_READ_END])
		{
			buf->resize(maxBytes);
			size_t bytesRead = read(stdOut[PIPE_READ_END], buf->data(), maxBytes);
			if (bytesRead == -1)
				throw err::OSException(CGVU_LOGMSG(
					"ChildProcess::readStdOut", "Unable to read from standard output of"
					" child process!"
				));
			buf->resize(bytesRead);
			return bytesRead > 0;
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::readStdOut", "Child process not started with standard"
				" output redirection!"
			));
	}
	inline bool readStdErr (std::vector<char>* buf, size_t maxBytes)
	{
		if (stdErr[PIPE_READ_END])
		{
			buf->resize(maxBytes);
			size_t bytesRead = read(stdErr[PIPE_READ_END], buf->data(), maxBytes);
			if (bytesRead == -1)
				throw err::OSException(CGVU_LOGMSG(
					"ChildProcess::readStdErr", "Unable to read from error output of"
					" child process!"
				));
			buf->resize(bytesRead);
			return bytesRead > 0;
		}
		else
			throw err::InvalidOpException(CGVU_LOGMSG(
				"ChildProcess::readStdErr", "Child process not started with error output"
				" redirection!"
			));
	}
	inline int wait (void) const
	{
		// Block until completion
		int status = -1;
		auto result = waitpid(pid, &status, 0);
		if (result == -1)
			throw err::OSException(CGVU_LOGMSG(
				"ChildProcess::wait", "Unable to query child process exit code!"
			));

		// Report exit code
		return status;
	}
	inline void sigint (void) const
	{
	}
#endif
};
#define CHILDPROC_IMPL ((struct ChildProcess_impl*)pimpl)

ChildProcess::ChildProcess(const std::string &executable)
	: pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new ChildProcess_impl();

	CHILDPROC_IMPL->filename = executable;
}

ChildProcess::~ChildProcess()
{
	// De-allocate implementation instance
	if (pimpl)
		delete CHILDPROC_IMPL;
}

void ChildProcess::run (const std::string &args)
{
	CHILDPROC_IMPL->run(args, os::NONE);
}

void ChildProcess::run (StandardStream redirect)
{
	CHILDPROC_IMPL->run("", redirect);
}

void ChildProcess::run (const std::string& args, StandardStream redirect)
{
	CHILDPROC_IMPL->run(args, redirect);
}

bool ChildProcess::writeStdIn (const void *data, size_t numBytes)
{
	return CHILDPROC_IMPL->writeStdIn(data, numBytes);
}

bool ChildProcess::readStdOut (std::vector<char>* buf, size_t maxBytes)
{
	return CHILDPROC_IMPL->readStdOut(buf, maxBytes);
}

bool ChildProcess::readStdErr (std::vector<char>* buf, size_t maxBytes)
{
	return CHILDPROC_IMPL->readStdErr(buf, maxBytes);
}

int ChildProcess::wait (void) const
{
	return CHILDPROC_IMPL->wait();
}

void ChildProcess::requestTermination (void) const
{
	CHILDPROC_IMPL->sigint();
}
