
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <stack>
#include <map>
#include <unordered_map>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <exception>
#include <stdexcept>
#include <thread>
#include <mutex>
#include <chrono>
#include <utility>

// GLM library
#include <glm/gtc/matrix_transform.hpp>

// GLFW library
#include <GLFW/glfw3.h>

// ImGUI library
#include <imgui.h>
#include <examples/imgui_impl_glfw.h>   // ToDo: Replace with custom code
#include <examples/imgui_impl_vulkan.h> // ToDo: Replace with custom code

// CGVu includes
#include "error.h"
#include "util/utils.h"
#include "log/logging.h"
#include "vk/vulkan.h"
#include "vk/vulkan_internals.h" // ToDo: Implement proper APIs for everything in here

// Implemented header
#include "application.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace cgvu;

// Error namespace
using namespace err;



//////
//
// Global state
//

// Anonymous namespace begin
namespace {

struct global {
	Application *theApp;
	std::mutex theApp_mutex;
	global() : theApp(nullptr) {};
} _;

// Anonymous namespace end
}



//////
//
// Exception implementations
//

////
// cgvu::err::GLFWException

struct GLFWException_impl
{
	const char* desc_glfw;
	int errorCode;
	GLFWException_impl() noexcept
	{
		// Fetch last GLFW error and description
		errorCode = glfwGetError(&desc_glfw);
	}
};
#define GLFWEXCEPTION_IMPL ((GLFWException_impl*)pimpl)

err::GLFWException::GLFWException(const log::EventSource &source) noexcept
	: Exception(source), pimpl(nullptr)
{}

err::GLFWException::GLFWException(log::EventSource &&source) noexcept
	: Exception(std::move(source)), pimpl(nullptr)
{}

err::GLFWException::~GLFWException() noexcept
{
	if (pimpl)
		delete GLFWEXCEPTION_IMPL;
}

std::string err::GLFWException::getDescription (void) const
{
	// Shortcut for saving one indirection
	auto &impl = *GLFWEXCEPTION_IMPL;

	// Build description
	std::stringstream desc;
	desc << "GLFW error: E_" << impl.errorCode << std::endl
	     << "  " << (impl.errorCode ? impl.desc_glfw : "Success.");
	return std::move(desc.str());
}



//////
//
// Interface pre-implementations
//

////
// cgvu::ApplicationStub::TransformationStack

template<class FltType>
struct TransformationStack_impl
{
	std::stack<typename cgvu::ApplicationStub<FltType>::Mat4> modelview, projection;
	TransformationStack_impl()
	{
		modelview.emplace(FltType(1));
		projection.emplace(FltType(1));
	}
};
#define TSTACK_IMPL ((TransformationStack_impl<FltType>*)pimpl)
#define TSTACK_PTR(inst) ((TransformationStack_impl<FltType>*)((inst).pimpl))

template <class FltType>
ApplicationStub<FltType>::TransformationStack::TransformationStack() : pimpl(nullptr)
{
	// Create implementation instance
	pimpl = new TransformationStack_impl<FltType>();
}

template <class FltType>
ApplicationStub<FltType>::TransformationStack::~TransformationStack()
{
	// De-allocate implementation instance
	if (pimpl)
		delete TSTACK_IMPL;
}

template <class FltType>
typename ApplicationStub<FltType>::Mat4&
ApplicationStub<FltType>::TransformationStack::modelview (void)
{
	return TSTACK_IMPL->modelview.top();
}

template <class FltType>
const typename ApplicationStub<FltType>::Mat4&
ApplicationStub<FltType>::TransformationStack::modelview (void) const
{
	return TSTACK_IMPL->modelview.top();
}

template <class FltType>
typename ApplicationStub<FltType>::Mat4&
ApplicationStub<FltType>::TransformationStack::projection (void)
{
	return TSTACK_IMPL->projection.top();
}

template <class FltType>
const typename ApplicationStub<FltType>::Mat4&
ApplicationStub<FltType>::TransformationStack::projection (void) const
{
	return TSTACK_IMPL->projection.top();
}

template <class FltType>
void ApplicationStub<FltType>::TransformationStack::pushModelview (void)
{
	auto &impl = *TSTACK_IMPL;
	impl.modelview.emplace(impl.modelview.top());
}

template <class FltType>
void ApplicationStub<FltType>::TransformationStack::popModelview(void)
{
	TSTACK_IMPL->modelview.pop();
}

template <class FltType>
void ApplicationStub<FltType>::TransformationStack::pushProjection (void)
{
	auto &impl = *TSTACK_IMPL;
	impl.projection.emplace(impl.projection.top());
}

template <class FltType>
void ApplicationStub<FltType>::TransformationStack::popProjection (void)
{
	TSTACK_IMPL->projection.pop();
}

template <class FltType>
typename ApplicationStub<FltType>::Mat4
ApplicationStub<FltType>::TransformationStack::getNormalMat (void) const
{
	return std::move(glm::transpose(glm::inverse(TSTACK_IMPL->modelview.top())));
}


////
// cgvu::ApplicationStub

template<class Real>
struct Application_impl
{
	// General
	bool initialized=false, started=false, halted=false;
	unsigned mainSurfaceW=960, mainSurfaceH=540;

	// GLFW
	GLFWwindow *hWnd = NULL;

	// Rendering
	Vulkan vulkan;
	Context *ctx = nullptr;
	VulkanDeviceScoreSet scoreSet = Vulkan::getDefaultScoreSet(DSS::BEST);
	log::Severity
	#ifdef _DEBUG
		drvDebugLevel=log::VERBOSE, vldDebugLevel=log::WARNING;
	#else
		drvDebugLevel=log::WARNING, vldDebugLevel=log::Severity(-1);
	#endif
	typename ApplicationStub<Real>::TransformationStack transforms;
	DepthStencilBuffer mainDepthStencil;

	// ImGUI
	// ToDo: Get rid of any and all bare-metal vulkan references in here
	CommandBuffer imguiCmd;
	VkDescriptorPool imguiDescPool = VK_NULL_HANDLE;
	std::vector<VkFramebuffer> *imguiFBs = nullptr;
	RenderPass imguiRP;
	RenderPass_impl *imguiRP_impl;

	// Statistics
	Real frameTime = 0;

	// Other state
	OrbitCamera<Real> defaultCam;
	Camera<Real> *cam;
	typename ApplicationStub<Real>::MouseState mouse;

	// Intialization
	Application_impl() : defaultCam({0, 0, 0}, {0, 1, -3}, 45), cam(&defaultCam) {}

	// Helpers
	static void onScroll (GLFWwindow* window, double xOff, double yOff)
	{
		auto &ss =
			*((&(((Application_impl<Real>*)*(((size_t*)::getApplication())+2))
				->mouse.scroll))+1);
		ss.x = (Real)xOff;
		ss.y = (Real)yOff;
	}
};
#define APP_IMPL ((Application_impl<FltType>*)pimpl)

template <class FltType>
ApplicationStub<FltType>::ApplicationStub() : pimpl(nullptr)
{
	// Enforce singleton
	std::lock_guard<std::mutex> l(_.theApp_mutex);
	if (_.theApp)
		throw InvalidOpException(CGVU_LOGMSG(
			"Application", "Only one active application allowed at a time!"
		));

	// Create implementation instance
	pimpl = new Application_impl<Real>();
	auto &impl = *APP_IMPL;
	/*impl.callbackPayload.appPtr = this;
	impl.callbackPayload.pimpl = (Application_impl*)pimpl;*/

	// Set singleton
	_.theApp = this;
}

template <class FltType>
ApplicationStub<FltType>::~ApplicationStub()
{
	// Acquire application singleton mutex
	std::lock_guard<std::mutex> l(_.theApp_mutex);

	// Issue cleanup
	cleanup();

	// De-allocate implementation instance
	if (pimpl)
		delete APP_IMPL;

	// Handle releasing the singleton
	if (_.theApp == this)
		_.theApp = nullptr;
	else
		std::cerr << CGVU_TXTMSG(
			"Application", "Possible internal state corruption detected during instance"
			               " destruction!", log::ERROR
		) << std::endl;
}

template <class FltType>
void ApplicationStub<FltType>::init (void)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	// GLFW and main window
	// - library init
	if (!glfwInit())
		throw GLFWException(CGVU_LOGMSG(
			"Application::init", "Unable to initialize GLFW!"
		));
	impl.initialized = true; // We have something to clean up from here on
	// - main window creation
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	impl.hWnd = glfwCreateWindow(
		impl.mainSurfaceW, impl.mainSurfaceH, getName().c_str(), nullptr, nullptr
	);
	if (!impl.hWnd)
		throw GLFWException(CGVU_LOGMSG(
			"Application::init", "Unable to create main window!"
		));
	// - query required Vulkan extensions and layers
	uint32_t count = 0;
	std::set<std::string> reqLayers, reqExts;
	std::map<std::string, Vulkan::DeviceExtensionInfo> reqDevExts;
	declareRequiredInstanceLayers(&reqLayers);
	const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&count);
	if (!glfwExtensions)
		throw GLFWException(CGVU_LOGMSG(
			"Application::init", "Unable to query GLFW Vulkan extension requirements!"
		));
	for (unsigned i=0; i<count; i++)
		reqExts.emplace(glfwExtensions[i]);
	declareRequiredInstanceExtensions(&reqExts);
	declareRequiredDeviceExtensions(&reqDevExts);

	// Vulkan
	impl.vulkan.setDebugLevel(cgvu::Dbg::LOADER_DRIVER, impl.drvDebugLevel);
	impl.vulkan.setDebugLevel(cgvu::Dbg::VALIDATION, impl.vldDebugLevel);
	impl.vulkan.init(getName(), reqExts, reqLayers);
	impl.ctx = &(impl.vulkan.obtainContext(impl.hWnd, impl.scoreSet, reqDevExts));

	// Rendering
	impl.mainDepthStencil = DepthStencilBuffer(
		*(impl.ctx), impl.mainSurfaceW, impl.mainSurfaceH, DST::DEPTH_STENCIL, true
	);
	impl.defaultCam.setDepthMapProvider([&impl] (const Vec2 &pos) -> Real {
		glm::uvec2 dsbufSize{ impl.mainSurfaceW, impl.mainSurfaceH };
		return cgvu::readDepthAt(
			impl.mainDepthStencil.access(), impl.mainDepthStencil.getTexelFormat(),
			dsbufSize, pos
		);
	});

	// Input
	glfwSetScrollCallback(impl.hWnd, impl.onScroll);

	// ImGUI
	// - additional Vulkan resources
	//   ToDo: GET RID OF THE LOW-LEVEL STUFF HERE!!! Note to self: start with font atlas
	//         upload at the bottom since it has the most amount of building blocks
	//         already in place
	VkDevice dev = impl.ctx->handle();
	std::vector<VkDescriptorPoolSize> ps = {
		{ VK_DESCRIPTOR_TYPE_SAMPLER, 32 },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 32 },
		{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 32 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 32 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 32 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 32 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 32 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 32 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 32 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 32 },
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 32 }
	};
	VkDescriptorPoolCreateInfo poolCI = {};
	poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolCI.poolSizeCount = (uint32_t)ps.size();
	poolCI.pPoolSizes = ps.data();
	poolCI.flags =
		  VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT
		| VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
	poolCI.maxSets = 1000;
	VkResult result = vkCreateDescriptorPool(
		dev, &poolCI, nullptr, &(impl.imguiDescPool)
	);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ApplicationStub::init", "Cannot create desciptor pool for ImGUI!"
		));
	// - context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
	// - style
	ImGui::StyleColorsLight();
	// - front-end bindings
	auto &surface = impl.ctx->surface();
	auto &devInfo = *(VulkanDeviceInfo*)impl.ctx->getDeviceInfo();
	auto &caps = *(VkSurfaceCapabilitiesKHR*)surface.getCapabilities();
	auto &fmt = *(VkSurfaceFormatKHR*)surface.getFormat();
	ImGui_ImplGlfw_InitForVulkan(impl.hWnd, true);
	ImGui_ImplVulkan_InitInfo ii = {};
	ii.Instance = impl.vulkan.handle();
	ii.PhysicalDevice = impl.ctx->handlePhy();
	ii.Device = impl.ctx->handle();
	ii.QueueFamily = devInfo.primaryGraphics;
	ii.Queue = devInfo.getGraphicsQueue();
	ii.PipelineCache = VK_NULL_HANDLE; // ToDo: revisit once implemented
	ii.DescriptorPool = impl.imguiDescPool;
	ii.Allocator = nullptr;
	ii.MinImageCount = std::max<unsigned>(caps.minImageCount, 2);
	ii.ImageCount = impl.ctx->surface().getSwapchainLength();
	ii.CheckVkResultFn = nullptr; // ToDo: implement
	// - render pass
	impl.imguiRP_impl = *(RenderPass_impl**)(((char*)&(impl.imguiRP))+sizeof(size_t));
	VkAttachmentDescription attachment = {};
	attachment.format = fmt.format;
	attachment.samples = VK_SAMPLE_COUNT_1_BIT;
	attachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachment.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	VkAttachmentReference attachmentRef = {};
	attachmentRef.attachment = 0;
	attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &attachmentRef;
	VkSubpassDependency subpassDep = {};
	subpassDep.srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDep.dstSubpass = 0;
	subpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDep.srcAccessMask = 0;  // or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	VkRenderPassCreateInfo rpCI = {};
	rpCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	rpCI.attachmentCount = 1;
	rpCI.pAttachments = &attachment;
	rpCI.subpassCount = 1;
	rpCI.pSubpasses = &subpass;
	rpCI.dependencyCount = 1;
	rpCI.pDependencies = &subpassDep;
	result = vkCreateRenderPass(dev, &rpCI, nullptr, &(impl.imguiRP_impl->rp));
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ApplicationStub::init", "Cannot create render pass object for ImGUI!"
		));
	ImGui_ImplVulkan_Init(&ii, impl.imguiRP_impl->rp);
	// - command buffers and font atlas creation
	impl.imguiCmd = impl.ctx->createCommandBuffer(TC::RENDER, true);
	VkCommandBuffer cmd = impl.imguiCmd.handle(0);
	VkCommandBufferBeginInfo bi = {};
	bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	bi.pInheritanceInfo = nullptr;
	result = vkBeginCommandBuffer(cmd, &bi);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ApplicationStub::init", "Creation of command buffer for ImGUI font atlas"
			" upload failed!"
		));
	ImGui_ImplVulkan_CreateFontsTexture(cmd);
	result = vkEndCommandBuffer(cmd);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ApplicationStub::init", "Compiling command buffer for ImGUI font atlas"
			" upload failed!"
		));
	VkSubmitInfo si = {};
	si.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	si.commandBufferCount = 1;
	si.pCommandBuffers = &cmd;
	result = vkQueueSubmit(ii.Queue, 1, &si, nullptr);
	if (result != VK_SUCCESS)
		throw err::VulkanException(result, CGVU_LOGMSG(
			"ApplicationStub::init", "Uploading the ImGUI font atlas failed!"
		));
	impl.ctx->waitIdle();
	ImGui_ImplVulkan_DestroyFontUploadObjects();
	// - framebuffers	
	impl.imguiFBs = (std::vector<VkFramebuffer>*)surface.obtainFramebuffer(impl.imguiRP);

	// Done!
	impl.started = true;
}

template <class FltType>
void ApplicationStub<FltType>::cleanup (void)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	// Handle cleanup
	if (impl.initialized)
	{
		// Wait for any pending operations on the GPU to terminate
		if (impl.ctx)
			impl.ctx->waitIdle();

		// Application can not continue normal operation from here on
		impl.started = false;

		// ImGUI
		ImGui_ImplVulkan_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
		impl.ctx->deleteObject(impl.imguiCmd);
		if (impl.imguiRP_impl->rp)
		{
			vkDestroyRenderPass(impl.ctx->handle(), impl.imguiRP_impl->rp, nullptr);
			impl.imguiRP_impl->rp = VK_NULL_HANDLE;
		}
		if (impl.imguiDescPool)
		{
			vkDestroyDescriptorPool(impl.ctx->handle(), impl.imguiDescPool, nullptr);
			impl.imguiDescPool = VK_NULL_HANDLE;
		}

		// ToDo: investigate whether some kind of Vulkan::shutdown() would be a good
		//       thing here (instead of relying solely on the Vulkan destructor)

		// GLFW main window
		if (impl.hWnd)
		{
			glfwDestroyWindow(impl.hWnd);
			impl.hWnd = NULL;
		}
		glfwTerminate();
	}
	impl.initialized = false;
}

template <class FltType>
void ApplicationStub<FltType>::configureMainWindow (unsigned width, unsigned height)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	// Behave differently according to application status
	if (impl.started)
		// Initiate a window resize event
		glfwSetWindowSize(impl.hWnd, width, height);
	else
	{
		// Just change the initial settings
		impl.mainSurfaceW = width;
		impl.mainSurfaceH = height;
	}
}

template <class FltType>
void ApplicationStub<FltType>::setVulkanLogLevel (VulkanDebugChannel channel,
                                                  log::Severity severity)
{
	if (APP_IMPL->initialized)
		throw InvalidOpException(CGVU_LOGMSG(
			"Application::setVulkanLogLevel", "Unable to change Vulkan log level after"
			" initialization!"
		));
	switch (channel)
	{
		case cgvu::Dbg::LOADER_DRIVER: APP_IMPL->drvDebugLevel = severity; return;
		case cgvu::Dbg::VALIDATION: APP_IMPL->vldDebugLevel = severity;
		default: /* DoNothing() */;
	}
}

template <class FltType>
void ApplicationStub<FltType>::guideVulkanDeviceSelection (
	VulkanScoringCriterion criterion, float score
)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	if (impl.initialized)
		throw InvalidOpException(CGVU_LOGMSG(
			"Application::guideVulkanDeviceSelection", "Unable to change selection"
			" criteria for main Vulkan device after initialization!"
		));
	impl.scoreSet[criterion] = score;
}

template <class FltType>
void ApplicationStub<FltType>::guideVulkanDeviceSelection (
	DefaultScoreSet defaultScoreSet
)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	if (impl.initialized)
		throw InvalidOpException(CGVU_LOGMSG(
			"Application::guideVulkanDeviceSelection", "Unable to change selection"
			" criteria for main Vulkan device after initialization!"
		));
	impl.scoreSet = Vulkan::getDefaultScoreSet(defaultScoreSet);
}

template <class FltType>
void ApplicationStub<FltType>::setMainCamera (Camera<Real> &camera)
{
	APP_IMPL->cam = &camera;
}

template <class FltType>
const Camera<FltType>& ApplicationStub<FltType>::mainCamera (void) const
{
	return *(APP_IMPL->cam);
}

template <class FltType>
void ApplicationStub<FltType>::declareRequiredInstanceLayers (
	std::set<std::string> *out
) const
{
}

template <class FltType>
void ApplicationStub<FltType>::declareRequiredInstanceExtensions (
	std::set<std::string> *out
) const
{
}

template <class FltType>
void ApplicationStub<FltType>::declareRequiredDeviceExtensions (
	std::map<std::string, Vulkan::DeviceExtensionInfo> *out
) const
{
}

template <class FltType>
void ApplicationStub<FltType>::mainInit (Context &context)
{
}

template <class FltType>
void ApplicationStub<FltType>::drawGUI (Context &context)
{
}

template <class FltType>
void ApplicationStub<FltType>::mainCleanup (Context &context)
{
}

template <class FltType>
int ApplicationStub<FltType>::handleUnhandledException (const std::exception& e)
{
	// Just output to console and return the generic system failure code
	std::cerr << e.what() << std::endl;
	return EXIT_FAILURE;
}

template <class FltType>
int ApplicationStub<FltType>::run (int argc, char* argv[])
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;

	/**
		TODO: Introduce command line parsing hooks
	**/

	// Global process exit code
	int exitCode = EXIT_SUCCESS;

	// [BEGIN] outermost exception try/catch for gracefull exit during init or run
	try {

		// System initialization
		init();
		Context &ctx = primaryContext();
		ctx.subscribeEventListener(this);

		// App initialization
		mainInit(ctx);

		// Application main loop
		// - init input
		{ double xPos, yPos;
		  glfwGetCursorPos(impl.hWnd, &xPos, &yPos);
		  impl.mouse.lastPos.x = impl.mouse.curPos.x = (Real)xPos;
		  impl.mouse.lastPos.y = impl.mouse.curPos.y = (Real)yPos;
		  impl.mouse.movement.y = impl.mouse.movement.x = 0;
		  impl.mouse.button[2] = impl.mouse.button[1] = impl.mouse.button[0] = false;
		  impl.mouse.scroll.x = impl.mouse.scrollState.x = 0;
		  impl.mouse.scroll.y = impl.mouse.scrollState.y = 0; }
		// - init stats
		Stats stats = {};
		unsigned long long frameCounter = 0; unsigned avgFrames = 0;
		std::chrono::high_resolution_clock::time_point
			frameStart = std::chrono::high_resolution_clock::now(),
			lastStatsAvg = frameStart;
		// - init viewing
		impl.cam->setViewport(impl.mainSurfaceW, impl.mainSurfaceH);
		// - actual main loop
		bool quit = false;
		if (impl.started) do
		{
			// Input update
			{ double xPos, yPos;
			  glfwGetCursorPos(impl.hWnd, &xPos, &yPos);
			  impl.mouse.lastPos.x = impl.mouse.curPos.x;
			  impl.mouse.lastPos.y = impl.mouse.curPos.y;
			  impl.mouse.curPos.x = (Real)xPos;
			  impl.mouse.curPos.y = (Real)yPos;
			  impl.mouse.movement.x = impl.mouse.curPos.x - impl.mouse.lastPos.x;
			  impl.mouse.movement.y = impl.mouse.curPos.y - impl.mouse.lastPos.y;
			  impl.mouse.button[0] = glfwGetMouseButton(impl.hWnd, 0) == GLFW_PRESS;
			  impl.mouse.button[1] = glfwGetMouseButton(impl.hWnd, 1) == GLFW_PRESS;
			  impl.mouse.button[2] = glfwGetMouseButton(impl.hWnd, 2) == GLFW_PRESS; }

			// Rendering
			// - frame init
			impl.cam->update(stats, impl.mouse);
			impl.transforms.projection() = impl.cam->projectionMatrix();
			impl.transforms.modelview() = impl.cam->viewMatrix();
			ctx.prepareFrame();
			unsigned curFrameID = ctx.getSwapImageID();
			// - user rendering code
			quit = mainLoop(ctx, stats);
			// - GUI
			VkCommandBuffer imguiCmd = impl.imguiCmd.handle(curFrameID);
			VkCommandBufferBeginInfo bi = {};
			bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
			VkResult result = vkBeginCommandBuffer(imguiCmd, &bi);
			if (result != VK_SUCCESS)
				throw err::VulkanException(result, CGVU_LOGMSG(
					"ApplicationStub::run", "Unable to reset ImGUI draw commands buffer!"
				));
			VkRenderPassBeginInfo rpi = {};
			rpi.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			rpi.renderPass = impl.imguiRP_impl->rp;
			rpi.framebuffer = impl.imguiFBs->at(curFrameID);
			rpi.renderArea.extent.width = impl.mainSurfaceW;
			rpi.renderArea.extent.height = impl.mainSurfaceH;
			rpi.clearValueCount = 0;
			rpi.pClearValues = nullptr;
			vkCmdBeginRenderPass(imguiCmd, &rpi, VK_SUBPASS_CONTENTS_INLINE);
			ImGui_ImplVulkan_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();
			drawGUI(ctx);
			ImGui::Render();
			ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), imguiCmd);
			vkCmdEndRenderPass(imguiCmd);
			result = vkEndCommandBuffer(imguiCmd);
			if (result != VK_SUCCESS)
				throw err::VulkanException(result, CGVU_LOGMSG(
					"ApplicationStub::run", "Unable to record ImGUI drawing commands!"
				));
			ctx.submitCommands(impl.imguiCmd);
			// - buffer swap
			ctx.presentFrame();

			// OS interaction
			std::this_thread::yield();
			glfwPollEvents();

			// Update stats
			frameCounter++; avgFrames++;
			if (impl.halted)
			{
				frameStart = std::chrono::high_resolution_clock::now();
				lastStatsAvg = frameStart;
				avgFrames = 0;
				impl.halted = false;
			}
			std::chrono::high_resolution_clock::time_point _now =
				std::chrono::high_resolution_clock::now();
			std::chrono::high_resolution_clock::duration
				frameTime = _now - frameStart,
				avgTime = _now - lastStatsAvg;
			frameStart = _now;
			double frameTime_dbl = double(
				std::chrono::duration_cast<std::chrono::nanoseconds>(frameTime).count()
			);
			stats.frameTime = Real(frameTime_dbl/1000000000.0);
			stats.FPS = Real(1.0/frameTime_dbl);
			if (avgTime > std::chrono::seconds(1))
			{
				std::stringstream title;
				stats.avgFPS = Real(double(avgFrames) / (
					double(
						std::chrono::duration_cast<std::chrono::nanoseconds>(avgTime)
						.count()
					) / 1000000000.0
				));
				title << getName() << " (fps: " << stats.avgFPS << ")";
				glfwSetWindowTitle(impl.hWnd, title.str().c_str());
				lastStatsAvg = frameStart;
				avgFrames = 0;
			}
		}
		// - check exit conditions
		while (!(quit || glfwWindowShouldClose(impl.hWnd)));

		// App client cleanup
		ctx.waitIdle();
		mainCleanup(ctx);

	// [END] outermost exception try/catch for gracefull exit during init or run
	}
	catch (const std::exception &e)
	{
		// Delegate to final handler
		exitCode = handleUnhandledException(e);
	}

	// Auxillary try/catch block for handling cleanup exceptions
	try {
		// De-init app
		cleanup();
	}
	catch (const std::exception &e)
	{
		// Delegate to final handler, but only commit exit code when there was no other
		// fatal exception before invoking cleanup in order to not hide the underlying
		// cause of the exit
		int exitCode_new = handleUnhandledException(e);
		exitCode = (exitCode==EXIT_SUCCESS) ? exitCode_new : exitCode;
	}

	// Done!
	return exitCode;
}

template <class FltType>
typename ApplicationStub<FltType>::TransformationStack&
ApplicationStub<FltType>::transforms (void)
{
	return APP_IMPL->transforms;
}

template <class FltType>
const typename ApplicationStub<FltType>::MouseState&
ApplicationStub<FltType>::mouse (void) const
{
	return APP_IMPL->mouse;
}

template <class FltType>
const DepthStencilBuffer& ApplicationStub<FltType>::mainDepthStencil (void)
{
	return APP_IMPL->mainDepthStencil;
}

template <class FltType>
Vulkan& ApplicationStub<FltType>::vulkanInstance (void)
{
	return APP_IMPL->vulkan;
}

template <class FltType>
const Vulkan& ApplicationStub<FltType>::vulkanInstance (void) const
{
	return APP_IMPL->vulkan;
}

template <class FltType>
Context& ApplicationStub<FltType>::primaryContext (void)
{
	return *(APP_IMPL->ctx);
}

template <class FltType>
const Context& ApplicationStub<FltType>::primaryContext (void) const
{
	return *(APP_IMPL->ctx);
}

template <class FltType>
unsigned ApplicationStub<FltType>::mainSurfaceWidth (void) const
{
	return APP_IMPL->mainSurfaceW;
}

template <class FltType>
unsigned ApplicationStub<FltType>::mainSurfaceHeight (void) const
{
	return APP_IMPL->mainSurfaceH;
}

template <class FltType>
void ApplicationStub<FltType>::onWindowIsResizing (
	cgvu::Context &ctx, unsigned newWidth, unsigned newHeight
)
{
	APP_IMPL->halted = true;
}

template <class FltType>
void ApplicationStub<FltType>::onWindowRefresh (cgvu::Context &ctx)
{
}

template <class FltType>
void ApplicationStub<FltType>::onPresentationSurfaceChanged (cgvu::Context &ctx)
{
	// Shortcut for saving one indirection
	auto &impl = *APP_IMPL;
	PresentationSurface &surface = ctx.surface();

	// Update information that ImGUI wants
	ImGui_ImplVulkan_SetMinImageCount(
		((VkSurfaceCapabilitiesKHR*)surface.getCapabilities())->minImageCount
	);

	// Update application state
	impl.mainSurfaceW = surface.getWidth();
	impl.mainSurfaceH = surface.getHeight();
	impl.cam->setViewport(impl.mainSurfaceW, impl.mainSurfaceH);
	onMainWindowResized(impl.mainSurfaceW, impl.mainSurfaceH);
}



//////
//
// Class implementations
//

////
// cgvu::OrbitCamera

template<class FltType>
struct OrbitCamera_impl
{
	// Convenience aliases
	typedef typename OrbitCamera<FltType>::Real Real;
	typedef typename OrbitCamera<FltType>::Vec3 Vec3;
	typedef typename OrbitCamera<FltType>::Mat4 Mat4;

	// Viewport information
	unsigned vpWidth, vpHeight;

	// Parameters
	Vec3 focusOld, focusNew, focusCur;
	Real theta, phi, dist, fov, focusLerp, zNear=0.015625, zFar=8192.0;

	// State dependent on the parameters
	// - auxiliary vectors
	Vec3 fore, right, up;
	// - resulting matrices
	Mat4 matView, matProj;

	// Misc
	typename OrbitCamera<Real>::DepthMapProvider depthMapProvider;

	OrbitCamera_impl(const Vec3 &focus, Real theta, Real phi, Real dist, Real fov)
		: focusOld(focus), focusNew(focus), focusCur(focus), theta(theta), phi(phi),
		  dist(dist), fov(fov), focusLerp(1)
	{
		calcAuxiliaryVecs();
	}

	OrbitCamera_impl(const Vec3 &at, const Vec3 &eyePos, Real FoV)
	{
		lookAtFov(at, eyePos, FoV);
	}

	void calcAuxiliaryVecs (void)
	{
		// Prelims
		Real sinTheta=glm::sin(theta), cosTheta=glm::cos(theta);

		// Fore vector
		Vec3 foreXZ(-sinTheta, 0, cosTheta);
		fore = glm::cos(phi)*foreXZ + Vec3(0, -glm::sin(phi), 0);

		// Right vector
		right.x = cosTheta;
		right.y = 0;
		right.z = sinTheta;

		// Up vector
		up = glm::cross(fore, right);
	}

	void lookAtFov (const Vec3 &at, const Vec3 &eye, Real FoV)
	{
		// Shortcuts
		typedef glm::vec<2, Real> Vec2;

		// Apply params that map directly
		fore = at - eye;
		focusCur = focusOld = focusNew = at;
		dist = glm::length(fore);
		fore /= dist;
		fov = FoV;
		focusLerp = 1;

		// Infer the angles
		theta = std::atan2(-fore.x, fore.z);
		phi = glm::half_pi<Real>() - glm::acos(glm::dot(-fore, Vec3(0, 1, 0)));

		// Update dependent state (we assume this is called very rarely)
		calcAuxiliaryVecs();
	}
};
#define ORBITCAM_IMPL ((OrbitCamera_impl<FltType>*)pimpl)

template <class FltType>
OrbitCamera<FltType>::OrbitCamera(
	const Vec3& initalFocus, Real initialDist, Real initialTheta, Real initialPhi,
	Real initialFoV
)
{
	// Create implementation instance
	pimpl = new OrbitCamera_impl<Real>(
		initalFocus, initialTheta, initialPhi, initialDist, initialFoV
	);
}

template <class FltType>
OrbitCamera<FltType>::OrbitCamera(const Vec3 &at, const Vec3 &eyePos, Real FoV)
{
	// Create implementation instance
	pimpl = new OrbitCamera_impl<Real>(at, eyePos, FoV);
}

template <class FltType>
OrbitCamera<FltType>::~OrbitCamera()
{
	// De-allocate implementation instance
	if (pimpl)
		delete ORBITCAM_IMPL;
}

template <class FltType>
void OrbitCamera<FltType>::setViewport (unsigned width, unsigned height)
{
	// Shortcut for saving one indirection
	auto &impl = *ORBITCAM_IMPL;

	// Set it
	impl.vpWidth = width;
	impl.vpHeight = height;

	// Update projection matrix (we assume this is called very rarely)
	impl.matProj = glm::perspective(glm::radians(impl.fov), Real(width)/Real(height),
	                                impl.zNear, impl.zFar);
	impl.matProj[1][1] *= Real(-1);
}

template <class FltType>
void OrbitCamera<FltType>::lookAtFoV (const Vec3 &at, const Vec3 &eyePos, const Vec3&,
                                      Real FoV)
{
	// Delegate (up vector is ignored by orbit cameras)
	ORBITCAM_IMPL->lookAtFov(at, eyePos, FoV);
}

template <class FltType>
void OrbitCamera<FltType>::update (
	const typename ApplicationStub<Real>::Stats &stats,
	const typename ApplicationStub<Real>::MouseState &mouse
)
{
	// Shortcut for saving one indirection
	auto &impl = *ORBITCAM_IMPL;

	// Handle Keyboard controls
	auto &io = ImGui::GetIO();
	Vec3 camFocusChange(0, 0, 0);
	if (!(io.WantCaptureKeyboard))
	{
		if (ImGui::IsKeyDown(GLFW_KEY_UP))
			camFocusChange += Real(2)*impl.fore*stats.frameTime;
		if (ImGui::IsKeyDown(GLFW_KEY_DOWN))
			camFocusChange -= Real(2)*impl.fore*stats.frameTime;
		if (ImGui::IsKeyDown(GLFW_KEY_LEFT))
			camFocusChange += Real(2)*impl.right*stats.frameTime;
		if (ImGui::IsKeyDown(GLFW_KEY_RIGHT))
			camFocusChange -= Real(2)*impl.right*stats.frameTime;
		if (ImGui::IsKeyDown(GLFW_KEY_RIGHT_CONTROL))
			camFocusChange += Real(2)*impl.up*stats.frameTime;
		if (ImGui::IsKeyDown(GLFW_KEY_KP_0))
			camFocusChange -= Real(2)*impl.up*stats.frameTime;
	}

	// Handle mouse controls
	if (!(io.WantCaptureMouse))
	{
		if (ImGui::IsMouseDown(ImGuiMouseButton_Left))
		{
			impl.theta = glm::mod<Real>(
				impl.theta + glm::half_pi<Real>()/Real(360)*mouse.movement.x,
				glm::two_pi<Real>()
			);
			impl.phi = glm::clamp<Real>(
				impl.phi + glm::half_pi<Real>()/Real(360)*mouse.movement.y,
				-glm::half_pi<Real>(), glm::half_pi<Real>()
			);
			impl.calcAuxiliaryVecs();
		}
		if (mouse.isScrollingY() != 0)
			impl.dist = glm::max(
				  impl.dist
				-    std::max(Real(0.1)*impl.dist, Real(0.03125))
				  * Real(ImGui::IsKeyDown(GLFW_KEY_LEFT_CONTROL) ? 3 : 1)*mouse.scroll.y,
				Real(.00001)
			);
		if (ImGui::IsMouseDown(ImGuiMouseButton_Right))
		{
			Real speed = Real(.0009765625)*impl.dist;
			camFocusChange += speed * mouse.movement.x*impl.right;
			camFocusChange += speed * mouse.movement.y
				* (ImGui::IsKeyDown(GLFW_KEY_LEFT_SHIFT) ? Real(3)*impl.fore : impl.up);
		}
		else if (ImGui::IsMouseDown(ImGuiMouseButton_Middle))
			camFocusChange += Real(.001953125)*impl.dist * mouse.movement.y*impl.fore;
	}
	impl.focusOld += camFocusChange;
	impl.focusNew += camFocusChange;
	impl.focusCur += camFocusChange;
	if (impl.focusLerp < Real(1))
	{
		impl.matView = glm::lookAt(
			impl.focusCur - impl.fore*impl.dist, impl.focusCur, impl.up
		);
		impl.focusLerp = glm::min(impl.focusLerp + Real(2)*stats.frameTime, Real(1));
		impl.focusCur =
			cgvu::util::smoothLerp(impl.focusOld, impl.focusNew, impl.focusLerp);
	}
	else
		impl.matView = glm::lookAt(
			impl.focusCur - impl.fore*impl.dist, impl.focusNew, impl.up
		);

	// If we were given a depth provider, handle double-click-to-focus
	if (   impl.depthMapProvider
	    && !(io.WantCaptureMouse) && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
	{
		Real depth = impl.depthMapProvider(mouse.curPos);
		if (depth < 1.f)
		{
			Vec4 pt =
				  glm::inverse(impl.matProj * impl.matView)
				* Vec4(mouse.curPos.x*Real(2)/Real(impl.vpWidth) - Real(1),
				       mouse.curPos.y*Real(2)/Real(impl.vpHeight) - Real(1),
				       depth, 1);
			impl.focusCur = impl.focusOld =
				cgvu::util::smoothLerp(impl.focusOld, impl.focusNew, impl.focusLerp);
			impl.focusNew = Vec3(pt) / pt.w;
			impl.focusLerp = 0;
		}
	}
}

template <class FltType>
const typename OrbitCamera<FltType>::Mat4& OrbitCamera<FltType>::viewMatrix (void) const
{
	return ORBITCAM_IMPL->matView;
}

template <class FltType>
const typename OrbitCamera<FltType>::Mat4& OrbitCamera<FltType>::projectionMatrix (void)
	const
{
	return ORBITCAM_IMPL->matProj;
}

template <class FltType>
typename OrbitCamera<FltType>::Vec3 OrbitCamera<FltType>::position (void) const
{
	// Shortcut for saving one indirection
	auto &impl = *ORBITCAM_IMPL;

	// Reconstruct from auxiliary vectors
	return std::move(impl.focusCur - impl.fore*impl.dist);
}

template <class FltType>
void OrbitCamera<FltType>::setDepthMapProvider (const DepthMapProvider &depthMapProvider)
{
	ORBITCAM_IMPL->depthMapProvider = depthMapProvider;
}



//////
//
// Function implementations
//

extern "C" CGVU_API Application* getApplication (void)
{
	return _.theApp;
}

CGVU_API Application& referenceApplication (void)
{
	return *(_.theApp);
}



//////
//
// Explicit template instantiations
//

// Only float and double variants are intended
template class cgvu::ApplicationStub<float>;
template class cgvu::ApplicationStub<double>;
template class cgvu::OrbitCamera<float>;
template class cgvu::OrbitCamera<double>;
