
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <utility>

// GLM library
#include <glm/gtx/norm.hpp>

// CGVU includes
#include "utils.h"

// Implemented header
#include "regulargrid.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::util;



//////
//
// Class implementations
//


////
// cgvu::util::Grid3D

template<class Real>
struct Grid3D_impl
{
	Real cellwidth;
	typename Grid3D<Real>::PointAccessor pntAccess;

	std::unordered_map<
		typename Grid3D<Real>::Cell, std::vector<size_t>,
		typename Grid3D<Real>::Cell::Hash
	> grid;

	Grid3D_impl(Real cellwidth, typename Grid3D<Real>::PointAccessor pointAccessor)
		: cellwidth(cellwidth), pntAccess(pointAccessor)
	{}
};
#define GRID3D_IMPL ((Grid3D_impl<Real>*)pimpl)

template <class FltType>
Grid3D<FltType>::Grid3D(Real cellwidth, PointAccessor pointAccessor) : pimpl(nullptr)
{
	pimpl = new Grid3D_impl(cellwidth, pointAccessor);
}

template <class FltType>
Grid3D<FltType>::~Grid3D()
{
	if (pimpl)
		delete GRID3D_IMPL;
}

template <class FltType>
void Grid3D<FltType>::insert (size_t index)
{
	// Shortcut for saving one indirection
	auto &impl = *GRID3D_IMPL;

	// Retrieve the point
	Vec3 point;
	impl.pntAccess(&point, index);

	// Insert into appropriate cell
	Cell cell = Cell::get(point, impl.cellwidth);
	impl.grid[cell].push_back(index);
}

template <class FltType>
bool Grid3D<FltType>::query (std::vector<size_t> *out, const Vec3 &queryPoint,
                             bool distanceSort) const
{
	// Shortcut for saving one indirection
	auto& impl = *GRID3D_IMPL;


	// Keep track of first new element position in output array
	size_t sortStart = out->size();

	// Determine query cell
	Cell _q = Cell::get(queryPoint, impl.cellwidth);

	// Loop through 27-neighborhood
	bool foundPoints = false;
	for (unsigned i=0; i<27; i++)
	{
		Cell qCur{/* x = */ _q.x-1 + long(i%3),
		          /* y = */ _q.y-1 + long((i/3)%3),
		          /* z = */ _q.z-1 + long(i/9)};
		auto cell = impl.grid.find(qCur);
		if (cell != impl.grid.end())
		{
			foundPoints = true;
			out->insert(out->end(), cell->second.begin(), cell->second.end());
		}
	}

	// Sort according to distance if requested
	if (distanceSort)
		std::sort(out->begin()+sortStart, out->end(),
		[&impl, &queryPoint] (size_t l, size_t r) {
			Vec3 lPoint, rPoint;
			impl.pntAccess(&lPoint, l);
			impl.pntAccess(&rPoint, r);
			return   glm::distance2(lPoint, queryPoint)
			       < glm::distance2(rPoint, queryPoint);
		});

	return foundPoints;
}

template <class FltType>
void Grid3D<FltType>::setPointAccessor (PointAccessor pointAccessor)
{
	GRID3D_IMPL->pntAccess = pointAccessor;
}



//////
//
// Explicit template instantiations
//

// Only float and double variants are intended
template class Grid3D<float>;
template class Grid3D<double>;
