
#ifndef __UTILS_H__
#define __UTILS_H__


//////
//
// Includes
//

// C++ STL
#include <cstring>
#include <functional>
#include <algorithm>

// GLM library
#include <glm/glm.hpp>

// Library config
#include "cgvu/libconfig.h"



//////
//
// Namespace openings
//

// Root namespace
namespace cgvu {

// Module namespace
namespace util {



//////
//
// Function definitions
//

/**
 * @brief
 *		Performs a warnings-free reinterpretation of an integral value as a void pointer.
 */
template <class T>
inline void* toVoidP (T integral)
{
	return reinterpret_cast<void*>((size_t)integral);
}

/**
 * @brief
 *		Performs a warnings-free reinterpretation of a void pointer as an integral value.
 */
template <class T>
inline T fromVoidP(void *p)
{
	return (T)(size_t)p;
}

/** @brief C-string list search. */
inline bool contains (
	const char **list, const char *elem, size_t num,
	const std::function<int(const char*, const char*)> &comparator=std::strcmp
)
{
	for (size_t i=0; i<num; i++)
		if (comparator(elem, list[i]) == 0)
			return true;
	return false;
}

/** @brief Generic list search. */
template <class C, class E>
inline bool contains (const C &list, const E &elem)
{
	auto it = std::find(list.begin(), list.end(), elem);
	return it != list.end();
}

/** @brief Generic linear interpolation. */
template <class T, class real=double>
inline T lerp (const T &v1, const T &v2, real t)
{
	return v1 * (real(1)-t)   +   v2 * t;
}

/** @brief Generic cubic for C1-smooth interpolation. */
template <class real>
real smoothStep (real t_linear)
{
	real t2 = t_linear*t_linear;
	return glm::clamp(real(-2)*t2*t_linear + real(3)*t2, real(0), real(1));
}

/** @brief Generic C1-smooth cubic interpolation. */
template <class T, class real=double>
inline T smoothLerp (const T &v1, const T &v2, real t_linear)
{
	return lerp(v1, v2, smoothStep(t_linear));
}

/** @brief Integer "within range" comparision. */
inline bool inRange (int min, int max, int val)
{
	return val >= min && val <= max;
}

/** @brief Generic "within range" comparision. */
template <class T>
inline bool inRange (const T &min, const T &max, const T &val)
{
	return val >= min && val <= max;
}

/** @brief Generic value clamping. */
template <class T>
inline T clamp (const T& min, const T& max, const T& val)
{
	return std::min(val, std::max(val, min));
}

/** @bief Hash functor to use on vectors of arbitrary dimension. */
template <class VecType>
struct VectorHash {
	/** @bief The function call operator. Performs the hashing. */
	inline size_t operator()(const VecType &vec) const {
		constexpr unsigned n = vec.length();
		size_t seed = n;
		for (unsigned i=0; i<n; i++)
			seed ^= vec[i] + ((size_t)0x9e3779b9) + (seed<<6) + (seed>>2);
		return seed;
	}
};

/** @brief Streams data into a vector. */
template <class Vec>
inline void streamToVec(Vec* out, std::istream& stream)
{
	constexpr size_t n = std::remove_pointer_t<decltype(out)>::length();
	for (unsigned i=0; i<n; i++)
		stream >> (*out)[i];
}

/** @brief Streams data into the first @a n components of a vector. */
template <class Vec>
inline void streamToVec(Vec *out, std::istream &stream, unsigned n)
{
	for (unsigned i=0; i<n; i++)
		stream >> (*out)[i];
}

/** @brief Checks if a file pointed to by the given path exists or not. */
CGVU_API bool fileExists (const std::string &filepath);

/** @brief Checks if a directory pointed to by the given path exists or not. */
CGVU_API bool dirExists (const std::string &dirpath);

/**
 * @brief
 *		Returns the file or directory name portion of a path, i.e. everything that
 *		follows after the last slash '/' or backslash '\' character in the path.
 */
CGVU_API std::string nameWithoutPath (const std::string &path);

/**
 * @brief
 *		Returns only the path portion of a complete file or directory path, i.e.
 *		everything preceding the last slash '/' or backslash '\' character in the path.
 */
CGVU_API std::string pathWithoutName (const std::string &path);

/**
 * @brief
 *		Returns the given path with its file extension removed, i.e. everything that
 *		precedes the last dot '.' after the last slash '/' or backslash '\' character in
 *		the path.
 *
 * @note
 *		Does not check wether the path actually points to a file - to prevent characters
 *		after the last dot '.' in a directory name being removed, use @ref fileExists or
 *		@ref dirExists to verify the nature of the path first. This will be especially
 *		relevant if you work with relative paths ("..")
 */
CGVU_API std::string pathWithoutExtension (const std::string &path);

/**
 * @brief
 *		Returns the file extension of a path, i.e. everything that follows the last dot
 *		'.' after the last slash '/' or backslash '\' character in a string.
 *
 * @note
 *		Does not check wether the path actually points to a file - to prevent a file
 *		extension being returned when used on a directory use @ref dirExists or @ref
 *		fileExists to verify the nature of the path first. This will be especially
 *		relevant if you work with relative paths ("..")
 */
CGVU_API std::string getFileExtension (const std::string &path);

/** @brief Performs bitwise-and on compatible types (useful e.g. for enums). */
template <class T>
inline T bitAnd (const T op1, const T op2)
{
	return (T)(((unsigned)op1)&((unsigned)op2));
}

/** @brief Performs a bitwise-and test on compatible types (useful e.g. for enums). */
template <class T>
inline bool bitTest (const T op1, const T op2)
{
	return ((unsigned)op1)&((unsigned)op2);
}



//////
//
// Classes
//

/**
 * @brief
 *		RAII wrapper to make dealing with C-style handles and similar things that are
 *		hard to manage using STL-provided facilites (like unique_ptr) exception safe.
 */
template <
	class T, class finalizer_rettype, class finalizer_argtype,
	finalizer_rettype (*finalizer)(finalizer_argtype*)
>
struct GenericRAII
{
	////
	// Data members

	/** @brief The C-style thing (handle, object etc.) managed by this RAII-wrapper. */
	T *thing;


	////
	// Object construction / destruction

	/** @brief The default constructor. */
	inline GenericRAII() : thing(nullptr)
	{};

	/**
	 * @brief Constructs the RAII wrapper with the given @ref #thing to manage.
	 *
	 * @param thing
	 *		The C-style thing (handle, object etc.) that should be made exception-safe
	 *		via RAII.
	 */
	inline GenericRAII(T *thing) : thing(thing)
	{};

	/** @brief The destructor. */
	~GenericRAII()
	{
		if (thing)
		{
			finalizer(thing);
			thing = nullptr;
		}
	}


	////
	// Conversion

	/** @brief Access to managed thing as C-style pointer. */
	inline operator T* (void) { return thing; }

	/** @brief Dereference to managed thing. */
	inline T& operator * (void) { return *thing; }

	/**
	 * @brief
	 *		Get the memory address of the thing (in many cases that will yield a
	 *		pointer-to-a-pointer).
	 */
	inline T** address (void) { return &thing; }
};

/**
 * @brief
 *		Specialized version of @ref GenericRAII suitable for most C-style objects (with
 *		the notable exception of memory buffers allocated using @c malloc - use @ref
 *		RAIIbuffer for that). It assumes a finalizer with @c void return type and an
 *		argument of the same type as the @ref GenericRAII::thing .
 */
template <class T, void (*finalizer)(T*)>
using RAII = GenericRAII<T, void, T, finalizer>;

/**
* @brief
*		Specialized version of @ref GenericRAII suitable for C-style memory buffers like
*		dynamic arrays allocated with @c malloc . It assumes a finalizer with @c void
*		return type taking a @c void pointer as argument.
*/
template <class T, void (*finalizer)(void*)>
using RAIIbuffer = GenericRAII<T, void, void, finalizer>;

/**
 * @brief
 *		Utility that can be used to run code when going out of scope - handy for
 *		exception safety.
 */
class Finally
{
private:
	const std::function<void(void)> &finalizer;
public:
	inline Finally(const std::function<void(void)> &finalizer) : finalizer(finalizer) {}
	inline ~Finally() { finalizer(); }
};

/**
 * @brief
 *		Utility that can efficiently serve as unique identifier given some sort of name,
 *		which can be of a variety of types.
 */
class Identifier
{
private:
	size_t id;
public:
	// ToDo: implement string names (e.g. via hashing)
	inline Identifier(signed name) : id(name) {}
	inline Identifier(unsigned name) : id(name) {}
	inline bool operator== (Identifier other) { return id == other.id; }
};



//////
//
// Namespace closings
//

// Module namespace: util
}

// Root namespace: cgvu
}


#endif // ifndef __UTILS_H__
