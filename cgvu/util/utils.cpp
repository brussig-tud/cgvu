
//////
//
// Includes
//

// POSIX headers
#include <sys/types.h>
#include <sys/stat.h>

// C++ STL
#include <cerrno>
#include <string>
#include <sstream>
#include <algorithm>
#include <utility>

// CGVU includes
#include "os/os.h"

// Implemented header
#include "utils.h"



//////
//
// Namespaces
//

// Implemented namespaces
using namespace cgvu;
using namespace cgvu::util;



//////
//
// Function implementations
//

// ToDo: investigate whether to fail by exception on a stat() error or not

bool util::fileExists (const std::string &filepath)
{
	struct stat f;
	return (stat(filepath.c_str(), &f) == 0)  &&  f.st_mode & S_IFREG;
}

bool util::dirExists (const std::string &dirpath)
{
	struct stat d;
	return (stat(dirpath.c_str(), &d) == 0)  &&  d.st_mode & S_IFDIR;
}

std::string util::nameWithoutPath (const std::string &path)
{
	const char *begin = path.c_str(), *ptr = begin + std::max<size_t>(path.size()-1, 0);
	while (ptr >= begin && *ptr != '/' && *ptr != '\\')
		ptr--;
	ptr++;
	return std::move(std::string(ptr, path.size()-(ptr-begin)));
}

std::string util::pathWithoutName (const std::string &path)
{
	const char *begin = path.c_str(), *ptr = begin + std::max<size_t>(path.size()-1, 0);
	while (ptr >= begin && *ptr != '/' && *ptr != '\\')
		ptr--;
	return std::move(std::string(begin, ptr-begin));
}

std::string util::pathWithoutExtension (const std::string &path)
{
	const char *begin = path.c_str(), *ptr = begin + std::max<size_t>(path.size()-1, 0);
	while (ptr >= begin && *ptr != '.' && *ptr != '/' && *ptr != '\\')
		ptr--;
	if (*ptr == '.')
		return std::move(std::string(begin, ptr-begin));
	return path;
}

std::string util::getFileExtension (const std::string &path)
{
	const char *begin = path.c_str(), *ptr = begin + std::max<size_t>(path.size() - 1, 0);
	while (ptr >= begin && *ptr != '.' && *ptr != '/' && *ptr != '\\')
		ptr--;
	if (*ptr == '.')
		return std::move(std::string(ptr+1, path.size()-(ptr+1-begin)));
	return std::move(std::string(nullptr, 0));
}
