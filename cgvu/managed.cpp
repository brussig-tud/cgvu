
//////
//
// Includes
//

// Implemented header
#include "managed.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace cgvu;



//////
//
// Interface pre-implementations
//

////
// cgvu::ManagedObject

template<class Object, class Manager>
size_t ManagedObject<Object, Manager>::objectID (void) const
{
	return pimpl;
}
