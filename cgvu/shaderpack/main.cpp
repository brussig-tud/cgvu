
//////
//
// Includes
//

// C++ STL
#include <string>
#include <iostream>

// CGVu library
#include <cgvu/util/utils.h>
#include <cgvu/vk/shaderprog.h>



//////
//
// Namespaces
//

// CGVu
using namespace cgvu;



//////
//
// Application entry point
//

int main (int argc, char* argv[])
{
	// Check for presence of command line options
	if (argc < 2)
	{
		std::cerr << "[ShaderPack] Error: no input file specified!" << std::endl;
		return -1;
	}

	// Parse options and look for first argument that is not an option, which is our
	// input file
	std::string progfile, outfile;
	enum Cmd {
		NEUTRAL,
		OUTPUT
	} curCmd = NEUTRAL;
	for (int arg=1; arg<argc; arg++)
	{
		switch (curCmd)
		{
			case NEUTRAL:
				switch (argv[arg][0])
				{
					case '-':
					{
						// Option
						if (argv[arg][1] == 'o' && argv[arg][2] == 0)
						{
							curCmd = OUTPUT;
							break;
						}
						else
						{
							std::cerr << "[ShaderPack] Error: unrecognized option in"
							             " argument " << arg << ": " << argv[arg]
							          << std::endl;
							return -1;
						}
					}

					default:
					{
						if (!progfile.empty())
						{
							std::cerr << "[ShaderPack] Error: only one program file may"
							             " be specified as input!" << std::endl;
							return -1;
						}

						progfile = argv[arg];
					}
				}
				break;

			case OUTPUT:
				outfile = argv[arg];
				curCmd = NEUTRAL;
				break;
		}
	}
	if (outfile.empty())
		outfile = cgvu::util::pathWithoutExtension(progfile) + ".spk";

	// Collect modules
	cgvu::ShaderProgram prog(cgvu::SHADER_SRC_DEF, progfile, false);

	// Compile
	int retVal = EXIT_FAILURE;
	try {
		prog.load();
		prog.save(outfile);
		retVal = EXIT_SUCCESS;
	}
	// ToDo: introduce more fine-grained exception types to differentiate between at
	//       least .prog parsing errors and GLSLC compilation errors
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}

	// All done!
	return retVal;
}
