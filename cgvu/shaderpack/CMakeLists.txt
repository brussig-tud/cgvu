
# Setup
cgvuSetBooleanNegated("SHADERC_LINK_STATIC" ${CGVU_BUILD_SHARED})

# Target creation
cgvuAddApplication(shaderpack STATIC_CGVU ${SHADERC_LINK_STATIC})
set_target_properties(shaderpack PROPERTIES OUTPUT_NAME "shaderpack")

# List sources
cgvuAddSources(shaderpack FILES main.cpp)

# Additional includes
target_include_directories(shaderpack PRIVATE "${PEGLIB_INCLUDE_DIR}")
