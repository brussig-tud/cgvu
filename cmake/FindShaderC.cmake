#
# Find ShaderC
#
# Try to find Google's ShaderC SPIR-V compiler library
# This module defines
# - ShaderC_DIR
# - ShaderC_INCLUDE_DIR
# - ShaderC_LIBS
# - ShaderC_FOUND
#
# This modules imports the targets:
# - shaderc
# - shaderc_shared
# - shaderc_combined
#
# The following variables can be set as arguments for the module.
# - GRAPHICS_SDK_DIR : Root directory of the graphics API SDK containing ShaderC
#

# Additional modules
include(FindPackageHandleStandardArgs)

# Helper macro for listing all subdirectories with the given name within a directory
FUNCTION(findSubDirectories result scandir finddir)
	string(TOLOWER "${finddir}" finddir_lower)
	file(GLOB_RECURSE children LIST_DIRECTORIES true "${scandir}/." "${scandir}/*")
	set(dirlist "")
	foreach(child ${children})
		if(IS_DIRECTORY ${child})
			get_filename_component(child_name ${child} NAME)
			string(TOLOWER "${child_name}" child_name_lower)
			if("${finddir_lower}" STREQUAL "${child_name_lower}")
				list(APPEND dirlist ${child})
			endif()
		endif()
	endforeach()
	set(${result} ${dirlist} PARENT_SCOPE)
ENDFUNCTION()


# Only execute if ShaderC_DIR not yet set or, if marked as REQUIRED, not yet found
if("${ShaderC_DIR}" STREQUAL "" OR (ShaderC_FIND_REQUIRED AND "${ShaderC_DIR}" STREQUAL "ShaderC_DIR-NOTFOUND"))
	# Make sure the CMake find... functions will actually do something
	set(ShaderC_DIR "" FORCE)
	unset(ShaderC_DIR)
	unset(ShaderC_DIR CACHE)

	# Handle argument GRAPHICS_SDK_DIR
	if ("${GRAPHICS_SDK_DIR}" STREQUAL "")
		if(NOT "$ENV{VK_SDK_PATH}" STREQUAL "")
			string(REPLACE "\\" "/" GRAPHICS_SDK_DIR "$ENV{VK_SDK_PATH}")
		elseif("$ENV{VULKAN_SDK}" STREQUAL "")
			string(REPLACE "\\" "/" GRAPHICS_SDK_DIR "$ENV{VULKAN_SDK}")
		else()
			# No hints provided to the Find script
			message("FindShaderC: no graphics SDK could be identified, this find script will most likely fail.")
		endif()
	endif()

	# ToDo: ShaderC in the Windows Vulkan SDK is a complete mess. For now, just report we can't find it...
	set(ShaderC_DIR "ShaderC_DIR-NOTFOUND" CACHE PATH "Root directory of the ShaderC SPIR-V compiler utilities package")
	# Do the search
	#findSubDirectories(SHADERC_CANDIDATES ${GRAPHICS_SDK_DIR} "libshaderc")
	#set(SHADERC_FOUND_BASE "")
	#foreach(can ${SHADERC_CANDIDATES})
	#	get_filename_component(POSSIBLE_SHADERC_BASE ${can}/.. ABSOLUTE)
	#	set(listsfile_test ${POSSIBLE_SHADERC_BASE}/CMakeLists.txt)
	#	if(EXISTS "${listsfile_test}" AND NOT IS_DIRECTORY "${listsfile_test}")
	#		set(SHADERC_FOUND_BASE ${POSSIBLE_SHADERC_BASE})
	#		break()
	#	endif()
	#endforeach()
	#if("${SHADERC_FOUND_BASE}" STREQUAL "")
	#	set(ShaderC_DIR "ShaderC_DIR-NOTFOUND" CACHE FILE "Root directory of the ShaderC SPIR-V compiler utilities package")
	#else()
	#	set(ShaderC_DIR ${SHADERC_FOUND_BASE} CACHE FILE "Root directory of the ShaderC SPIR-V compiler utilities package")
	#	include(${ShaderC_DIR}/CMakeLists.txt)
	#	get_cmake_property(_variableNames VARIABLES)
	#	list (SORT _variableNames)
	#	foreach (_variableName ${_variableNames})
	#		message(STATUS "${_variableName}=${${_variableName}}")
	#	endforeach()
	#endif()
else()
	# Supress repeated "not found" messages when ShaderC is not required
	set(ShaderC_FIND_QUIETLY 1)
endif()

# Handle REQUIRD argument, define *_FOUND variable
find_package_handle_standard_args(ShaderC DEFAULT_MSG ShaderC_DIR)

# Post-process
if(SHADERC_FOUND)
	mark_as_advanced(SHADERC_LIBRARY)
endif()
