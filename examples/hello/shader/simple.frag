#version 450
#extension GL_KHR_vulkan_glsl : enable

// Uniforms
layout(binding = 1) uniform CGVuMaterial {
	vec4 albedo;
	float specularity;
	float shininess;
} cgvu_mat;

layout(binding = 2) uniform CGVuGlobalLight {
	vec3 pos;   // in eye space
	vec3 color;
} cgvu_light;

layout(binding = 3) uniform sampler2D texAlbedo;

// Inputs
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;
layout(location = 3) in vec3 in_color;

// Outputs
layout(location = 0) out vec4 out_color;


// Shader entry point
void main()
{
	// Final color from material
	vec4 texColor = texture(texAlbedo, in_texcoord);
	vec3 color = in_color * cgvu_mat.albedo.rgb;
	const float texSaturation = 1-0.4;
	color =   0.4*color
	        + texSaturation*((1-texColor.a)*color + texColor.a*color*texColor.rgb);

	// Lighting
	// - init
	vec3 normal = normalize(in_normal);
	vec3 dirToLight = normalize(cgvu_light.pos - in_pos);
	// - diffuse term (lambert)
	vec3 diffuse = color*cgvu_light.color*max(dot(dirToLight, normal), 0);
	// - specular term (blinn-phong)
	vec3 halfVec = normalize(normalize(-in_pos) + dirToLight);
	vec3 specular =
		  color*cgvu_mat.specularity*cgvu_light.color
		* pow(max(dot(halfVec, normal), 0), cgvu_mat.shininess);

	// Final color
	out_color = vec4(diffuse + specular, cgvu_mat.albedo.a);
}
